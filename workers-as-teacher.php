<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 22.01.2019
 * Time: 08:28
 */
?>

    <div class="row" style="margin-bottom: 2rem;">
        <div class="col-md-3">
            <a href="/youth-workers.html" class="btn btn-success">Back to YOUTH-WORKER menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h6 style="background-color: #007bff; padding: .5rem; color: #fff" class="text-center">INNOVATIVE TRAINING
                ON SELF-EMPLOYMENT FOR YOUNG PEOPLE BASED ON MENTORING</h6>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10 col-sm-11">
            <h2 class="text-center worker-title">YOUTH-WORKER AS TEACHER</h2>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <a href="#">
                        <img src="/img/youth-workers/teachers_guide.png" alt="guide" class="img-fluid"/>
                    </a>
                </div>
                <div class="col-md-6">
                    <a href="#">
                        <img src="/img/youth-workers/teachers_ppt.png" alt="presentations" class="img-fluid"/>
                    </a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <a href="#">
                        <img src="/img/youth-workers/teachers_tools.png" alt="tools" class="img-fluid"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
