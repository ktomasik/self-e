<?php
/**
 * Created by PhpStorm.
 * User: lklapa
 * Date: 19.05.2019
 * Time: 17:26
 */
?>

<style>
    .curriculum-menu {
        border: 1px solid #8dca2a;
        -webkit-border-radius: .25rem;
        -moz-border-radius: .25rem;
        border-radius: .25rem;
        padding: 20px;
    }
    table.partners-table td {
        padding: 10px;
    }
    table.partners-table td:first-of-type {
        text-align: right;
        font-weight: bold;
        vertical-align: middle;
    }
    table.partners-table img {
        max-width: 200px;
        max-height: 80px;
    }
    .curriculum-image p {
        font-size: 0.8em;
        color: #333333;
        font-style: italic;
    }
    table.curriculum-plan tr:nth-child(even) {
        background: rgb(238,255,229);
    }
    table.curriculum-plan tr:nth-child(odd) {
        background: rgb(224,241,252);
    }
    table.curriculum-plan tr:first-child {
        background: #fefefe;
    }
    table.curriculum-plan tr > td:nth-of-type(3) {
        text-align: center;
    }
    ol.curriculum-plan-list li:nth-child(even) {
        color: rgb(79,98,40);
    }
    ol.curriculum-plan-list li:nth-child(odd) {
        color: rgb(36,64,97);
    }
    #v-pills-tabContent h3 {
        color: #7ad0fa;
        border-bottom: 1px solid #8dca2a;
        margin-bottom: 24px;
        padding-bottom: 4px;
    }
</style>
    <div class="row" style="margin-bottom: 2rem;">
        <div class="col-md-12">
            <a href="/workers-as-learner.html?lang=pl" class="btn btn-success">Back to YOUTH-WORKER AS LEARNER menu</a>
        </div>
    </div>

<div class="row">
    <div class="col-3 small curriculum-menu">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-tab-0" data-toggle="pill" href="#v-pills-0" role="tab" aria-controls="v-pills-0" aria-selected="true">Home</a>
            <a class="nav-link" id="v-pills-tab-1" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="false">Skróty z jęzka angielskiego</a>
            <a class="nav-link" id="v-pills-tab-2" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">1. Wprowadzenie do projektu</a>
            <a class="nav-link" id="v-pills-tab-3" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">2. Cele i treść programu nauczania</a>
            <a class="nav-link" id="v-pills-tab-4" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">3. Rezultaty uczenia się</a>
            <a class="nav-link" id="v-pills-tab-5" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false">4. Strategie nauczania i uczenia się</a>
            <a class="nav-link" id="v-pills-tab-6" data-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-6" aria-selected="false">5. Strategia w zakresie ewaluacji</a>
            <a class="nav-link" id="v-pills-tab-7" data-toggle="pill" href="#v-pills-7" role="tab" aria-controls="v-pills-7" aria-selected="false">6. Zasoby do nauczania i uczenia się</a>
            <a class="nav-link" id="v-pills-tab-8" data-toggle="pill" href="#v-pills-8" role="tab" aria-controls="v-pills-8" aria-selected="false">7. Plan szkolenia dla pracownika młodzieżowego</a>
            <a class="nav-link" id="v-pills-tab-9" data-toggle="pill" href="#v-pills-9" role="tab" aria-controls="v-pills-9" aria-selected="false">8. MODUŁ I</a>
            <a class="nav-link" id="v-pills-tab-10" data-toggle="pill" href="#v-pills-10" role="tab" aria-controls="v-pills-10" aria-selected="false">9. MODUŁ II</a>
            <a class="nav-link" id="v-pills-tab-11" data-toggle="pill" href="#v-pills-11" role="tab" aria-controls="v-pills-11" aria-selected="false">10. MODUŁ III</a>
        </div>
        <div class="text-center">
            <hr />
            <h4>Pobierz program nauczania</h4>
            <a href="/files/Self-E%20Curriculum%20Final_PL.pdf" target="_blank">PDF</a> |
            <a href="/files/Self-E%20Curriculum%20Final_PL.docx" target="_blank">Word</a>
        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-0" role="tabpanel" aria-labelledby="v-pills-0">
                <h1>Program nauczania dla pracowników młodzieżowych w ramach kursu
                    SELF-E
                </h1>

                <em>Mentoring społeczny, jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia</em>

                <table class="partners-table mt-5">
                <tr><td>SOCIAL INNOVATION FUND (LT)</td><td><img src="img/partners/logo-sif.png" /></td></tr>
                <tr><td>CARDET (CY)</td><td><img src="img/partners/logo-c.png" /></td></tr>
                <tr><td>CWEP (PL)</td><td><img src="img/partners/logo-cwep.png" /></td></tr>
                <tr><td>KNOW AND CAN ASSOCIATION (BG)</td><td><img src="img/partners/logo-kc.png" /></td></tr>
                <tr><td>KNJUC (LT)</td><td><img src="img/partners/logo-knjuc.png" /></td></tr>
                <tr><td>VINI (LT)</td><td><img src="img/partners/logo-vini.png" /></td></tr>
                </table>

            </div>
            <div class="tab-pane fade" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1">
                <h3>Skróty z jęzka angielskiego</h3>
                <p>EU – European Union<br />
                ICT – Information and communications technologies<br />
                LSE – Life-Style Entrepreneurship<br />
                NEET – Not in education, Employment or Training<br />
                NGO – Non-governmental organization<br />
                OER – Open education resource.<br />
                SE – Self-employment<br />
                </p>
            </div>
            <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2">
                <h3>1. Wprowadzenie do projektu</h3>
                <p><strong>CEL GŁÓWNY PROJEKTU</strong></p>
                <p>Głównym celem projektu SELF-E jest promowanie pracy pracowników młodzieżowych oraz wspieranie samozatrudnienia opartego na stylu życia wśród młodych ludzi o mniejszych szansach, w tym młodzieży NEET. Projekt SELF-E wpisuje się w strategię ramową ET 2020, która podkreśla znaczenie indywidualnych umiejętności w erze gospodarki opartej na wiedzy.</p>
                <p>Projekt umożliwi młodzieży o mniejszych szansach zwiększenie ich udziału w rynku pracy, ponieważ uzyskają oni adekwatne umiejętności i wysokiej jakości kompetencje, potrzebne do podjęcia pracy na własny rachunek, a także będą zmotywowani przez mentorów do samozatrudnienia opartego na ich stylu życia.</p>
                <p>Projekt ma także za założenie zmniejszenie bezrobocia i lepszą integrację opisanej wyżej grupy docelowej na rynku pracy ze społeczeństwem, a także zwiększenie stopy zatrudnienia do 75%, (jeden z celów strategii Europe 2020).</p>
                <p><strong>ZAŁOŻENIA</strong></p>
                <ol>
                    <li>Zwiększenie zdolności pracowników młodzieżowych do organizowania innowacyjnych szkoleń o charakterze pozaformalnego uczenia się na temat samozatrudnienia młodzieży, z wykorzystaniem mentoringu.</li>
                    <li>Wspieranie pracowników młodzieżowych w stosowaniu nowych metod motywowania młodych ludzi o mniejszych szansach, w tym młodzieży NEET, aby uczyć się i prowadzić działalność na własny rachunek (ze szczególnym naciskiem na przedsiębiorczość typu life-style).</li>
                    <li>Ułatwienie młodzieży ich drogi do dorosłości i integrację na rynku pracy poprzez samozatrudnienie w oparciu o styl życia.</li>
                    <li>Rozwój kompetencji młodzieżowych &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221;.</li>
                    <li>Stworzenie możliwości walidacji uzyskanych kompetencji „Inicjatywność i przedsiębiorczość.”</li>
                </ol>
                <p><strong> </strong></p>
                <p><strong>GŁÓWNE REZULTATY</strong></p>
                <p>Trzy główne rezultaty pracy intelektualnej to:</p>
                <ol>
                    <li>Zestaw narzędzi dla pracowników młodzieżowych „Mentoring społeczny, jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia &#8211; SELF-E.”</li>
                    <li>Zestaw ćwiczeń praktycznych/OER dla młodych uczniów o mniejszych szansach, w tym młodzieży NEET „Przedsiębiorczość oparta o styl życia.”</li>
                    <li>Narzędzie służące do walidacji kompetencji kluczowej „Inicjatywność i przedsiębiorczość.”</li>
                </ol>
                <p><strong>GRUPA DOCELOWA</strong></p>
                <ol>
                    <li>Pracownicy młodzieżowi pracujący z młodymi ludźmi o mniejszych szansach w:
                        <ul>
                            <li>Ośrodkach młodzieżowych</li>
                            <li>Organizacjach pozarządowych</li>
                            <li>Organizacjach zajmujących się niepełnosprawną młodzieżą</li>
                            <li>Organizacjach dla migrantów</li>
                            <li>Urzędach pracy ds. młodzieży</li>
                        </ul>
                    </li>
                    <li>Młodzież o mniejszych szansach, w tym młodzież NEET</li>
                </ol>
                <div class="alert alert-info">
                    Więcej informacji o Partnerach w projekcie, wynikach i rezultatach SELF-E można znaleźć na stronie:
                    <a href="http://self-e.lpf.lt/" target="_blank">http://self-e.lpf.lt/</a>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3">
                <h3>2. Cele i treść programu nauczania „Mentoring społeczny, jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia – SELF-E”</h3>
                <p><strong>Główne cele</strong> programu nauczania to:</p>
                <ul>
                    <li>zdefiniowanie ram szkolenia dla pracowników młodzieżowych w celu wzmocnienia ich zdolności do dostarczania innowacyjnych nieformalnych szkoleń, na temat samozatrudnienia w  oparciu o styl życia, dla młodych uczniów o mniejszych szansach, w tym młodzieży NEET;</li>
                    <li>wprowadzenie innowacyjnych ścieżek szkoleniowych opartych na mentoringu społecznym w  celu motywowania młodych ludzi o mniejszych szansach, w tym młodzieży NEET, do uczenia się o przedsiębiorczym stylu życia i bycia przedsiębiorcą.</li>
                </ul>
                <p><strong>Założenia</strong> programu nauczania są następujące:</p>
                <ul>
                    <li>opisanie zawartości modułów szkoleniowych;</li>
                    <li>wyjaśnienie celu i głównych efektów uczenia się kursu szkoleniowego SELF-E;</li>
                    <li>zdefiniowanie procesu mentoringu w zakresie samozatrudnienia w oparciu o styl życia, jako nowej nieformalnej ścieżki uczenia się;</li>
                    <li>zdefiniowanie koncepcji samozatrudnienia w oparciu o styl życia;</li>
                    <li>wprowadzenie głównej koncepcji przedsiębiorczości typu life-style, jako użytecznej alternatywy dla samozatrudnienia młodzieży na współczesnym rynku pracy;</li>
                    <li>nauczenie się, w jaki sposób motywować młodzież, aby założyli własną działalność w opaciu o  własny styl życia;</li>
                    <li>zdefiniowanie planu szkoleń w zakresie szkolenia pracowników młodzieżowych opartych na mentoringu społecznym i OER;</li>
                    <li>uświadomienie młodzieży możliwości i znaczenia walidacji ich kompetencji „ Inicjatywność i przedsiębiorczość;”</li>
                    <li>przedstawienie schematu planu lekcji szkolenia SELF-E dla młodych uczniów o mniejszych szansach, w tym młodzieży NEET, opartych na mentoringu społecznym i OER na temat samozatrudnienia w oparciu o styl życia.</li>
                </ul>
                <p>Program nauczania zawiera materiały szkoleniowe dla pracownika młodzieżowego w temacie „Mentoring społeczny, jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia – SELF-E:’</p>
                <ol>
                    <li>Mentoring społeczny na temat samozatrudnienia w zakresie stylu życia (LSE), jako nowa nieformalna ścieżka kształcenia dla młodych osób o mniejszych szansach, w tym młodzieży NEET.</li>
                    <li>Ułatwienie procesu społecznego mentoringu w obrębie LSE z wykorzystaniem otwartych zasobów edukacyjnych (OER) o przedsiębiorczym stylu życia.</li>
                    <li>Walidacja kompetencji „Inicjatynowność i przedsiębiorczość” wśród osób młodych o mniejszych szansach.”</li>
                </ol>
                <p>Kurs szkoleniowy SELF-E opiera się na mentoringu społecznym i wykorzystuje ćwiczenia dostępne w ramach otwartych zasobów edukacyjnych (OER).</p>
                <p>Szkolenie SELF-E trwa 27 godzin, co obejmuje zarówno naukę w tradycyjnym środowisku lekcyjnym, jak i online. Strategie nauczania i uczenia się zostały wyjaśnione w 4 części programu nauczania.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4">
                <h3>3. Rezultaty uczenia się</h3>
                <p><img src="img/curriculum/image007.jpg" class="float-left"/>Po pomyślnym ukończeniu szkolenia „Mentoring społeczny jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia – SELF-E,” pracownicy młodzieżowi poprawią swoje kompetencje mentora społecznego w zakresie samozatrudnienia w opaciu o styl życia (LSE) i będą w stanie skutecznie zorganizować szkolenia dla młodych osób o mniejszych szansach (w tym NEETs).</p>
                <p>Pod koniec kursu pracownicy młodzieżowi będą posiadać wiedzę/umiejętności, by:</p>
                <ul>
                    <li>Rozumieć koncepcję samozatrudnienia w oparciu o styl życia.</li>
                    <li>Konceptualizować główną koncepcję przedsiębiorczości w oparciu o styl życia, jako użyteczną alternatywę samozatrudnienia młodzieży na współczesnym rynku pracy.</li>
                    <li>Analizować różne typy LSE w obrębie samozatrudnienia.</li>
                    <li>Rozumieć konteksty biznesowe LSE i utworzyć biznesplany.</li>
                    <li>Opracowywać, wdrażać i monitorować strategie marketingowe dla LSE.</li>
                    <li>Zdefiniować miękkie umiejętności, potrzebne, aby prowadzić przedsiębiorczy styl życia.</li>
                    <li>Wykorzystać różne metody i narzędzia, celem zwiększenia motywacji młodych ludzi o mniejszych szansach, w tym młodzieży niekształcącej się i niepracującej (NEET), aby zakładali własne działalności, jednocześnie odkrywając nowe możliwości biznesowe.</li>
                    <li>Korzystać z mediów społecznościowych w celu promowania przedsiębiorczego stylu życia.</li>
                </ul>
                <p>Szkolenie SELF-E dostarcza teoretycznych materiałów i praktycznych narzędzi dla pracowników młodzieżowych do pracy z grupą docelową: młodzież o mniejszych szansach, w tym NEET.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5">
                <h3>4. Strategie nauczania i uczenia się</h3>
                <p>Strategia pedagogiczna tego szkolenia polega na podejściu „blended learning:” połączenie tradycyjnego i wirtualnego uczenia się za pośrednictwem platformy e-learningowej, która jest otwartym zasobem edukacyjnym, umożliwiającym samodzielną naukę w dogodnym czasie oraz miejscu.</p>
                <div class="alert alert-info">
                    <strong>Blended learning</strong> to program edukacyjny (formalny lub nieformalny), który łączy internetowe media cyfrowe z tradycyjnymi metodami nauczania w klasie. Wymaga fizycznej obecności nauczyciela i ucznia, z pewnymi elementami kontroli nad czasem, miejscem, ścieżką lub tempem nauczania.
                </div>
                <div class="row">
                    <div class="col-md-5"><img class="img-fluid" src="img/curriculum/image009.png"/></div>
                    <div class="col-md-7"><img class="img-fluid" src="img/curriculum/image008_pl.png"/></div>
                </div>
                <div class="alert alert-info">
                    <strong>Otwarte zasoby edukacyjne </strong> (OER) to materiały cyfrowe, które mogą być ponownie wykorzystane do nauczania, nauki, badań i innych celów, udostępniane bezpłatnie za pośrednictwem otwartych licencji, umożliwiających korzystanie z materiałów, które nie byłyby natychmiast dostępne na mocy samych praw autorskich. OER to kompletne kursy, materiały szkoleniowe, moduły, podręczniki, wideo streaming, testy, oprogramowanie i wszelkie inne narzędzia, materiały lub techniki wykorzystywane do wspierania dostępu do wiedzy.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image010.jpg" class="img-fluid"/>
                    <p>https://www.google.es/search?q=open+education+resources&amp;source=lnms&amp;tbm=isch&amp;sa=X&amp;ved=0ahUKEwi6vYTl_fPcAhVFtosKHWNGDNsQ_AUICigB&amp;biw=1920&amp;bih=974#imgrc=Cl32e5SzCoJjFM:</p>
                </div>
                <p>Kurs jest innowacyjny, ponieważ opiera się na technologiach informacyjno-komunikacyjnych oraz <strong>metodologii odwróconego szkolenia</strong> z wykorzystaniem otwartych zasobów edukacyjnych (OER). </p>
                <div class="alert alert-info">
                    <strong>Odwrócona metodologia</strong> szkolenia oznacza, że wychowawca pełni rolę facylitatora i instruuje kursantów/uczniów (osoby pracujące z młodzieżą), jak samodzielnie dokonać wstępnej analizy materiałów szkoleniowych on-line dostępnych jako otwarte zasoby edukacyjne. Następnie uczestnicy omawiają wyniki z prowadzącym podczas spotkań w klasie lekcyjnej. Oznacza to, że kursanci (osoby pracujące z młodzieżą) używają <strong>odwróconej metody uczenia (ang. flipped learning model)</strong> w celu poprawy swoich umiejętności i kompetencji.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image011.png" class="img-fluid"/>
                    <p>Źródło: https://sites.google.com/a/mahidol.edu/how-gen-z-learn-by-il-mu/techforactivelearning/7&#8212;tools-for-flipped-classroom</p>
                </div>
                <p>W celu pogłębienia wiedzy w temacie każdego z modułów warto skorzystać z opracowanego <strong>zestawu narzędzi e-Toolkit dla pracowników młodzieżowych</strong>. E-learning jest bardzo ważny podczas części szkoleniowej, która obejmuje łącznie 27 godzin zajęć. Materiały metodyczne dla osób pracujących z młodzieżą, przyszłych menedżerów lub mentorów dla młodzieży w trudnej sytuacji składają się z teorii i zadań praktycznych. Aby zapewnić aktywny udział osób pracujących z młodzieżą i pełne zrozumienie treści modułów, cztery sesje szkoleniowe (łącznie 8 godzin zajęć) i trzy sesje online (łącznie 19 godzin zajęć) są włączone do strategii nauczania oraz uczenia się.</p>
                <ul>
                    <li>Pierwsza sesja – w klasie lekcyjnej – polega na ocenie kompetencji pracownika młodzieżowego, aby stać się mentorem LSE i prowadzić kurs szkoleniowy dla uczniów z grup defaworyzowanych. Tu przygotowany został Moduł I „Mentoring dotyczący samozatrudnienia w oparciu o styl życia, jako nowej ścieżki pozaformalnego kształcenia dla młodzieży o  mniejszych szansach, w tym NEET.”</li>
                    <li>Druga sesja – w klasie lekcyjnej – polega na pogłębieniu wiedzy uczestników (pracowników młodzieżowych) poprzez praktyczne ćwiczenia, po zapoznaniu się z materiałem I modułu online. Tu przygotowany został Moduł II „Ułatwianie procesu mentoringu społecznego LSE przy użyciu otwartych zasobów edukacyjnych (OER) dotyczących przedsiębiorczości typu life-style.” Przedstawione zostają także otwarte zasoby edukacyjne (36 przykładów), a uczestnicy testują co najmniej jeden z nich, następnie wypełniając arkusz oceniający, opracowany dla każdego z OER.</li>
                    <li>Trzecia sesja – w klasie lekcyjnej – składa się z dwóch części:</li>
                    <ul>
                        <li>Analizy znaczenia OER dla ułatwienia mentoringu LSE.</li>
                        <li>Wprowadzenia do modułu III Walidacja kompetencji „Inicjatywność i przedsiębiorczość.”</li>
                    </ul>
                    <li>Czwarta sesja – w klasie lekcyjnej – jest sesją końcową i składa się z kilku ćwiczeń z Modułu III. Służy gromadzeniu informacji zwrotnych na temat ogólnego szkolenia i dyskusji na temat planów organizacji procesu mentoringu LSE dla młodzieży w trudnej sytuacji. Uczestnicy oceniają również swoje kompetencje i wpływ szkolenia, korzystając z narzędzia do samooceny. Na zakończenie tej sesji zaleca się przekazanie uczestnikom certyfikatów dotyczących ukończenia szkolenia „Mentoring społeczny, jako innowacyjna ścieżka kształcenia w zakresie przedsiębiorczości opartej o styl życia – SELF-E.”</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6">
                <h3>5. Strategia w zakresie ewaluacji</h3>
                <p>Ocena wiedzy i kompetencji uczestników zostanie przeprowadzona na trzy sposoby (po pomyślnym ukończeniu kursu uczestnicy otrzymają certyfikaty potwierdzające uzyskane kompetencje):</p>
                <ul>
                    <li>przeprowadzenie testu samooceny on-line (ocena wstępna i po ewaluacji);</li>
                    <li>uczestniczenie w pracach grupowych i wypełnianie zadań praktycznych podczas sesji w klasie;</li>
                    <li>refleksja nad własnymi doświadczeniami edukacyjnymi po szkoleniu.</li>
                </ul>
                <p>Na początku kursu szkoleniowego SELF-E uczestnicy zostaną poproszeni o wykonanie testu samooceny on-line w celu określenia poziomu ich wiedzy i kompetencji w temacie materiału szkoleniowego. Pod koniec kursu zostaną poproszeni o ponowne wykonanie testu samooceny on-line w celu oceny postępów w nauce.</p>
                <p><img src="img/curriculum/image012.png" class="float-left" />W ocenie wstępnej i ocenie końcowej oceniane są 3 aspekty:</p>
                <ul>
                    <li>Znajomość procesu mentoringu;</li>
                    <li>Wiedza na temat przedsiębiorczości w oparciu o styl życia;</li>
                    <li>Wiedza potwierdzająca kompetencje „Inicjatywność i przedsiębiorczość.”</li>
                </ul>
                <p>Powstało 10 pytań związanych z oceną wiedzy z zakresu mentoringu, 10 pytań związanych z oceną wiedzy na temat przedsiębiorczości w oparciu o styl życia i 5 pytań związanych z oceną wiedzy na temat walidacji kompetencji „Inicjatywność i przedsiębiorczość,” w sumie 25 pytań zamkniętych. To narzędzie oceny zostanie zaprogramowane i będzie dostępne on-line bezpłatnie.</p>
                <p>Jak wspomniano powyżej, druga metoda oceny polega na tym, że uczestnicy podczas sesji w klasie wykonają zadania praktyczne, wezmą udział w ćwiczeniach w grupach, w autorefleksji oraz dyskusji.</p>
                <p>Po trzecie, pod koniec sesji końcowej uczestnicy zostaną poproszeni o zastanowienie się nad swoimi doświadczeniami edukacyjnymi podczas szkolenia, zarówno tymi w klasie, jak i online. Uczestnicy podzielą się tym, jak skutecznie wykorzystali wiedzę zdobytą podczas szkolenia. Działanie to będzie promować dalszą naukę i praktyczne wykorzystanie wiedzy i kompetencji w codziennych zajęciach dydaktycznych/szkoleniowych.</p>
                <p>Uczestnicy, którzy udzielili wymaganych poprawnych odpowiedzi, na co najmniej 20 pytań w końcowym teście samooceny i byli aktywni w praktycznych działaniach i refleksjach, będą certyfikowani.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-7">
                <h3>6. Zasoby do nauczania i uczenia się</h3>
                <p>Instytucje, które będą organizować kursy szkoleniowe SELF-E dla pracowników młodzieżowych &#8211; kursantów powinny zapewnić dogodne środowisko do uczenia się, wyposażenie techniczne i narzędzia niezbędne do zapewnienia procesu nauczania i uczenia się w oparciu o podejście oparte na mieszanym uczeniu się. Wymagane są:</p>
                <ul>
                    <li>sala lekcyjna z projektorem multimedialnym i komputerem z możliwością używania Power Point do spotkań w klasie;</li>
                    <li>komputery osobiste z dostępem do Internetu oraz otwartych zasobów edukacyjnych i platformy edukacyjnej projektu SELF-E (<a href="http://self-e.lpf.lt/">http://self-e.lpf.lt/</a>);</li>
                    <li>inne zasoby organizacyjne do spotkań w klasie (tablica, materiały informacyjne, papier itp.).</li>
                </ul>
                <p>Trenerzy kursu SELF-E powinni być odpowiednio przygotowani do procesu dydaktycznego.</p>
                <p>Powinni:</p>
                <img src="img/curriculum/image013.jpg" class="float-right" />
                <ul>
                    <li>stworzyć przyjazne przychologiczne srodowisko uczenia się;</li>
                    <li>zapoznać się z rodzajem odbiorców i mieć świadomość ich potrzeb i oczekiwań;</li>
                    <li>mieć doświadczenie w zakresie pracy z młodzieżą o mniejszych szansach, w tym młodzieży NEET;</li>
                    <li>posiadać wiedzę i umiejętności potrzebne do organizowania sesji e-learningowych;</li>
                    <li>mieć dobrą znajomość w zakresie treści nauczania (moduły I-III);</li>
                    <li>posiadać takie cechy osobiste jak: dobre umiejętności komunikacyjne, tolerancyjnosć, zdolność motywowania uczniów oraz zdolności do samooceny;</li>
                    <li>posiadać dobre umiejętności do roli facylitatora, takie jak umiejętności inicjowania dyskusji, reagowania na opinie, udzielania wsparcia, itp.</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-8" role="tabpanel" aria-labelledby="v-pills-8">
                <h3>7. Plan szkolenia dla pracownika młodzieżowego</h3>
                <p>Plan szkoleń został opracowany, aby pomóc w organizacji szkoleń dla pracowników młodzieżowych, jak skutecznie dostarczać szkolenia "SELF-E" dla młodzieży z mniejszymi szansami. Plan krok po kroku przedstawia cały proces mieszanego uczenia się, wykorzystujący odwróconą metodologię szkolenia, zdefiniowaną w części 4.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>Nr</strong></td>
                        <td><strong>Metoda</strong></td>
                        <td><strong>Czas (*a. h)</strong></td>
                        <td><strong>Treści/Tematy</strong></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>W klasie</td>
                        <td>2</td>
                        <td>
                            <p>Wprowadzenie do kursu szkoleniowego SELF-E dla pracowników młodzieżowych. Wstępna ocena kompetencji pracownika młodzieżowego, aby zostać mentorem LSE i prowadzić kurs szkoleniowy dla uczniów z grup defaworyzowanych.</p>
                            <p>Wprowadzenie do Modułu I „Mentoring społeczny na temat samozatrudnienia w oparciu o styl życia, jako nowej nieformalnej ścieżki uczenia się.”</p>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Sesja online</td>
                        <td>6</td>
                        <td>
                            <p>Samouczenie się Modułu I.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>W klasie</td>
                        <td>2</td>
                        <td>
                            <p>Ćwiczenia grupowe w zakresie Modułu I, dyskusje. Wprowadzenie Modułu II „Ułatwianie procesu społecznego mentoringu LSE za pomocą otwartych zasobów edukacyjnych (OER) na temat przedsiębiorczego stylu życia.” Wypełnienie, co najmniej jednego arkusza oceny dla wybranego OER.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Sesja online</td>
                        <td>9</td>
                        <td>
                            <p>Samouczenie się Modułu II. Wypełnienie arkuszy oceny dla wszystkich OER, co umożliwia przejście do kolejnej, piątej sesji.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>W klasie</td>
                        <td>2</td>
                        <td>
                            <p>Dyskusje na temat efektów samouczenia się w Module II na podstawie danych z arkuszu oceny wypełnionego przez uczniów. Przeanalizowanie znaczenia OER dla ułatwienia mentoringu LSE. Wprowadzenie do Modułu III Walidacja kompetencji „Inicjatywność i przedsiębiorczość.”</p>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Sesja online</td>
                        <td>4</td>
                        <td>
                            <p>Samouczenie się Modułu III.  Wypełnienie testu przez uczestników kursu szkoleniowego SELF-E.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>W klasie</td>
                        <td>2</td>
                        <td><p>Sesja końcowa:</p>
                            <ul>
                                <li>Ćwiczenia grupowe w zakresie Modułu III.</li>
                                <li>Zbieranie opinii na temat całego kursu szkoleniowego, dyskusje.</li>
                                <li>Ocena końcowa przez kursantów.</li>
                                <li>Nagrody - certyfikaty.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Suma</strong></td>
                        <td class="text-center"><strong>27</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <p class="text-right small"><em>*Uwaga: 1 godzina akademicka = 45 minut</em></p>
                <p>Jak wynika w powyższej tabeli, istnieje siedem głównych etapów.</p>
                <img src="img/curriculum/image014.jpg" class="float-right" />
                <ol class="curriculum-plan-list">
                    <li>Podczas pierwszej sesji bezpośredniej podane jest krótkie wprowadzenie do kursu szkoleniowego SELF-E. Uczestnicy są zaproszeni do wstępnej oceny kompetencji pracownika młodzieżowego, aby zostać mentorem LSE i poprowadzić szkolenia dla uczniów z grup defaworyzowanych. Po jej ukończeniu są wprowadzani do Modułu I: „Mentoring społeczny na temat samozatrudnienia w oparciu o styl życia, jako nowej nieformalnej ścieżki uczenia się.”</li>
                    <li>Po sesji w klasie następuje sesja online dotycząca Modułu I, do samodzielnej nauki.</li>
                    <li>Podczas drugiej sesji w klasie, prowadzone są ćwiczenia w temacie Modułu I (w grupach) i  dyskusje, aby sprawdzić, czego się kursanci nauczyli, oraz aby mogli podzielić się doświadczeniami z procesu samodzielnego uczenia się i zadać pytania. Następnie wprowadzony zostaje Moduł II „Wspieranie procesu społecznego mentoringu LSE za pomocą otwartych zasobów edukacyjnych (OER) na temat przedsiębiorczego stylu życia.” Uczestnicy dzielą się swoją opinią i doświadczeniem w zakresie, co najmniej jednego OER, wypełniając arkusz oceny.</li>
                    <li>Po sesji w klasie następuje sesja online dla Modułu II. Jest to sesja do samodzielnej nauki. Oprócz studiowania materiału teoretycznego do Modułu II, uczniowie wypełniają arkusze oceny dla wszystkich OER, aby móc przejść do kolejnej sesji. Wielką wagę przywiązuje się do arkuszy oceny, co wymaga od uczniów robienia notatki podczas sesji, ale w ten sposób uczniowie łatwiej podzielą się swoimi doświadczeniami w korzystaniu z OER, co również ułatwi proces mentoringu, a także sprawi, że dyskusje podczas trzeciej sesji w klasie będą lepiej zorganizowane.</li>
                    <li>Podczas trzeciej sesji w klasie uczestnicy dzielą się swoim doświadczeniem z samodzielnego uczenia się Modułu II na podstawie wypełnionych arkuszy oceny. Omawiają i analizują znaczenie OER, aby ułatwić mentoring LSE. Zostają również wprowadzeni do Modułu III Walidacja kompetencji „Inicjatywność i przedsiębiorczość.”</li>
                    <li>Po sesji w klasie następuje sesja online dla Modułu III. Oprócz nauki materiałów teoretycznych, uczniowie proszeni są o wypróbowanie testu on-line do oceny umiejętności deforyzowanej młodzieży w zakresie kompetencji „Inicjatywność i przedsiębiorczość.” To ważny etap, ponieważ pracownicy młodzieżowi są przygotowani do przeprowadzania testów wśród szczególnej grupy młodzieży.</li>
                    <li>Podczas sesji końcowej uczniowie uczestniczą w ćwiczeniach grupowych w zakresie Modułu III, omawiają i dzielą się swoimi doświadczeniami z samokształcenia i korzystania z narzędzia oceny. Aby ocenić postępy szkolenia, uczniowie wypełnią ewaluacyjną ankietę końcową, co uprawnia do uzyskania certyfikatów.</li>
                </ol>
            </div>
            <div class="tab-pane fade" id="v-pills-9" role="tabpanel" aria-labelledby="v-pills-9">
                <h3>8. MODUŁ I. „Mentoring społeczny w zakresie samozatrudnienia opartego na stylu życia, jako nowa ścieżka kształcenia nieformalnego”</h3>
                <p><strong>Celem modułu jest:</strong></p>
                <p>Wprowadzenie pracowników młodzieżowych do mentoringu społecznego dotyczącego samozatrudnienia w stylu życia jako nowej nieformalnej ścieżki uczenia się w celu zachęcania, promowania i wspierania rozwoju osobistego i zawodowego osób młodych w celu rozpoczęcia własnej działalności gospodarczej.</p>
                <p><strong>Cele modułu I są następujące:</strong></p>
                <ul>
                    <li>zdefiniowanie koncepcji mentoringu na temat samozatrudnienia w zakresie stylu życia: dlaczego mentoring społeczny jest innowacyjną i wartościową pozaformalną ścieżką uczenia się dla samozatrudnienia młodych ludzi o mniejszych szansach, w tym młodzieży NEET;</li>
                    <li>określenie głównych ról i zadań podopiecznego i mentora w mentoringu społecznym na temat samozatrudnienia w zakresie stylu życia;</li>
                    <li>wyraźne wyjaśnienie głównych ról i zadań mentora w mentoringu społecznym na temat samozatrudnienia w zakresie stylu życia;</li>
                    <li>wyjaśnienie korzyści i wyzwań związanych z mentoringiem w zakresie samozatrudnienia na rzecz stylu życia;</li>
                    <li>określenie głównych etapów mentoringu społecznego w procesie LSE;</li>
                    <li>określenie sposobów zapewnienia skutecznego mentora społecznego w LSE;</li>
                    <li>wprowadzenie metod wyboru mentorów i dopasowywania ich do podopiecznych młodych ludzi i wyjaśnienie, w jaki sposób zapewnić pożądane standardy jakości mentora społecznego w zakresie samozatrudnienia na rzecz stylu życia;</li>
                    <li>wprowadzenie sposobu organizacji początkowych, średnioterminowych i końcowych sesji mentoringu społecznego na temat samozatrudnienia w stylu życia;</li>
                    <li>określenie celów monitorowania jakości mentoringu społecznego w zakresie samozatrudnienia na rzecz stylu życia;</li>
                    <li>zdefiniowanie kodeksów etycznych i zawodowych zarówno dla mentorów młodzieżowych, jak i młodych podopiecznych;</li>
                    <li>wprowadzenie narzędzi zarządzania dla mentoringu społecznego w zakresie samozatrudnienia w zakresie stylu życia.</li>
                </ul>
                <p><strong>Opis modułu I:</strong></p>
                <p>W tym module uczeń zrozumie znaczenie mentoringu &#8211; jest to partnerstwo rozwojowe, dzięki któremu jedna osoba dzieli się wiedzą, umiejętnościami, informacjami i perspektywą, aby wspierać rozwój osobisty i zawodowy kogoś innego. Analizowane są różne typy mentoringu, ponieważ istnieją dwa typy: para (indywidualny) i grupowy. Mentoring również może mieć wiele formuł: np. face-to-face, e-mentoring i mieszany mentoring. Studia przypadków dla osób uczących się i materiały teoretyczne będą miały możliwość ich zbadania.</p>
                <p>Osoby uczące się w tym module zrozumieją, w jaki sposób wspierają mentorów, mają odwagę i otwierają własne sieci dla Mentees. Szczególna uwaga zostanie poświęcona mentoringowi w kwestiach związanych z zatrudnieniem, gdzie mentorzy pomagają osobom mentorowanym w znalezieniu zatrudnienia lub założeniu przedsiębiorstwa.</p>
                <p>Proces mentoringu analizowany jest w tym module z obu perspektyw &#8211; Mentora i Mentee, ponieważ wymaga zarówno zaangażowania strony, szacunku, odpowiedzialności i równych oczekiwań. Silny związek między podopiecznym, mentorem i menedżerem jest najważniejszym punktem udanego mentoringu. Dla Mentee to proces oferujący doskonałe możliwości rozwoju osobistego i zawodowego. Wspierana przez Mentora, osoba mentorowana może rozwijać swoje umiejętności i kompetencje związane z zatrudnieniem. Mentor również otrzymuje korzyści, które są dostarczane i omawiane z uczniami w tym module.</p>
                <p>Uczniowie dowiedzą się o skutecznych rozmowach odbywających się w trakcie relacji mentoringu &#8211; że powinny być istotne dla podstawowego celu, a osoba mentorowana powinna skoncentrować się na planie przedsiębiorczości i kwestiach, które na niego wpływają.</p>
                <p>Korzyści mentorskie wyjaśnione są również w tym module, ponieważ często podnosi ono zrozumienie między pokoleniami, kulturami i różnymi rodzajami ludzi. Mentoring jest dobrą metodą uczenia się i rozwoju, a dla Mentee daje odwagę sprostać ewentualnym konfliktom i w razie potrzeby podejmować ryzyko.</p>
                <p><strong>Wyniki nauki</strong></p>
                <p>Pod koniec modułu I uczestnicy (osoby pracujące z młodzieżą) będą mogli:</p>
                <ul>
                    <li>rozumieć koncepcje mentoringu na temat samozatrudnienia w zakresie przedsiębiorczości opartej o styl życia, stylu życia podopiecznego, mentora i menedżera;</li>
                    <li>rozróżniać role menedżera, mentora i podopiecznego w procesie mentoringu społecznego w  zakresie przedsiębiorczości opartej o styl życia.</li>
                    <li>umieć identyfikować i wyjaśniać zalety, mocne strony i wyzwania mentoringu społecznego w zakresie przedsiębiorczości opartej o styl życia.</li>
                    <li>rozumieć znaczenie ułatwiania narzędzi wykorzystywanych podczas procesu mentoringu społecznego w zakresie przedsiębiorczości opartej o styl życia.</li>
                    <li>być w stanie skutecznie organizować i zarządzać procesem społecznego mentoringu na temat przedsiębiorczości opartej o styl życia (w tym dopasowywanie podopiecznych i mentorów, organizowanie sesji początkowej, monitorowanie w połowie okresu i sesje końcowe);</li>
                    <li>umieć ustanowić standardy jakości oraz kodeksy etyczne i zawodowe dla menedżerów i mentorów.</li>
                </ul>
                <p>Opieka społeczna opiera się na podejściu &#8220;blended learning&#8221;: połączenie tradycyjnej i wirtualnej nauki poprzez rozwiniętą platformę e-learningową z zestawem OER. Moduł II przedstawia sposób korzystania z OER w celu ułatwienia mentoringu społecznego.</p>
                <p><strong>GŁÓWNE ETAPY SZKOLENIA PRZY WYKORZYSTANIU MENTORINGU:</strong></p>
                <img src="img/curriculum/image015_pl.png" class="img-fluid" />
            </div>
            <div class="tab-pane fade" id="v-pills-10" role="tabpanel" aria-labelledby="v-pills-10">
                <h3>9. Moduł II "Ułatwianie procesu społecznego mentoringu na LSE poprzez wykorzystanie zestawu otwartych zasobów edukacyjnych (OER) w zakresie przedsiębiorczości typu life-style"</h3>
                <p><strong>Celem modułu jest:</strong></p>
                <p>Wprowadzenie koncepcji samozatrudnienia w stylu życia ze szczególnym uwzględnieniem przedsiębiorczości typu life-style.</p>
                <p><strong>Cele modułu II są następujące:</strong></p>
                <ul>
                    <li>zdefiniowanie koncepcji samozatrudnienia w stylu życia;</li>
                    <li>wprowadzenie głównych cech przedsiębiorczości typu life-style jako użytecznej alternatywy dla samozatrudnienia wśród młodzieży na dzisiejszym rynku pracy;</li>
                    <li>wprowadzenie metod motywowania młodzieży do samodzielnego stylu życia;</li>
                    <li>zapoznanie się z różnymi rodzajami przedsiębiorczości typu life-style za pomocą historii sukcesu;</li>
                    <li>określenie głównych zasad biznesowych w LSE i głównych części biznesplanu;</li>
                    <li>analizowanie strategii marketingowych w LSE;</li>
                    <li>wprowadzenie zestawu ćwiczeń praktycznych jako zestawu ścieżek OER dla &#8220;samozatrudnienia&#8221; do wykorzystania, w celu ułatwienia procesu społecznego mentoringu na LSE;</li>
                    <li>przeanalizowanie planu lekcji na innowacyjny &#8220;kurs szkoleniowy SELF-E&#8221; dla młodych ludzi o mniejszych szansach, w tym młodzieży NEET, w oparciu o mentoring społeczny i OER.</li>
                </ul>
                <p><strong>Opis modułu II:</strong></p>
                <p>W tym module uczeń porówna główne zasady przedsiębiorczości z koncepcją przedsiębiorczości typu life-style. Ogólna definicja przedsiębiorcy jest podana i wyjaśniona w konkretnych przypadkach. Analizowane są podobieństwa i różnice między przedsiębiorczością a przedsiębiorczością typu life-style. Narzędzia oceny dla możliwości stania się przedsiębiorcą stylu życia są podane, ponieważ nie każdy może być przedsiębiorcą prowadzącym styl życia – wymaga to szczególnych cech osobistych, a także pragnienia, aby biznes powstał z ich pasji, niezależnie od tego, czy jest to zysk finansowy czy społeczny, czy też zapobieganie izolacji.</p>
                <p>Również w module II przedstawiono biznesowe zasady przedsiębiorczości, które są niezbędne dla wszystkich rodzajów działalności, w tym przedsiębiorczości typu life-style. Ważne jest zrozumienie zasad prowadzenia i zarządzania przedsiębiorstwem, w tym ustalenie misji biznesowej, ukończenie gruntownych badań rynkowych, opracowanie strategii marketingowej, planowanie finansowe i prognozowanie, zarządzanie ludźmi i tak dalej. Biznesplan to nie tylko narzędzie do pozyskiwania funduszy. W rzeczywistości jest to narzędzie do zrozumienia, w jaki sposób i dlaczego firma jest tworzona. Może służyć do monitorowania postępu; wymusza regularne przeglądy propozycji wartości, założeń marketingowych, planu operacyjnego i planu finansowego.</p>
                <p>Przedsiębiorczość w stylu życia może być przydatna jako alternatywa dla przedsiębiorczości opartej o styl życia. W związku z tym moduł II dostarcza wielu różnych czynników i barier, z  jakimi musi się zmierzyć przedsiębiorczość w stylu życia i dlaczego postanawiają rozpocząć działalność.</p>
                <p>Moduł II będzie wykorzystywał różne metody uczenia się, gdy uczący się angażuje się w działania poprzez interaktywne sesje, w tym dyskusje i quizy, metody instruktażowe, z szeregiem narzędzi on-line. Oba zostaną wykorzystane: nauka twarzą w twarz i nauka on-line. Ten moduł zaspokoi potrzeby osób pracujących z młodzieżą, aby zapoznać się z podstawową wiedzą na temat przedsiębiorczości typu life-style i samozatrudnienia w stylu życia w ogóle, a także z OER: jak korzystać z OER w procesie mentoringu?</p>
                <p>24 OZE zostaną przedstawione w trzech następujących obszarach tematycznych</p>
                <ul>
                    <li>Przedsiębiorczość i samozatrudnienie (SE)</li>
                    <li>Przedsiębiorczość typu life-style</li>
                    <li>Zasady biznesowe i marketing w przedsiębiorczości typu life-style</li>
                </ul>
                <p>12 historie sukcesu będą wykorzystywane jako narzędzie motywacji dla młodych uczniów o mniejszych szansach, w tym młodzieży NEET, aby stać się przedsiębiorcami stylu życia.</p>
                <p>Pracownik młodzieżowy zapozna się z utworzonymi zasobami OER podczas projektu SELF-E i dowie się, jak z nich korzystać w procesie mentoringu społecznego na temat samozatrudnienia stylu życia.</p>
                <p><strong>Przykładowy plan lekcji</strong> na innowacyjny kurs szkoleniowy SELF-E dla osób uczących się w niekorzystnej sytuacji oparty na mentoringu społecznym, który obejmuje sesje mentoringowe twarzą w twarz i naukę on-line przy użyciu zestawu ćwiczeń praktycznych - OER.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>Faza treningu SELF-E</strong></td>
                        <td><strong>Sesje</strong></td>
                        <td><strong>Czas trwania</strong></td>
                        <td><strong>Zawartość; OER</strong></td>
                    </tr>
                    <tr>
                        <td>Przygotowanie</td>
                        <td>&nbsp;</td>
                        <td>Co najmniej dwa tygodnie przed pierwszą sesją</td>
                        <td>
                            <ul>
                                <li>Wybór mentorów i podopiecznych;</li>
                                <li>Dopasowanie mentorów i podopiecznych</li>
                                <li>Sprawdź uprawnienia</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Wstępne spotkanie mentorskie</td>
                        <td><strong>Pierwsza sesja.</strong>
                            <p>Pierwsze spotkanie mentoringu twarzą w twarz</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Poznawanie się</li>
                                <li>Wprowadzenie do mentoringu społecznego</li>
                                <li>Krótka prezentacja kursu szkoleniowego SELF-E</li>
                                <li>Ocena kompetencji uczniów - korzystanie z narzędzia online</li>
                                <li>Opracowanie indywidualnych planów dla podopiecznego</li>
                                <li>Wprowadzenie do podręcznika dla uczących się</li>
                                <li>Ustawianie zadań dla indywidualnej sesji on-line</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Pierwszy obszar tematyczny - samokształcenie</td>
                        <td><strong>Druga sesja.</strong>
                            <p>Sesje internetowe na temat "Przedsiębiorczość i samozatrudnienie (SE)"</p></td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Samodzielna praca w trybie online w pierwszym obszarze tematycznym: "Przedsiębiorczość i samozatrudnienie (SE)"</li>
                                <li>Uczniowie wybierają OER z pierwszego obszaru tematycznego</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Pierwszy obszar tematyczny - pogłębienie wiedzy w klasie</td>
                        <td><strong>Trzecia sesja.</strong>
                            <p>Spotkanie twarzą w twarz "Przedsiębiorczość i samozatrudnienie (SE)"</p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Praca w grupie w celu pogłębienia wiedzy na temat pierwszego obszaru tematycznego poprzez praktyczne ćwiczenia</li>
                                <li>Motywacja do samozatrudnienia</li>
                                <li>Podawanie zadań dla czwartej sesji e-learningu on-line w drugim obszarze tematycznym</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Drugi obszar tematyczny - samokształcenie</td>
                        <td><strong>Czwarta sesja.</strong>.
                            <p>Sesje online na temat "Przedsiębiorczości w stylu życia"</p>
                        </td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Praca indywidualna online w drugim obszarze tematycznym: “Przedsiębiorczoś w stylu życia"”</li>
                                <li>Uczniowie wybierają OER z drugiego obszaru tematycznego</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Śródokresowy monitoring i drugi obszar tematyczny</td>
                        <td><strong>Piąta sesja.</strong>
                            <p>Spotkanie "twarzą w twarz" na temat "Przedsiębiorczości typu life-style"</p>
                            <p>Śródokresowy monitoring mentoringu</p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Praca w grupie w celu pogłębienia wiedzy na temat drugiego obszaru tematycznego poprzez praktyczne ćwiczenia</li>
                                <li>Motywacja do samozatrudnienia</li>
                                <li>Podanie zadań do sesji 6. sesji e-learningu on-line na trzecim obszarze tematycznym</li>
                                <li>Śródokresowy monitoring sesji mentorskich</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Trzeci obszar tematyczny - samokształcenie</td>
                        <td><strong>Szósta sesja.</strong>
                            <p>Sesje internetowe "Zasady biznesowe i marketing w Przedsiębiorczości opartej na stylu życia</p>
                        </td>
                        <td>8</td>
                        <td>
                            <ul>
                                <li>Samodzielna praca online w trzecim obszarze tematycznym: "Zasady biznesowe i marketing w przedsiębiorczości typu life-style"</li>
                                <li>Uczniowie wybierają OER z trzeciego obszaru tematycznego</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Trzeci obszar tematyczny – pogłębianie wiedzy w klasie</td>
                        <td><strong>Siódma sesja.</strong>
                            <p>Spotkanie face-to-face na sesjach internetowych "Zasady biznesowe i marketing w przedsiębiorczości typu life-style"</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Praca w grupie w celu pogłębienia wiedzy na temat trzeciego obszaru tematycznego poprzez praktyczne ćwiczenia</li>
                                <li>Podawanie zadań do 8. sesji e-learningu opartego na sesji e-learningowej na LSE</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Samokształcenie w zakresie hitorii sukcesu</td>
                        <td><strong>Ósma sesja.</strong>
                            <p>Sesje online na temat "Styl życia Przedsiębiorczość - droga do sukcesu"</p></td>
                        <td>6</td>
                        <td>
                            <ul>
                                <li>Samodzielna praca w trybie online w obszarze: "Historie sukcesu dotyczące samozatrudnienia opartego na przedsiębiorczości typu life-style"</li>
                                <li>Wypełnianie arkusza opinii w celu doprowadzenia go do dziewiątej sesji</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Ostatnie spotkanie mentorskie</td>
                        <td><strong>Dziewiąta sesja.</strong>
                            <p>Ostateczne spotkanie twarzą w twarz.</p>
                            <p>Finalizacja procesu mentoringu</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Opinie na temat historii sukcesu</li>
                                <li>Finalizacja kursu i planu rozwoju osobistego</li>
                                <li>Ocena kompetencji uczniów - korzystanie z narzędzia online</li>
                                <li>Zbieranie opinii i nagrody</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><strong>Suma</strong></td>
                        <td>54 godziny</td>
                        <td>2 kredyty</td>
                    </tr>
                    </tbody>
                </table>
                <p><strong>Wyniki nauki</strong></p>
                <p>Pod koniec modułu II uczestnik (pracownik młodzieżowy) powinien:</p>
                <ul>
                    <li>rozumieć koncepcję samozatrudnienia w zakresie stylu życia i być w stanie zapewnić go młodym uczniom o mniejszych szansach, w tym młodzieży NEET;</li>
                    <li>umieć wyjaśnić główne cechy przedsiębiorczości w stylu życia jako użyteczną alternatywę samozatrudnienia dla młodzieży na dzisiejszym rynku pracy;</li>
                    <li>umieć motywować młodzież, aby stała się osobą samozatrudnioną prowadzącą styl życia;</li>
                    <li>umieć przedstawić przykłady różnych rodzajów przedsiębiorczości typu life-style;</li>
                    <li>umieć wykorzystywać OZE do samodzielnego zatrudnienia w stylu życia w ramach procesu mentoringu społecznego dla młodzieży defaworyzowanej.</li>
                </ul>
                <p><em>Walidacja postępu w nauce jest bardzo ważnym kryterium motywacji uczącego się. Moduł III prezentuje metody oceny wiedzy zdobytej podczas edukacji młodzieży, na przykład testy on-line, przewodniki dla uczących się itp.</em></p>
            </div>
            <div class="tab-pane fade" id="v-pills-11" role="tabpanel" aria-labelledby="v-pills-11">
                <h3>10. MODUŁ III. Walidacja kompetencji „Inicjatywność i przedsiębiorczość"</h3>
                <p><strong>Celem modułu III jest:</strong></p>
                <p>Rozwijanie kompetencji osób pracujących z młodzieżą w celu oceny i potwierdzenia umiejętności i kompetencji młodych uczniów &#8220;Inicjatywa i przedsiębiorczość&#8221;.</p>
                <p><strong>Cele modułu III są następujące:</strong></p>
                <ul>
                    <li>dokonanie przeglądu rozwoju procesów walidacji w krajach UE;</li>
                    <li>analizowanie metody, zalety walidacji nieformalnego i pozaformalnego uczenia się młodzieży;</li>
                    <li>przedstawienie najczęściej stosowanych nieformalnych metod oceny wiedzy uzyskanej podczas edukacji młodzieży;</li>
                    <li>wprowadzenie testu on-line &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221; w celu oceny umiejętności i kompetencji w zakresie przedsiębiorczości typu życiowego młodych uczniów o mniejszych szansach;</li>
                    <li>wprowadzenie przewodnika dla uczących się &#8220;Dlaczego walidacja jest dla mnie ważna&#8221; jako narzędzia motywującego do nauki i sprawdzania poprawności;</li>
                    <li>wprowadzenie zestawu umiejętności i wiedzy, które są zawarte w teście dla uczniów młodzieżowych &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221;;</li>
                    <li>przedstawienie koncepcji i struktury portfela wiedzy do oceny umiejętności i kompetencji uczących się.</li>
                </ul>
                <p><strong>Opis modułu III:</strong></p>
                <p>Moduł zawiera teoretyczne materiały dotyczące strategii oceniania, ich celu, celów, zasad, metod i narzędzi w nieformalnej edukacji młodzieży. Moduł krótko omawia rozwój procesów walidacji w krajach UE, a także metody i korzyści walidacji nieformalnego i pozaformalnego uczenia się młodzieży. Ponadto moduł przedstawia najczęściej stosowane metody oceniania w  pozaformalnej edukacji młodzieżowej, takie jak dyskusja, test, rozmowa z nauczycielem (wywiad), analiza przypadku, obserwacja, ocena przez innych członków grupy i refleksja. Każda metoda jest krótko opisana jako taka: nazwa metody, wielkość grupy uczących się, środowisko uczenia się (sytuacja), cel i przedmiot oceny (umiejętności, wiedza, kompetencje). W tym module podkreślono również, że proces walidacji opiera się na ocenie umiejętności i kompetencji, a zatem narzędzia i metody są podobne. Jednak sama ocena jest zwykle ukierunkowana na sprawdzenie poziomu zrozumienia szkolenia i jego zawartości. Walidacja jest kolejnym krokiem i ma na celu uznanie umiejętności i kompetencji, które można uzyskać w ramach nielicznych szkoleń lub poprzez nieformalne uczenie się i życiowe doświadczenia.</p>
                <p>Moduł poświęcony jest pokazaniu znaczenia walidacji kompetencji uzyskanych przez młodzież o mniejszych szansach w ramach szkolenia SELF-E. Podkreśla się, że pracownicy młodzieżowi muszą przekonać swoich uczących się z LSE do oceny i potwierdzania kompetencji, ponieważ będzie to okazja do poprawy swoich CV poprzez dodanie ważnych umiejętności miękkich, co zwiększy ich dostęp do rynku pracy. Przedstawiono metodologię uczenia się przez doświadczenie za pomocą narzędzia do samooceny online, która pomoże pracownikom młodzieżowym zmotywować młodzież z mniejszymi szansami na sprawdzenie swoich kompetencji.</p>
                <p>Narzędzie samooceny pomaga uczniom młodszym mającym mniejsze szanse w ocenie umiejętności i wiedzy pod kątem kompetencji &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221;. Moduł przedstawia listę umiejętności z zakresu przedsiębiorczości, które są ważne dla uczniów i mogą zostać dodane do ich CV. Lista umiejętności jest wybierana przez partnerów projektu. Uczniowie zostaną poproszeni o ocenę ich miękkich umiejętności przed mentoringiem społecznym (ponowna ocena) i po procesie szkolenia (po ocenie). Szkolenie w ramach procesu mentoringu społecznego (zob. Moduł II) rozwija te umiejętności dzięki wykorzystaniu OER. Tak więc po mentoringu społecznym oczekuje się, że poziom umiejętności miękkich w zakresie kompetencji &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221; zostanie zwiększony i będzie widoczny w procesie po ocenie. Dlatego samoocena jest doskonałym narzędziem motywacyjnym dla uczniów, ponieważ pokazuje ich postępy w nauce.</p>
                <p>Przewodnik dla uczących się &#8220;Dlaczego walidacja jest dla mnie ważna?&#8221; Jest również prezentowany w tym module, ponieważ jest ważnym narzędziem motywacji młodego ucznia do uczenia się i dalszego potwierdzania zdobytych umiejętności związanych z kompetencją &#8220;Inicjatywa i poczucie przedsiębiorczości&#8221;. Należy podkreślić, że koncepcja portfela wiedzy, jego struktura i znaczenie w dalszym potwierdzaniu umiejętności i kompetencji uczących się zostały również wprowadzone w przewodniku dla uczących się.</p>
                <p>Jako praktyczna część Modułu III, pracownicy młodzieżowi samodzielnie wykonają test samooceny, aby nauczyć się, jak zapewnić młodym ludziom liczne możliwości podczas szkolenia SEL-E. Pracownicy młodzieżowi dowiedzą się, jak mierzyć wpływ szkolenia w zakresie szkoleń SELF-E na młodych ludzi, oceniając ich umiejętności w zakresie przedsiębiorczości przed i po szkoleniu SELF-E, korzystając z internetowego testu oceny SELF-E, który składa się z co najmniej 30 pytań. Podczas sesji twarzą w twarz, pracownicy młodzieżowi pogłębią swoją wiedzę na temat oceny i zatwierdzania kompetencji przedsiębiorczych młodych ludzi.</p>
                <p><strong>Wyniki nauki</strong></p>
                <p>Pod koniec modułu III pracownik młodzieżowy powinien:</p>
                <ul>
                    <li>Znać przepisy UE dotyczące walidacji uczenia się pozaformalnego i  nieformalnego w  kontekście przedsiębiorczości.</li>
                    <li>Znać różne narzędzia i metody oceny i zatwierdzania kompetencji &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221;.</li>
                    <li>Zapoznać się z zestawem umiejętności i wiedzy, które są zawarte w teście dla uczniów młodzieżowych &#8220;Poczucie inicjatywy i przedsiębiorczości&#8221;.</li>
                    <li>Umieć korzystać z opracowanego w ramach projektu internetowego narzędzia oceny, aby ocenić umiejętności uczących się, aby zmierzyć wpływ procesu uczenia się.</li>
                    <li>Umieć zmotywować młodzież do sprawdzenia swoich kompetencji uzyskanych w ramach szkolenia SELF-E.</li>
                    <li>Umieć przedstawić uczniom młodzieżowym przewodnik dla uczących się &#8220;Dlaczego walidacja jest dla mnie ważna&#8221;.</li>
                    <li>Rozumieć znaczenie koncepcji portfela wiedzy.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

