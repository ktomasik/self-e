<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 31.01.2019
 * Time: 08:50
 */
?>
<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }

    .red {
        color: #bb120e;
    }

    .row-margin {
        margin: 1.5em auto;
    }

    .questionWindow {
        max-width: 70%;
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }

    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }

    .feedbackScreen {
        max-width: 500px;
        margin: auto;
    }

    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }

    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/youth-workers-module2.html" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <span class="green">Theme:</span> <span class="blue">Lifestyle entrepreneurship (LSE)</span><br/>
            <span class="green">Exercise:</span> <span
                    class="blue">What traits are typical of lifestyle entrepreneur?</span><br/>
            <span class="green">Developed by:</span> <span class="blue">Social Innovation Fund, LT</span><br/><br/>
            <span class="green">Aim:</span> <span
                    class="blue">To understand what lifestyle Entrepreneurship is.</span><br/><br/>
            <span class="green">Learning outcomes:</span>
            <ul>
                <li><span class="blue">Understand your possibilities to become a lifestyle entrepreneur.</span></li>
                <li><span class="blue">Ground the main differences between entrepreneurship and LSE.</span></li>
                <li><span class="blue">Understand the benefits of LSE.</span></li>
            </ul>
            <span class="green">Expected duration:</span> <span class="blue">20 minutes</span><br/><br/>
            <span class="green">Description:</span><br/>
            <span>This exercise will help you learn about some typical entrepreneurial traits and give you an idea if you should consider seeking an entrepreneurial opportunity. There are some typical lifestyle entrepreneurial principles that many entrepreneurs share. In addition, there are some entrepreneurial traits that are undesirable and, if not managed, could be destructive.</span><br/><br/>
            <span class="green">Task:</span> <span class="blue">For each question, please select one possible answer which you think is correct.</span>
            <div class="row justify-content-center row-margin">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start Exercise</button>
                </div>
            </div>
        </div>
        <div class="task-desc" style="display: none">
            <span class="green">Theme:</span> <span class="blue">Lifestyle entrepreneurship (LSE)</span><br/>
            <span class="green">Exercise:</span> <span
                    class="blue">What traits are typical of lifestyle entrepreneur?</span><br/>
            <span class="green">Developed by:</span> <span class="blue">Social Innovation Fund, LT</span><br/>
            <span class="green">Task:</span> <span class="blue">Please select the one possible answer which you think is correct.</span>
        </div>

        <div id="window-0" style="display: none" class="questionWindow" data-value="0">
            <div class="question">
                <h3 class="text-center">Can everyone be a lifestyle entrepreneur?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-0" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>A lifestyle entrepreneur  requires specific personal traits, along with the desire to make a business out of their passion whether this is for financial or social gain.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>A lifestyle entrepreneur requires specific personal traits, along with the desire to make a business out of their passion whether this is for financial or social gain.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-1" style="display: none" class="questionWindow" data-value="1">
            <div class="question">
                <h3 class="text-center">Please find incorrect statement</h3>
                <h4 class="text-center">Both entrepreneur and LSE are oriented towards:</h4>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">managing a business</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">seeking profit</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">completing market research
                </button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">developing a marketing
                    strategy
                </button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">financial planning</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">people management</button>
            </div>
        </div>
        <div id="feedback-1" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Lifestyle entrepreneurs desire adventure. Motivated by quality of life rather than growth of profit. Running and managing a business, including setting a business mission, completing thorough market research, developing a marketing strategy, financial planning and forecasting, people management are essential principles to all types of business. </span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p>
                <span>Running and managing a business, including setting a business mission, completing thorough market research, developing a marketing strategy, financial planning and forecasting, people management are essential principles to all types of business. Entrepreneurs just want profit. They typically want to grow profit as fast as possible.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-2" style="display: none" class="questionWindow" data-value="2">
            <div class="question">
                <h3 class="text-center">Can different styles of LSE be a useful alternative to self-employment for
                    disadvantages learners?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-2" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>There are different styles of LSE. All of them can be a useful alternative to self-employment for disadvantaged people. There are many role model businesses that could identify their journey and experiences of lifestyle entrepreneurship across many different business sectors.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>There are different styles of LSE. All of them can be a useful alternative to self-employment for disadvantaged people. There are many role model businesses that could identify their journey and experiences of lifestyle entrepreneurship across many different occupational areas.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-3" style="display: none" class="questionWindow" data-value="3">
            <div class="question">
                <h3 class="text-center">Does Lifestyle Entrepreneurship (LSE) create opportunities for self-employment
                    around one’s passions, hobbies and lifestyle skills?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-3" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>It is usually a home- based business that can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>It is usually a home- based business that can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle. Examples of businesses that Lifestyle entrepreneurs run include: cake-making, homemade jams, chutneys etc., catering from home, childcare, dog walking, pet-sitting, virtual office services, online businesses and mobile hairdressing.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-4" style="display: none" class="questionWindow" data-value="4">
            <div class="question">
                <h3 class="text-center">Which of the following statements describes the typical LSE pursuing a new
                    entrepreneurial opportunity?</h3>
                <h4 class="text-center">Life style entrepreneur:</h4>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="a1" data-val="false">does everything on his/her own
                    and rarely asks for help from others
                </button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="a2" data-val="false">hires others to perform all the
                    tasks required for the new enterprise
                </button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="a2" data-val="false">relies on others to do the
                    "hard work"
                </button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="a4" data-val="true">does most things on his/her own
                    and asks for help when needed
                </button>
            </div>
        </div>
        <div id="feedback-4" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="a4">
                <p class="green">Your answer is correct!</p>
                <span>Successful life style entrepreneur performs many of the tasks required to start a new enterprise, but seeks expert advice in specific areas in which he/she is unfamiliar and needs help.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="a1">
                <p class="red">This is not the expected answer!</p><br/>
                <span>Successful beginning entrepreneurs recognize what they do not know about starting a new enterprise and seek out help from others with more experience.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="a2">
                <p class="red">This is not the expected answer!</p><br/>
                <span>Usually LSE have limited money to hire new employees.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-5" style="display: none" class="questionWindow" data-value="5">
            <div class="question">
                <h3 class="text-center">Does successful life style entrepreneur realize the value of money?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-5" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Successful entrepreneurs work hard for their money and usually spend it very carefully since money saved goes directly to the bottom line generating more profit.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-finish">Finish</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>If you do not understand the value of money, it is nearly impossible to succeed as entrepreneur. Successful entrepreneurs do not over spend when buying materials, always negotiate for a better price, and save money for an unexpected expense or decrease in sales. Foolish entrepreneurs tend to spend too much money on things that do not generate profit or help the enterprise like more office space than needed, fancy restaurants, and expensive cars.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-finish">Finish</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="finish questionWindow text-center" style="display: none">
            <h3 class="green">Thank you for completing the exercise!</h3>
            <div class="points"></div>
            <p>If you are satisfied with your results, please continue to the exercises list.</p>
            <p>If you are not totally satisfied with your results, you are kindly advised to repeat the exercise as it will help you to deepen your knowledge on the topic.</p>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="exercise1_en.html" class="btn btn-primary repeat">Repeat the exercise</a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="btn btn-success backToList">Back to exercises list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function (e) {
        e.preventDefault();
        $('.home-screen').hide();
        $('#window-0').show(400);
        $('.task-desc').show();
    });

    var testWindows = $('.questionWindow');

    var points = 0;
    $(testWindows).each(function () {
        var current = parseInt($(this).attr('data-value'));
        var ansbtn = $('#window-' + current + ' .answers .ansBtn');
        $(ansbtn).each(function () {
            $(this).click(function () {
                var attr = $(this).attr('data-name');
                var feedbackScreen = $('#feedback-' + current + ' .feedback');
                var dataVal = $(this).attr('data-val');
                if(dataVal === 'true') {
                    points++;
                }
                $(feedbackScreen).each(function () {
                    var feedback = $(this).attr('data-name');
                    if (feedback === attr) {
                        $(this).show(400);
                        $('#window-' + current).hide();
                    }
                });
                $('.finish .points').html('<p class="scoreMessage">You answered correctly <strong>'+points+' of 6</strong> questions.</p>');
            });
        });
        var nextBtn = $('#feedback-' + current + ' .feedback .btn-next');
        $(nextBtn).click(function () {
            $('#feedback-' + current).hide();
            $('#window-' + (current + 1)).show(400);
        });

        var nextFinish = $('#feedback-5 .feedback .btn-finish');
        $(nextFinish).click(function () {
            $('#feedback-5').hide();
            $('.finish').show(400);
        })
    });
</script>
