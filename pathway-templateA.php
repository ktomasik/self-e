<?php

/**
 * TEMPLATE FOR YES/NO OER QUESTIONS WITH THE SAME FEEDBACK
 */

?>

<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }

    .red {
        color: #bb120e;
    }

    .row-margin {
        margin: 1.5em auto;
    }

    .questionWindow {
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }

    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }

    .feedbackScreen {
        margin: auto;
    }

    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }

    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
    p.note {
        color: rgb(249, 47, 76);
        font-weight: bold;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <span class="green">Theme:</span> <span class="blue"><?= $exercise['theme'] ?></span><br/>
            <span class="green">Exercise:</span> <span
                class="blue"><?= $exercise['exercise'] ?></span><br/>
            <span class="green">Developed by:</span> <span class="blue"><?= $exercise['developedBy'] ?></span>
            <br/>
            <div class="intro-hide">
                <br/>
                <span class="green">Aim:</span> <span
                    class="blue"><?= $exercise['aim'] ?></span><br/><br/>
                <span class="green">Learning outcomes:</span>
                <ul>
                    <?php foreach ($exercise['learningOutcomes'] as $learningOutcome) { ?>
                        <li><span class="blue"><?= $learningOutcome ?></span></li>
                    <?php } ?>
                </ul>
                <span class="green">Expected duration:</span> <span class="blue"><?= $exercise['expectedDuration'] ?></span><br/><br/>
                <span class="green">Description:</span><br/>
                <span><?= $exercise['description'] ?></span><br/><br/>
            </div>
            <span class="green">Task:</span> <span class="blue"><?= $exercise['task'] ?></span>
            <br />
            <br />
            <div class="row justify-content-center row-margin intro-hide">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start Exercise</button>
                </div>
            </div>
        </div>

        <?php

        $i = 0;
        foreach ($exercise['questions'] as $question) { ?>
            <div id="window-<?=$i?>" style="display: none" class="questionWindow" data-value="<?=$i?>">
                <div class="question">
                    <?php if (isset($exercise['settings']['intro'])) { ?>
                        <h3 class="text-center"><?=$exercise['settings']['intro']?></h3>
                    <?php } ?>
                    <h3 class="text-center">(<?= $i + 1?>/<?=count($exercise['questions'])?>) <?=$question['q']?></h3>
                </div>
                <div class="answers">
                    <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="<?=$question['c'] === 'yes' ? 'true' : 'false'?>">
                        <?=isset($question['yes']) ? $question['yes'] : $dictionary['yes']?>
                    </button>
                    <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="<?=$question['c'] === 'no' ? 'true' : 'false'?>">
                        <?=isset($question['no']) ? $question['no'] : $dictionary['no']?>
                    </button>
                </div>
            </div>
            <div id="feedback-<?=$i?>" class="feedbackScreen">
                <div class="feedback correctBox" style="display: none;" data-name="<?=$question['c'] === 'yes' ? 'yes' : 'no'?>">
                    <p class="green"><?=$dictionary['feedbackOk']?></p>
                    <span><?php
                        if (isset($question['fy'], $question['fn'])) {
                            echo $question['c'] === 'yes' ? $question['fy'] : $question['fn'];
                        } else {
                            echo $question['f'];
                        }
                        ?></span>
                    <div class="row justify-content-center row-margin">
                        <button class="btn btn-success btn-next"><?=$dictionary['ok']?></button>
                    </div>
                </div>
                <div class="feedback incorrectBox" style="display: none;" data-name="<?=$question['c'] === 'yes' ? 'no' : 'yes'?>">
                    <p class="red"><?=$dictionary['feedbackWrong']?></p><br/>
                    <span><?php
                        if (isset($question['fy'], $question['fn'])) {
                            echo $question['c'] === 'yes' ? $question['fn'] : $question['fy'];
                        } else {
                            echo $question['f'];
                        }
                        ?></span>
                    <div class="row justify-content-center row-margin">
                        <button class="btn btn-success btn-next"><?=$dictionary['ok']?></button>
                    </div>
                </div>
            </div>

            <?php
            $i++;
        } ?>

        <div class="finish questionWindow text-center" style="display: none">
            <h3 class="green">Thank you for completing the exercise!</h3>
            <div class="points">
                <p class="scoreMessage">You answered correctly <strong><span class="userPoints"></span> of <?= count($exercise['questions']) ?></strong> questions.</p>
            </div>
            <p>If you are satisfied with your results, please continue to the exercises list.</p>
            <p>If you are not totally satisfied with your results, you are kindly advised to repeat the exercise as it will help you to deepen your knowledge on the topic.</p>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="#" class="btn btn-primary repeat">Repeat the exercise</a>
                    </div>
                    <div class="col-md-4">
                        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success backToList">Back to exercises list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function (e) {
        e.preventDefault();
        $('.intro-hide').hide();
        $('#window-0').show(400);
    });

    var testWindows = $('.questionWindow');

    var points = 0;
    $(testWindows).each(function () {
        var current = parseInt($(this).attr('data-value'));
        var ansbtn = $('#window-' + current + ' .answers .ansBtn');
        $(ansbtn).each(function () {
            $(this).click(function () {
                var attr = $(this).attr('data-name');
                var feedbackScreen = $('#feedback-' + current + ' .feedback');
                var dataVal = $(this).attr('data-val');
                if(dataVal === 'true') {
                    points++;
                }
                $(feedbackScreen).each(function () {
                    var feedback = $(this).attr('data-name');
                    if (feedback === attr) {
                        $(this).show(400);
                        $('#window-' + current).hide();
                    }
                });
            });
        });
        var nextBtn = $('#feedback-' + current + ' .feedback .btn-next');
        $(nextBtn).click(function () {
            if (current < <?= count($exercise['questions']) - 1?>) {
                $('#feedback-' + current).hide();
                $('#window-' + (current + 1)).show(400);
            } else {
                // points
                $('span.userPoints').text(points);
                $('#feedback-' + current).hide();
                $('.finish').show(400);
            }
        });
    });

    $('a.repeat').click(function(e){
        e.preventDefault();
        window.location.reload();
    });
</script>
