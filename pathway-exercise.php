<?php

include_once "functions.php";

$exerciseId = (int)$_GET['id'];
$categoryId = (int)$_GET['category'];
$lang = 'en'; // @todo later to implement via $_GET variable

$dictionary['feedbackOk'] = 'Your answer is correct!';
$dictionary['feedbackWrong'] = 'This is not the expected answer!';
$dictionary['yes'] = 'Yes';
$dictionary['no'] = 'No';
$dictionary['ok'] = 'OK';

// @todo finish the dictionary later with language versions, for now EN only
$dictionary['back'] = 'Back';
$dictionary['theme'] = 'Theme';
$dictionary['exercise'] = 'Exercise';
$dictionary['developedBy'] = 'Developed by';
$dictionary['aim'] = 'Aim';
$dictionary['learningOutcomes'] = 'Learning outcomes';
$dictionary['expectedDuration'] = 'Expected duration';
$dictionary['description'] = 'Description';
$dictionary['task'] = 'Task';
$dictionary['startExercise'] = 'Start Exercise';

$exercise = getExerciseData($lang, $exerciseId);

if (in_array($exerciseId, [2, 4, 5, 6, 9, 10, 13, 14, 15, 16], true)) {
    include 'pathway-templateA.php';
}
if (in_array($exerciseId, [1, 17, 21], true)) {
    include 'pathway-templateB.php';
}
if (in_array($exerciseId, [3], true)) {
    include 'pathway-templateC.php';
}
