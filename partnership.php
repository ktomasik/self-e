<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 offset-md-6">
            <span class="dark-blue"><strong>Project number No.: 2017-3-LT02-KA205-005536</strong></span>
        </div>
        <h1>Partnership</h1>
        <div class="row">
            <div class="col-sm-6 offset-sm-6 col-md-8 offset-md-4">
                <br />
                <h3><span class="dark-blue">Project Coordinator:</span></h3>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox coordinator">
                    <img src="/img/partners/logo-sif.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>Social Innovation Fund</strong><br />
                Kaunas, Lithuania<br />
                <a href="http://www.lpf.lt" target="_blank">lpf.lt</a><br>
                <a href="mailto:sif@lpf.lt">sif@lpf.lt</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 offset-sm-6 col-md-8 offset-md-4">
                <br />
                <h3><span class="light-blue">Project Partners:</span></h3>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox">
                    <img src="/img/partners/logo-c.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>CARDET - Centre for the Advancement of Research & Development in Educational Technology</strong><br />
                Nicosia, Cyprus<br />
                <a href="http://www.cardet.org/" target="_blank">cardet.org</a>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox">
                    <img src="/img/partners/logo-cwep.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>Stowarzyszenie Centrum Wspierania Edukacji i Przedsiębiorczości (CWEP)</strong><br />
                Rzeszów, Poland<br />
                <a href="https://cwep.eu/" target="_blank">cwep.eu</a>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox">
                    <img src="/img/partners/logo-kc.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>Know and Can Association (K&C)</strong><br />
                Sofia, Bulgaria<br />
                <a href="http://www.znamimoga.org/" target="_blank">znamimoga.org</a>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox">
                    <img src="/img/partners/logo-knjuc.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>Kauno neigaliojo jaunimo uzimtumo centras (KNJUC)</strong><br />
                Kaunas, Lithuania<br />
                <a href="http://www.nju.lt/" target="_blank">nju.lt</a>
            </div>
        </div>
        <div class="row partner">
            <div class="col-sm-6 col-md-4">
                <div class="logoBox">
                    <img src="/img/partners/logo-vini.png" />
                </div>
            </div>
            <div class="col-sm-6 col-md-8">
                <strong>Verslo iniciatyva (VINI)</strong><br />
                Kaunas, Lithuania<br />
                <a href="http://www.vini.lt/" target="_blank">vini.lt</a>
            </div>
        </div>
    </div>
</div>

