<?php

/**
 * TEMPLATE FOR MIXED OER QUESTIONS WITH OPTIONAL DIFFERENT FEEDBACK AND MULTIPLE SELECTION FOR ANSWERS
 */

?>

<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }

    .red {
        color: #bb120e;
    }

    .row-margin {
        margin: 1.5em auto;
    }

    .questionWindow {
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }

    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }

    .feedbackScreen {
        margin: auto;
    }

    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }

    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
    p.note {
        color: rgb(249, 47, 76);
        font-weight: bold;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <span class="green">Theme:</span> <span class="blue"><?= $exercise['theme'] ?></span><br/>
            <span class="green">Exercise:</span> <span
                class="blue"><?= $exercise['exercise'] ?></span><br/>
            <span class="green">Developed by:</span> <span class="blue"><?= $exercise['developedBy'] ?></span>
            <br/>
            <div class="intro-hide">
                <br/>
                <span class="green">Aim:</span> <span
                    class="blue"><?= $exercise['aim'] ?></span><br/><br/>
                <span class="green">Learning outcomes:</span>
                <ul>
                    <?php foreach ($exercise['learningOutcomes'] as $learningOutcome) { ?>
                        <li><span class="blue"><?= $learningOutcome ?></span></li>
                    <?php } ?>
                </ul>
                <span class="green">Expected duration:</span> <span class="blue"><?= $exercise['expectedDuration'] ?></span><br/><br/>
                <span class="green">Description:</span><br/>
                <span><?= $exercise['description'] ?></span><br/><br/>
            </div>
            <span class="green">Task:</span> <span class="blue"><?= $exercise['task'] ?></span>
            <br />
            <br />
            <div class="row justify-content-center row-margin intro-hide">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start Exercise</button>
                </div>
            </div>
        </div>

        <?php

        $i = 0;
        foreach ($exercise['questions'] as $question) {
            $feedbackDivs = ''; ?>
            <div id="window-<?=$i?>" style="display: none" class="questionWindow" data-value="<?=$i?>">
                <div class="question">
                    <?php if (isset($question['intro'])) { ?>
                        <h3 class="text-center"><?=$question['intro']?></h3>
                    <?php } ?>
                    <h3 class="text-center">(<?= $i + 1?>/<?=count($exercise['questions'])?>) <?=$question['q']?></h3>
                    <?php if (isset($question['introPreQ'])) { ?>
                        <h3 class="text-center"><?=$question['introPreQ']?></h3>
                    <?php } ?>
                </div>
                <div class="answers">
                    <?php
                    $aNo = 0;
                    foreach ($question['a'] as $answer) { ?>
                        <button type="button" class="btn btn-outline-primary ansBtn" data-val="<?=$answer[1] ? 'true' : 'false'?>" data-feedback="q<?=$i?>a<?=$aNo?>" <?=isset($question['multiple']) ? 'data-multiple="true"' : ''?>>
                            <?=$answer[0]?>
                        </button>
                    <?php
                        if (!isset($question['multiple'])) {
                            if (isset($answer[2])) {
                                $feedback = $answer[2];
                            } elseif (isset($question['fy'], $question['fn'])) {
                                $feedback = $answer[1] ? $question['fy'] : $question['fn'];
                            } else {
                                $feedback = $question['f'];
                            }
                            $feedbackDivs .= '<div class="feedback ' . ($answer[1] ? 'correctBox' : 'incorrectBox') . '" style="display: none;" data-feedback="q' . $i . 'a' . $aNo . '">
                                            <p class="' . ($answer[1] ? 'green' : 'red') . '">' . ($answer[1] ? $dictionary['feedbackOk'] : $dictionary['feedbackWrong']) . '</p>
                                            <span>' . $feedback . '</span>
                                            <div class="row justify-content-center row-margin">
                                                <button class="btn btn-success btn-next">' . $dictionary['ok'] . '</button>
                                            </div>
                                        </div>';
                        }
                        $aNo++;
                    } ?>
                </div>
                <?php if (isset($question['multiple'])) { ?>
                    <div class="text-center">
                        <button class="btn btn-success ansBtnMultiple" data-question="<?=$i?>">Check</button>
                    </div>
                <?php } ?>
            </div>
            <div id="feedback-<?=$i?>" class="feedbackScreen">
                <?php
                if (!isset($question['multiple'])) {
                    echo $feedbackDivs;
                } else {
                    echo '<div class="feedback correctBox" style="display: none;" data-feedback="q' . $i . 'yes">
                                            <p class="green">' . $dictionary['feedbackOk'] . '</p>
                                            <span>' . $question['fy'] . '</span>
                                            <div class="row justify-content-center row-margin">
                                                <button class="btn btn-success btn-next">' . $dictionary['ok'] . '</button>
                                            </div>
                                        </div>';
                    echo '<div class="feedback incorrectBox" style="display: none;" data-feedback="q' . $i . 'no">
                                            <p class="red">' . $dictionary['feedbackWrong'] . '</p>
                                            <span>' . $question['fn'] . '</span>
                                            <div class="row justify-content-center row-margin">
                                                <button class="btn btn-success btn-next">' . $dictionary['ok'] . '</button>
                                            </div>
                                        </div>';
                }
                ?>
            </div>

            <?php
            $i++;
        } ?>

        <div class="finish questionWindow text-center" style="display: none">
            <h3 class="green">Thank you for completing the exercise!</h3>
            <div class="points">
                <p class="scoreMessage">You answered correctly <strong><span class="userPoints"></span> of <?= count($exercise['questions']) ?></strong> questions.</p>
            </div>
            <p>If you are satisfied with your results, please continue to the exercises list.</p>
            <p>If you are not totally satisfied with your results, you are kindly advised to repeat the exercise as it will help you to deepen your knowledge on the topic.</p>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="#" class="btn btn-primary repeat">Repeat the exercise</a>
                    </div>
                    <div class="col-md-4">
                        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success backToList">Back to exercises list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function (e) {
        e.preventDefault();
        $('.intro-hide').hide();
        $('#window-0').show(400);
    });

    var current = 0;
    var points = 0;

    $('.ansBtn').click(function(e){
        e.preventDefault();
        if ($(this).attr('data-multiple') === 'true') {
            if ($(this).hasClass('btn-outline-primary')) {
                $(this).removeClass('btn-outline-primary').addClass('btn-success');
            } else {
                $(this).removeClass('btn-success').addClass('btn-outline-primary');
            }
            return true;
        }
        if ($(this).attr('data-val') === 'true') {
            points++;
        }
        $('div.feedback[data-feedback="' + $(this).attr('data-feedback') + '"]').show(400);
        $(this).closest('.questionWindow').hide();
    });

    $('.ansBtnMultiple').click(function(e){
        e.preventDefault();
        if ($(this).closest('.questionWindow').find('button.ansBtn[data-val="true"]').length == $(this).closest('.questionWindow').find('button.ansBtn.btn-success').length) {
            points++;
            $('div.feedback[data-feedback="q' + $(this).attr('data-question') + 'yes"]').show(400);
        } else {
            $('div.feedback[data-feedback="q' + $(this).attr('data-question') + 'no"]').show(400);
        }
        $(this).closest('.questionWindow').hide();
    });

    $('.btn-next').click(function () {
        current++;
        if (current <= <?= count($exercise['questions']) - 1?>) {
            $('#feedback-' + (current - 1)).hide();
            $('#window-' + current).show(400);
        } else {
            // points
            $('span.userPoints').text(points);
            $('#feedback-' + (current -1)).hide();
            $('.finish').show(400);
        }
    });

    $('a.repeat').click(function(e){
        e.preventDefault();
        window.location.reload();
    });
</script>
