<div class="row">
    <div>
        <div class="col-md-6 offset-md-6">
            <span class="dark-blue"><strong>Project number No.: 2017-3-LT02-KA205-005536</strong></span>
        </div>
        <h1>About project</h1>
        <p><strong class="blue">The main aim</strong> of the project is to promote quality youth work in order to foster self-employment of young people with fewer opportunities, including NEETs.</p>
        <p><strong class="blue">Objectives of SELF-E project are:</strong></p>
        <ol>
            <li>Strengthening capacity of youth workers to organise innovative non-formal learning on youth self-employment via mentoring.</li>
            <li>To support youth workers in applying new methods for motivating young people with fewer opportunities, including NEETs to learn and become self-employed (with special emphasis on Lifestyle Entrepreneurship).</li>
            <li>To foster the transition of young people from youth to adulthood through integration in the labour market by self-employment.</li>
            <li>To develop youth competence “Sense of initiative and entrepreneurship”.</li>
            <li>To create opportunities to validate obtained competence “Sense of initiative and entrepreneurship”.</li>
        </ol>


        <p><strong class="blue">Target groups:</strong></p>
        <p>Youth workers who work with young people with fewer opportunities-16 will be trained within the learning activity, at least 18 youth workers will be involved in piloting of the non-formal SELF-E training course as trainers and 80 will be involved to the multiplying events;<br/><br/>
            Young people with fewer opportunities, including NEETs - 48 will be involved in piloting as learners.<br/>
            To achieve aim and objectives, the partnership prepares three intellectual outputs:  Toolkit for youth workers “How to organize innovative non formal learning on self-employment for young people via mentoring”; Set of practical exercises/OERs “Pathway to Self- employment”; Assessment tool for validation of competence “Sense of initiative and entrepreneurship”. The general methodology to all 3 intellectual outputs is based on Open Educational Resources in order to promote the open access to developed educational tools. However, each outcome is based on the specific to this outcome methodology. SELF-E training course is based on mentoring methodology. The set of developed practical exercises/OERs allows using innovative reversed training methodology in delivering the non-formal SELF-E training course, which combines self-learning, when mentor appoints tasks for learners‘ self-study of OERs on self-employment, and face-to-face mentoring sessions for further discussion on the material learnt. The assessment of competence is based on online testing methodology with number of programmed closed questions.
        </p>

        <p><strong class="blue">Expected impact</strong> is increasing capacity of youth work in providing self-employment training for youth with fewer opportunities in order to increase the level of their competence at least for 40%.</p>

        <p>
            <strong class="blue">Three intellectual outputs will be produced:</strong><br>
            <strong class="green">O1</strong> - The Toolkit for youth workers „Social mentoring as innovative training pathway to lifestyle self-employment – SELF-E”<br/>
            <strong class="green">O2</strong> - Set of practical exercises/OERs “ Pathway to lifestyle self- employment“<br/>
            <strong class="green">O3</strong> - Assessment tool for validation of competence “Sense of initiative and entrepreneurship”
        </p>

        <p><strong class="blue">General methodology</strong> to all 3 intellectual outputs is based on Open Educational Resources in order to promote the open access to developed educational materials. However, each outcome is based on the specific to this outcome methodology as well.<br/><br/>
            O1 corresponds to the main project aim: promote quality youth work in order to foster lifestyle self-employment of young people with fewer opportunities, including NEETS.<br/><br/>
            Thus, the youth workers will be equipped by innovative reversed training methodology in delivering the non-formal training course Self-E: combination of self-learning, when mentor appoints tasks for learners on self-study on virtual environment, and face-to-face meetings sessions with mentors for further discussion on the material learnt.<br/><br/>
            The main aim of the O2 is to develop the Set of practical exercises- OERs „Pathway to lifestyle self- employment (SE)“.<br/><br/>
            The methodology of the set of practical exercises for self-learning is based on self-reflection, thus the practical exercises will be developed and programmed with the closed questions and will have the clear explanation of the correct answers, in order to give the learner the possibility to learn.<br/><br/>
            The collection of the digital stories on Life Style Entrepreneurship is based on the Role Model methodology giving the possibility to learners to think about his/her opportunities to follow the good sample.<br/><br/>
            The aim of O3 is to develop the Assessment tool for validation of young people-learners competence “Sense of initiative and entrepreneurship”.<br/><br/>
            The methodology of this Assessment tool is based on creating awareness of the possibility to validate skills and competences obtained via non-formal and its benefit for learners. Pre-assessment and post assessment methods will be used while producing the test for assessment of competence “Sense of initiative and entrepreneurship” in order to evaluate the progress of learning.<br/><br/>
        </p>
    </div>

</div>

