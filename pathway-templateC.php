<?php

/**
 * TEMPLATE FOR MATCHING EXERCISES
 */

?>

<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }

    .red {
        color: #bb120e;
    }

    .row-margin {
        margin: 1.5em auto;
    }

    .questionWindow {
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }

    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }

    .feedbackScreen {
        margin: auto;
    }

    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }

    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
    p.note {
        color: rgb(249, 47, 76);
        font-weight: bold;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <span class="green">Theme:</span> <span class="blue"><?= $exercise['theme'] ?></span><br/>
            <span class="green">Exercise:</span> <span
                class="blue"><?= $exercise['exercise'] ?></span><br/>
            <span class="green">Developed by:</span> <span class="blue"><?= $exercise['developedBy'] ?></span>
            <br/>
            <div class="intro-hide">
                <br/>
                <span class="green">Aim:</span> <span
                    class="blue"><?= $exercise['aim'] ?></span><br/><br/>
                <span class="green">Learning outcomes:</span>
                <ul>
                    <?php foreach ($exercise['learningOutcomes'] as $learningOutcome) { ?>
                        <li><span class="blue"><?= $learningOutcome ?></span></li>
                    <?php } ?>
                </ul>
                <span class="green">Expected duration:</span> <span class="blue"><?= $exercise['expectedDuration'] ?></span><br/><br/>
                <span class="green">Description:</span><br/>
                <span><?= $exercise['description'] ?></span><br/><br/>
            </div>
            <span class="green">Task:</span> <span class="blue"><?= $exercise['task'] ?></span>
            <br />
            <br />
            <div class="row justify-content-center row-margin intro-hide">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start Exercise</button>
                </div>
            </div>
        </div>

        <?php

        $i = 0;
        foreach ($exercise['questions'] as $question) {
            $feedbackDivs = ''; ?>
            <div id="window-<?=$i?>" style="display: none" class="questionWindow" data-value="<?=$i?>">
                <div class="question">
                    <?php if (isset($question['intro'])) { ?>
                        <h3 class="text-center"><?=$question['intro']?></h3>
                    <?php } ?>
                    <h3 class="text-center">(<?= $i + 1?>/<?=count($exercise['questions'])?>) <?=$question['q']?></h3>
                    <?php if (isset($question['introPreQ'])) { ?>
                        <h3 class="text-center"><?=$question['introPreQ']?></h3>
                    <?php } ?>
                </div>
                <form autocomplete="off" id="tableForm">
                <table class="table table-bordered">
                    <tr>
                        <th><?= $question['leftTitle'] ?></th>
                        <th><?= $question['rightTitle'] ?></th>
                    </tr>
                    <?php
                    uksort($question['right'], static function() { return mt_rand() > mt_getrandmax() / 2; });
                    foreach ($question['left'] as $index => $item) { ?>
                        <tr>
                            <td><?= $item ?></td>
                            <td>
                                <select class="form-control" data-check="<?= $index ?>">
                                    <option selected>-</option>
                                    <?php
                                    $j = 0;
                                    foreach (array_keys($question['right']) as $answer) { ?>
                                        <option value="<?= $answer ?>"><?= chr(65 + $j) ?></option>
                                        <?php
                                        $j++;
                                    } ?>
                                </select>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
                </form>
                <?php
                $matchLegend = '';
                $letter = 0;
                foreach ($question['right'] as $index => $item) {
                    $matchLegend .= '<strong>' . chr(65 + $letter) . '</strong>:' . $item . '<br />';
                    $letter++;
                }
                echo $matchLegend;
                ?>
                <div class="text-center">
                    <button class="btn btn-success ansBtnMultiple" data-question="<?=$i?>">Check</button>
                </div>
            </div>
            <div id="feedback-<?=$i?>" class="feedbackScreen">
                <?php
                    echo '<div class="feedback correctBox" style="display: none;" data-feedback="q' . $i . 'yes">
                                            <p class="green">' . $dictionary['feedbackOk'] . '</p>
                                            <span class="answerTableOk"></span>
                                            ' . $matchLegend . '
                                            <div class="row justify-content-center row-margin">
                                                <button class="btn btn-success btn-next">' . $dictionary['ok'] . '</button>
                                            </div>
                                        </div>';
                    echo '<div class="feedback incorrectBox" style="display: none;" data-feedback="q' . $i . 'no">
                                            <p class="red">' . $dictionary['feedbackWrong'] . '</p>
                                            <span class="answerTableWrong"></span>
                                            ' . $matchLegend . '
                                            <div class="row justify-content-center row-margin">
                                                <button class="btn btn-success btn-next">' . $dictionary['ok'] . '</button>
                                            </div>
                                        </div>';
                ?>
            </div>

            <?php
            $i++;
        } ?>

        <div class="finish questionWindow text-center" style="display: none">
            <h3 class="green">Thank you for completing the exercise!</h3>
            <div class="points">
                <p class="scoreMessage">You answered correctly <strong><span class="userPoints"></span> of <?= count($question['right']) ?></strong> questions.</p>
            </div>
            <p>If you are satisfied with your results, please continue to the exercises list.</p>
            <p>If you are not totally satisfied with your results, you are kindly advised to repeat the exercise as it will help you to deepen your knowledge on the topic.</p>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="#" class="btn btn-primary repeat">Repeat the exercise</a>
                    </div>
                    <div class="col-md-4">
                        <a href="/pathway-category.html?category=<?= $categoryId ?>" class="btn btn-success backToList">Back to exercises list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function (e) {
        e.preventDefault();
        $('.intro-hide').hide();
        $('#window-0').show(400);
    });

    var current = 0;
    var points = 0;

    $('.ansBtnMultiple').click(function(e){
        e.preventDefault();
        $('.answerTableOk').html($('#tableForm').html());
        $('.answerTableOk').find('option').prop("selected", false);
        $('.answerTableOk').find('select').attr('disabled', true);
        $('.answerTableWrong').html($('#tableForm').html());
        $('.answerTableWrong').find('option').prop("selected", false);
        $('.answerTableWrong').find('select').attr('disabled', true);
        $('#tableForm select').each(function(k, v){
            if ($(v).attr('data-check') === $(v).val()) {
                points++;
                $('.answerTableOk').find('select').eq(k).closest('tr').addClass('table-success');
                $('.answerTableWrong').find('select').eq(k).closest('tr').addClass('table-success');
            } else {
                $('.answerTableOk').find('select').eq(k).closest('tr').addClass('table-danger');
                $('.answerTableWrong').find('select').eq(k).closest('tr').addClass('table-danger');
            }
            $('.answerTableOk').find('select').eq(k).children('[value="' + $(v).val() + '"]').prop("selected", true);
            $('.answerTableWrong').find('select').eq(k).children('[value="' + $(v).val() + '"]').prop("selected", true);
        });

        if (points === <?= count($question['right']) ?>) {
            $('div.feedback[data-feedback="q0yes"]').show(400);
        } else {
            $('div.feedback[data-feedback="q0no"]').show(400);
        }
        $(this).closest('.questionWindow').hide();
    });

    $('.btn-next').click(function () {
        current++;
        if (current <= <?= count($exercise['questions']) - 1?>) {
            $('#feedback-' + (current - 1)).hide();
            $('#window-' + current).show(400);
        } else {
            // points
            $('span.userPoints').text(points);
            $('#feedback-' + (current -1)).hide();
            $('.finish').show(400);
        }
    });

    $('a.repeat').click(function(e){
        e.preventDefault();
        window.location.reload();
    });
</script>
