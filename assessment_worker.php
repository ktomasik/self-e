<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 31.01.2019
 * Time: 08:50
 */
?>
<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }

    .red {
        color: #bb120e;
    }

    .row-margin {
        margin: 1.5em auto;
    }

    .questionWindow {
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }

    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }

    .feedbackScreen {
        margin: auto;
    }

    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }

    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
    p.note {
        color: rgb(249, 47, 76);
        font-weight: bold;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/youth-workers-module2.html" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <div class="alert alert-info">
                Assessment tool for youth worker's competences to become manager/mentor of mentoring process on Self-assessment test for youth workers
            </div>
            <p>The aim of the test is to support the youth workers’ competencies to become Manager or Mentor in mentoring, validate their competences and skills obtained within SELF-E training course. The test consists of 25 questions.</p>
            <p>Self -assessment test evaluates the following:</p>
            <ul>
                <li><strong>Module 1</strong> “Social Mentoring on lifestyle self-employment as a new non-formal learning pathway”.</li>
                <li><strong>Module 2</strong> “Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship”.</li>
                <li><strong>Module 3</strong> “Validation of the Competences “Sense of initiative and entrepreneurship”.</li>
            </ul>
            <p>You will be given two choices for an answer: "Yes" or "No". After completion of each question an explanation is provided. Having completed the test, the system will provide you with the sheet in which you will see your Passing Score (number of questions which your answered correctly) and the Passing Rate (in percentage). See the sample of the table below, which shows that the test is completed successfully if the Passing Rate is not lower than 80% and at least 20 answers are correct.</p>
            <p class="note">Please, save test results for further reference.</p>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Total Questions</th>
                    <th>Full Score</th>
                    <th>Passing Rate</th>
                    <th>Passing Score</th>
                </tr>
                <tr>
                    <td>25</td>
                    <td>25</td>
                    <td>80%</td>
                    <td>20</td>
                </tr>
            </table>
            <div class="row justify-content-center row-margin">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start</button>
                </div>
            </div>
        </div>

        <?php
        $feedbackOk = '<p class="green">Your answer is correct!</p>';
        $feedbackWrong = '<p class="red">This is not the expected answer!</p>';
        $yes = 'Yes';
        $no = 'No';
        $ok = 'OK';
        $questions = [
                'q1' => [
                    'q' => 'Is mentoring on Life-Style Entrepreneurship (LSE) a process where two or more people meet and discuss their problems in personal level and find solutions?',
                    'c' => 'no',
                    'f' => 'Mentoring on LSE is the process in which one person (Mentor) shares knowledge, skills, information and perspective to foster both the personal and professional growth of someone else (Mentee/-s). Mentors support, courage and acquaint Mentees with possible networks. During mentoring on lifestyle self-employment issues, Mentors encourage Mentees to become lifestyle self-employed.',
                ],
                'q2' => [
                    'q' => 'Does mentoring on Life-Style Entrepreneurship (LSE) support Mentee only at a personal level?',
                    'c' => 'no',
                    'f' => 'Mentoring on LSE is a multidimensional process in which a Mentee receives support in multiple ways and levels. Through mentoring on LSE, one can receive support, courage and open networks, since Mentors help Mentees to get self-employed or to start an LSE business. The mentoring on LSE process also increases the Mentee’s knowledge, capability and self-esteem. Additionally, mentoring on LSE is a process offering excellent opportunities for personal development of Mentor and Mentee as well.',
                ],
                'q3' => [
                    'q' => 'Has mentoring on Life-Style Entrepreneurship (LSE) any challenges for Mentors or Mentees?',
                    'c' => 'yes',
                    'f' => 'Mentoring on LSE can be a positive experience for both Mentors and Mentees, but it can also be challenging. Most frequently, such challenges may lead to the early ending of mentoring relationships. Various unavoidable situations and life circumstances may occur and challenge the Mentor-Mentee relationships. It is recommended to provide clear information to Mentors and Mentees from the start about ending the mentoring relationships. This can help to prevent negative effects and reduce the possibility of early ending of mentoring relationships when they become difficult.',
                ],
                'q4' => [
                    'q' => 'Can mentoring on Life-Style Entrepreneurship (LSE) have a different form than face-to-face meetings?',
                    'c' => 'yes',
                    'f' => 'Mentoring on LSE may have many forms: a) face-to-face, b) e-Mentoring and; c) blended mentoring. Face-to-face mentoring means that a Mentor regularly meets his/her Mentees and communicates with them during face-to-face meetings. During e-Mentoring the ICT tools and e-Learning are used. The meetings of Mentor and Mentee/-s takes place online. Blended mentoring is a mixture of face-to-face and e-Mentoring meetings. A Mentor communicates with his/her Mentee(s) by using the e-Mentoring web environment and by meeting them face-to-face a few times during the mentoring process.',
                ],
                'q5' => [
                    'q' => 'The possible roles in mentoring on Life-Style Entrepreneurship (LSE) process are Manager, Mentor, and Mentee. Is this statement correct?',
                    'c' => 'yes',
                    'f' => 'The main roles of actors participating in the mentoring on LSE process are Manager, Mentor and Mentee. Sometimes Manager and Mentor could be the same person. Experienced and motivated Mentors are able to take Manager responsibilities and proceed mentoring in effective way. But the classic division between participants is: Manager, Mentor and Mentee.',
                ],
                'q6' => [
                    'q' => 'Are the main responsibilities of Mentor in mentoring on Life-Style Entrepreneurship (LSE) process related to preparation of tasks, check of progress and consultation?',
                    'c' => 'yes',
                    'f' => 'Mentor should prepare tasks for the Mentee(s), check his/her/their progress, consult and evaluate ideas. The Mentor also should give constructive feedback to the Mentee(-s) in order to help them with their development plan and do this by asking appropriate questions, offering different alternatives and solutions to solve problems and overcome barriers.<br /><br />The Mentor also has to help to build or increase the self-confidence of the Mentee; stimulate professional behaviour; teach by example; confront negative behaviour and attitudes; help in career growth; give professional and experienced advice; ultimately empower and enable the Mentee to reach determined goals.',
                ],
                'q7' => [
                    'q' => 'Does a Mentee need to set individual goals for mentoring on Life-Style Entrepreneurship (LSE) process?',
                    'c' => 'yes',
                    'f' => 'Every Mentee has to set individual goals for the mentoring on LSE process, no matter which type of mentoring on LSE in chosen (individual or group). During the initial mentoring meeting the Mentors should ask the Mentee(-s) what they would like to achieve during the mentoring on LSE process and by what date. The Mentee(-s) should formulate well-formed goals, which are positive and precise. A few examples of well-formed goals could be: “I want to get self-employed by__ (exact date)” or “I’ll have my own lifestyle business running successfully by__ (exact date)”. Keep in mind that the goals have to be completely formulated by the Mentee, not by the Mentor.',
                ],
                'q8' => [
                    'q' => 'Is it important for a Mentee to follow the rules of communication in mentoring on Life-Style Entrepreneurship (LSE) process?',
                    'c' => 'yes',
                    'f' => 'It is very important for a Mentee to follow the rules of communication in mentoring on LSE process as it will help to ensure the efficiency of mentoring process and proper communication between the Mentor and Mentee(-s). These rules are the rules of ethics which should be agreed on in the first mentoring meeting and which need to be followed during the whole mentoring process. Every Mentor should agree on communication rules with his/her Mentee(-s) as it will help them to work smoothly together in seeking individual/group goals.',
                ],
                'q9' => [
                    'q' => 'Can a Manager and a Mentor be the same person?',
                    'c' => 'yes',
                    'f' => 'Sometimes Mentor and Manager could be the same person. When Mentor is experienced, motivated to take Manager role and mentoring process is clearly described, Mentor is able to have Manager responsibilities. During the mentoring on LSE, the youth worker could take the Manager role and coordinate the whole mentoring process.',
                ],
                'q10' => [
                    'q' => 'Are Mentors expected to have regular communication with their Mentees only via face-to-face individual or group meetings?',
                    'c' => 'no',
                    'f' => 'A Mentor is expected to communicate with his/her Mentee(-s) by meeting them face-to-face a few times during the mentoring process and by using the web environment (e-mentoring). The e-mentoring requires some special secure environment for communicating with each other on a variety of LSE related issues. Nowadays technology provides possibility to communicate via Skype or using other online rooms. It is important to ensure (if possible) “an eye-contact” during these online meetings.',
                ],
                'q11' => [
                    'q' => 'Is entrepreneurship a skill that can be taught?',
                    'c' => 'yes',
                    'f' => 'Entrepreneurship is a skill which can be learned. In order to be an entrepreneur you should acquire certain skills (such as: spotting opportunities, self-awareness and self-efficacy, financial and economic literacy, planning and management, etc.) and knowledge, which could be useful when starting your own business. Then using these skills through learning by doing experience you could transform from beginner (learner) to successful entrepreneur (practitioner).<br /><br />A very comprehensive tool, designed to improve the entrepreneurial capacity of European citizens is EntreComp (full name: The Entrepreneurship Competence Framework; 2016). This tool could assist you in understanding and then developing the components of entrepreneurship in yourself, also in nurturing your personal development.',
                ],
                'q12' => [
                    'q' => 'Should an entrepreneur be socially adaptable?',
                    'c' => 'yes',
                    'f' => 'An entrepreneur has to be socially adaptable as this skill becomes helpful in establishing business contacts and cooperating with people from different backgrounds. It also helps the entrepreneurs to feel comfortable and adjust to different social situations.',
                ],
                'q13' => [
                    'q' => 'Is a lifestyle entrepreneur a person who builds his job around his life (style) and earns money through doing what he loves?',
                    'c' => 'yes',
                    'f' => 'A lifestyle entrepreneur is a person who builds his job around his life (style) and earns money through doing what he loves. People become lifestyle entrepreneurs for different reasons: some people want freedom to travel, some to spend more time with their families, other people to pursue other projects or aims they have, or live wherever they desire. A lifestyle business usually allows flexibility in personal time and location.<br /><br />Lifestyle businesses may vary in scope and size. There are some businesses that only lend themselves to a few customers, but they serve those exclusively well. Some wish to have a big customer base and some are satisfied just serving targeted customers. Regardless of size and scope, most lifestyle entrepreneurs love what they do, and sometimes will it do for free. It all depends on the personality and desires of the lifestyle entrepreneur.',
                ],
                'q14' => [
                    'q' => 'Do both traditional entrepreneurs and lifestyle entrepreneurs seek investors for their business?',
                    'c' => 'no',
                    'f' => 'Traditional entrepreneurs seek investors to help them in running their business by providing investments and wishing to receive the return on their investment later on. Lifestyle entrepreneurs do not seek investors. The business based on the lifestyle usually is the small one which could start with almost no initial cost.',
                ],
                'q15' => [
                    'q' => 'Is it important for a lifestyle entrepreneur to have the basic skills such as mother tongue, foreign language, mathematics, science and technology, digital competence, learning to learn, social and civic competencies, and a sense of initiative?',
                    'c' => 'yes',
                    'f' => 'Every lifestyle entrepreneur has to have a set of various basic skills, including mother tongue, foreign language, mathematics, science and technology, digital competence, learning to learn, social and civic competencies and a sense of initiative. These skills would be useful for him/her in various LSE everyday situations (communicating with the clients, preparing various financial documents, calculating income, planning expenses, etc.) and help him/her to successfully manage the lifestyle business.',
                ],
                'q16' => [
                    'q' => 'Does a lifestyle entrepreneur need to have good ICT (Information and Communication Technologies) skills in order to be successful in Life-Style Entrepreneurship?',
                    'c' => 'yes',
                    'f' => 'A lifestyle entrepreneur should have good ICT skills in order to be successful in LSE. Adequate competences in ICT are essential for effective participation in entrepreneurial activities. Using new opportunities and available tools and adapting knowledge & skills, it is easier to turn a hobby into a professional activity. This means that a lifestyle entrepreneur must understand and know how to choose the tools that will be beneficial for developing his/her lifestyle business.',
                ],
                'q17' => [
                    'q' => 'Is earning money important to lifestyle entrepreneurs?',
                    'c' => 'yes',
                    'f' => 'Yes, earning money is important for both, traditional and lifestyle entrepreneurs. The main difference between traditional ant lifestyle entrepreneurs is that the lifestyle entrepreneur is a person who builds his job around his life (style) and earns money through doing what he loves, while traditional entrepreneur seeks to earn as much money as possible. <br /><br />For lifestyle entrepreneur earning money is still important, but it is not the main motivation for him/her. Lifestyle entrepreneurs value a desired lifestyle over money. A lifestyle goes first before thinking of the ways to establish the business to support chosen lifestyle.',
                ],
                'q18' => [
                    'q' => 'Is Life-Style Entrepreneurship for everyone?',
                    'c' => 'no',
                    'f' => 'Not everyone can be a lifestyle entrepreneur; it requires specific personal traits, along with the desire to make a business out of one’s passion whether this is for financial or social gain or to prevent isolation. In order to do this person needs to start listening to himself, his feelings and asking essential questions about his (professional) skills, personal needs and goals in life.',
                ],
                'q19' => [
                    'q' => 'When you are starting your lifestyle business is it important to prepare a business plan?',
                    'c' => 'yes',
                    'f' => 'The first step the lifestyle entrepreneur has to take when starting a lifestyle business is to prepare a business plan, as it is a tool for understanding how the business will operate. It is the most important step in starting any new business or expanding an existing one. Thoroughly prepared business plan builds confidence in the lifestyle entrepreneur / small business owner’s ability to set up and operate the business, and may compensate the lack of capital and experience.',
                ],
                'q20' => [
                    'q' => 'Is financial planning the main part of Business Plan?',
                    'c' => 'no',
                    'f' => 'The financial planning is important for every business plan, but it is only the part of it. The Business Plan should include other parts, such as: the lifestyle entrepreneur’s or business owner’s goals for the enterprise; a description of the products or services offered and the anticipated market opportunities; and an explanation of the resources and means that will be employed to achieve the goals in the face of likely competition.',
                ],
                'q21' => [
                    'q' => 'Is it true that an individual could validate knowledge, skills and competences acquired in formal, non-formal and informal settings?',
                    'c' => 'yes',
                    'f' => 'Every person could validate knowledge, skills and competences he/she has acquired in formal, non-formal and informal settings. Through validation a person can get a wide range of knowledge, skills and competences acquired through life and in different contexts (e.g. through education (formal and non-formal), work and leisure activities, etc.) be identified, assessed and recognized. The youth workers should encourage youth learners to validate their knowledge and skills gained through mentoring on LSE, by completing the online self-assessment test before and after the mentoring on LSE process. Such validation would help to motivate the youth learners to become self-employed and start a lifestyle business.',
                ],
                'q22' => [
                    'q' => 'Are “validation” and “certification” synonyms?',
                    'c' => 'no',
                    'f' => 'Validation and certification are not synonyms. Validation is the process of assessing and recognizing a wide range of knowledge, know-how, skills and competences which people develop throughout their lives within different environments – formal, non-formal and informal. This process is done following a standard assessment procedure. Certification is the process of issuing the certificates or diplomas by accredited awarding bodies. These documents are the main proofs for recognition since they make formal, non-formal and informal learning visible.',
                ],
                'q23' => [
                    'q' => 'Are the tools and methods used to assess non-formal and informal learning very different from the ones in the formal education?',
                    'c' => 'no',
                    'f' => 'Many of the tools and methods used for assessing non-formal and informal learning are based on, or similar to, those used in formal education and training. The common assessment tools and methods used for assessing both types of learning are: tests, essay, debates and discussions, interviews, observation, peer assessment, self-assessment.<br /><br />For assessment of the skills developed through learning on LSE, it is recommended to use self-assessment method as it is the most suitable for clearing up the improvements of youth learners. During self-assessment youth learners reflect on their own work and judge how well they have performed in certain area. Developing reflective skills provides youth learners with the ability to consider their own performance and to identify their strengths, weaknesses, and areas that require improvement. Self-assessment can also provide insight into youth learners’ true comprehension of the learning material and can help to identify gaps in learners’ knowledge. It can be extremely valuable in helping youth learners to develop self-reflection, critique and judgment.',
                ],
                'q24' => [
                    'q' => 'Is it true that the only difference between formative and summative assessment is in the timing of their use – formative during and summative at the end of the learning process?',
                    'c' => 'no',
                    'f' => 'The difference between formative and summative assessment is not only in the timing of their use – formative during and summative at the end of the learning process. The aim and purpose of these two types of assessment is also different. <br /><br />The main aim of the formative assessment is to receive learners’ feedback on the educational process. It is used for improvement, not grading purposes. The formative assessment helps the youth workers-trainers to plan better their teaching activities by recognizing at which areas the youth learners are struggling and need more attention. <br /><br />The summative assessment is generally carried out at the end of a course/unit/learning module/project. Contrary to formative, summative assessment’s purpose is mainly to evaluate and grade youth learners’ theoretical and/or practical knowledge in LSE comparing it against some standard or benchmark.<br /><br />The youth learners participating in the training course on LSE should be asked at the beginning of the course to perform a self-assessment test on-line to determine the level of their knowledge and competences regarding the training material. Also at the end of the training course on LSE, the youth learners should be requested to retake the self-assessment test on-line in order to evaluate their progress in learning.',
                ],
                'q25' => [
                    'q' => 'Is the Knowledge Portfolio (KP) method useful for the validation of entrepreneurial skills?',
                    'c' => 'yes',
                    'f' => 'The Knowledge Portfolio method is useful for the validation of entrepreneurial skills as it is one of the most complex and frequently used methods to document evidence for validation purposes. KP aims to overcome the risk of subjectivity by introducing a mix of instruments to extract evidence of individuals\' competences and can incorporate assessments by third parties. Knowledge portfolio provides the audience with comprehensive insights into the achievements and successes of the learner. The Knowledge portfolio method tends to be process orientated. Some countries that provide national guidelines for validation, rather than prescribe validation methods, recommend a stage in the process which involves some form of assessment of the content of the Knowledge portfolio by a third party (such as a jury) to ensure greater validity.<br /><br />It is recommended for LSE Mentors to encourage their Mentees to fill-in the Knowledge portfolio during the mentoring on LSE process. The KP could become essential later on when the Mentees would decide to validate their entrepreneurial skills and knowledge acquired during the mentoring on LSE process.',
                ],
        ];

        $i = 0;
        foreach ($questions as $question) { ?>
            <div id="window-<?=$i?>" style="display: none" class="questionWindow" data-value="<?=$i?>">
                <div class="question">
                    <h3 class="text-center">(<?= $i + 1?>/<?=count($questions)?>) <?=$question['q']?></h3>
                </div>
                <div class="answers">
                    <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="<?=$question['c'] === 'yes' ? 'true' : 'false'?>"><?=$yes?></button>
                    <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="<?=$question['c'] === 'no' ? 'true' : 'false'?>"><?=$no?></button>
                </div>
            </div>
            <div id="feedback-<?=$i?>" class="feedbackScreen">
                <div class="feedback correctBox" style="display: none;" data-name="<?=$question['c'] === 'yes' ? 'yes' : 'no'?>">
                    <p class="green"><?=$feedbackOk?></p>
                    <span><?=$question['f']?></span>
                    <div class="row justify-content-center row-margin">
                        <button class="btn btn-success btn-next"><?=$ok?></button>
                    </div>
                </div>
                <div class="feedback incorrectBox" style="display: none;" data-name="<?=$question['c'] === 'yes' ? 'no' : 'yes'?>">
                    <p class="red"><?=$feedbackWrong?></p><br/>
                    <span><?=$question['f']?></span>
                    <div class="row justify-content-center row-margin">
                        <button class="btn btn-success btn-next"><?=$ok?></button>
                    </div>
                </div>
            </div>

        <?php
        $i++;
        } ?>

        <div class="finish questionWindow text-center" style="display: none">
            <h3 class="green">Assessment finished</h3>
            <div class="points"></div>
            <table class="table table-bordered table-striped">
                <tr>
                    <th>Total Questions</th>
                    <th>Full Score</th>
                    <th>Passing Rate</th>
                    <th>Passing Score</th>
                    <th>Your Score</th>
                    <th>Time Elapsed</th>
                </tr>
                <tr>
                    <td>25</td>
                    <td>25</td>
                    <td>80%</td>
                    <td>20</td>
                    <td><span id="userScore"></span></td>
                    <td><span id="timeElapsed"></span></td>
                </tr>
            </table>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="assessment_worker.html" class="btn btn-primary repeat">Try again</a>
                    </div>
                    <div class="col-md-4">
                        <a href="workers-as-learner.html" class="btn btn-success backToList">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function (e) {
        e.preventDefault();
        $('.home-screen').hide();
        $('#window-0').show(400);
        start_time = new Date();
    });

    var testWindows = $('.questionWindow');
    var start_time = 0;

    function getTime(start_time) {
        var end_time = new Date();
        var milli = end_time - start_time;
        var ms = milli % 1000;
        var s = Math.floor((milli / 1000) % 60);
        var m = Math.floor((milli / (60 * 1000)) % 60);
        return {'m': m, 's':s, 'ms':ms}
    }

    var points = 0;
    $(testWindows).each(function () {
        var current = parseInt($(this).attr('data-value'));
        var ansbtn = $('#window-' + current + ' .answers .ansBtn');
        $(ansbtn).each(function () {
            $(this).click(function () {
                var attr = $(this).attr('data-name');
                var feedbackScreen = $('#feedback-' + current + ' .feedback');
                var dataVal = $(this).attr('data-val');
                if(dataVal === 'true') {
                    points++;
                }
                $(feedbackScreen).each(function () {
                    var feedback = $(this).attr('data-name');
                    if (feedback === attr) {
                        $(this).show(400);
                        $('#window-' + current).hide();
                    }
                });
            });
        });
        var nextBtn = $('#feedback-' + current + ' .feedback .btn-next');
        $(nextBtn).click(function () {
            if (current < <?= count($questions) - 1?>) {
                $('#feedback-' + current).hide();
                $('#window-' + (current + 1)).show(400);
            } else {
                // points
                $('#userScore').html(points + ' (' + parseInt((points/<?=count($questions)?>)*100) + '%)');
                var time_array = getTime(start_time);
                $("#timeElapsed").text(time_array['m']+'m '+time_array['s']+'s');
                $('#feedback-' + current).hide();
                $('.finish').show(400);
            }
        });
    });
</script>
