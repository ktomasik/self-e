<?php
$page = 'home';
if (isset($_GET['page'])) {
    $page = filter_var($_GET['page'], FILTER_SANITIZE_STRING);
    if (!in_array($page, ['home', 'about', 'partnership', 'outcomes', 'events', 'youth-workers', 'workers-as-learner', 'youth-workers-module1', 'youth-workers-module2', 'youth-workers-module3', 'workers-as-teacher', 'young-people', 'pathway', 'exercise1_en', 'exercise2_en', 'success-stories', 'assessment_worker', 'curriculum', 'curriculum_pl', 'curriculum_bg', 'curriculum_lt', 'curriculum_gr', 'pathway-category', 'pathway-exercise'])) {
        $page = 'home';
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=yes">

    <!-- Google Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-20893557-43', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
          integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="/css/thumbnail-gallery.css">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <title>Self-e Project</title>
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-toggleable-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-sm-5 col-lg-3">
                        <a href="/"><img src="/img/project_logo.png" class="img-fluid"/></a>
                    </div>
                    <div class="col-sm-5 col-lg-9">
                    <ul class="navbar-nav mr-auto mt-2 mt-md-0" style="float:right">
                        <li class="nav-item <?= $page == 'home' ? 'active' : ''?>">
                            <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item <?= $page == 'about' ? 'active' : ''?>">
                            <a class="nav-link" href="/about.html">About Project</a>
                        </li>
                        <li class="nav-item <?= $page == 'partnership' ? 'active' : ''?>">
                            <a class="nav-link" href="/partnership.html">Partnership</a>
                        </li>
                        <li class="nav-item <?= $page == 'events' ? 'active' : ''?>">
                            <a class="nav-link" href="/events.html">Events</a>
                        </li>
                        <li class="nav-item <?= $page == 'outcomes' ? 'active' : ''?>">
                            <a class="nav-link" href="/outcomes.html">Outcomes</a>
                        </li>

                    </ul>
                    </div>
                </div>
            </div>

        </div>
    </nav>
</header>

<div style="clear:both"></div>

<main role="main" class="container" style="min-height: 54vh">
<?php
if (file_exists($page . '.php')) {
    include $page . '.php';
}
?>
</main>
<div style="clear: both"></div>

<footer>
    <div id="footer-bg"></div>
    <div class="container" id="footer-text">
        <div class="row align-items-center">
            <div class="col-md-4">
                <img src="/img/EU-funded_right.jpg" class="img-fluid"/>
            </div>
            <div class="col-md-8" style="color: #0e2c8e;border-left: 1px solid gray;">
                The European Commission support for the production of this project does not constitute an endorsement of
                the contents which reflects the views only of the authors, and the Commission cannot be held responsible for
                any use which may be made of the information contained therein.
            </div>
        </div>
        <div class="copyright">
            © Self-e, <?= date("Y");?>
        </div>
    </div>
</footer>



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
        integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script type="text/javascript" id="cookieinfo"
        src="//cookieinfoscript.com/js/cookieinfo.min.js">
</script>

</body>
</html>
