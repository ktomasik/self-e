<?php

include_once "pathway-exercises-data.php";

/* PATHWAY */
$themes = [
    "en" => [
        1 => "Entrepreneurship and self-employment (SE)",
        2 => "Life-Style Entrepreneurship",
        3 => "Business principles and Marketing in Life-Style Entrepreneurship",
    ]
];

$exNames = [
    "en" => [
        1 => "What traits are typical of lifestyle entrepreneur?",
        2 => "Should I become a lifestyle entrepreneur?",
        3 => "Main parts of the Business plan",
        4 => "Business principles in Life-Style Entrepreneurship",
        5 => "Understanding of entrepreneurship and self-employment (SE)",
        6 => "Benefits and challenges of self-employment",
        7 => "",
        8 => "",
        9 => "The importance of ICT for life style entrepreneurship",
        10 => "Prerequisites of using ICT for life style entrepreneurship",
        11 => "",
        12 => "",
        13 => "Skills of the self-employed entrepreneurs",
        14 => "What skills does the self-employed entrepreneur need?",
        15 => "What are the differences between Entrepreneurship and Life-style Entrepreneurship?",
        16 => "What are the similarities between Entrepreneurship and Life-style Entrepreneurship?",
        17 => "How do I develop my business plan? What do I have to know about the main principles of the business plan?",
        18 => "",
        19 => "",
        20 => "",
        21 => "What are the basics of Life-Style Entrepreneurship?",
        22 => "",
        23 => "",
        24 => ""
    ]
];

$exProps = [
        1 =>  ["link" => "#", "img" => ""],
        2 =>  ["link" => "#", "img" => ""],
        3 =>  ["link" => "#", "img" => ""],
        4 =>  ["link" => "#", "img" => ""],
        5 =>  ["link" => "#", "img" => ""],
        6 =>  ["link" => "#", "img" => ""],
        7 =>  ["link" => "#", "img" => ""],
        8 =>  ["link" => "#", "img" => ""],
        9 =>  ["link" => "#", "img" => ""],
        10 => ["link" => "#", "img" => ""],
        11 => ["link" => "#", "img" => ""],
        12 => ["link" => "#", "img" => ""],
        13 => ["link" => "#", "img" => ""],
        14 => ["link" => "#", "img" => ""],
        15 => ["link" => "#", "img" => ""],
        16 => ["link" => "#", "img" => ""],
        17 => ["link" => "#", "img" => ""],
        18 => ["link" => "#", "img" => ""],
        19 => ["link" => "#", "img" => ""],
        20 => ["link" => "#", "img" => ""],
        21 => ["link" => "#", "img" => ""],
        22 => ["link" => "#", "img" => ""],
        23 => ["link" => "#", "img" => ""],
        24 => ["link" => "#", "img" => ""]
];

// theme => [exNumbers]
$exThemeRelations = [
    -1 => [7, 8, 11, 12, 18, 19, 20, 21, 22, 23, 24], // dont exist yet, but will
    1 => [5, 6, 15, 16],
    2 => [1, 2, 13, 14, 21],
    3 => [3, 4, 9, 10, 17]
];

$backToPathway = [
    "en" => "Back to PATHWAY TO LIFESTYLE SELF-EMPLOYMENT menu",
];

$setOfPracticle = [
    "en" => "Set of practical exercises - OERs",
];

$varsToTranslate = [&$themes, &$exNames, &$backToPathway, &$setOfPracticle];
translateVars($varsToTranslate, "en");

function translateVars(&$vars, $lang)
{
    foreach ($vars as &$value) {
        $value = $value[$lang];
    }
}

function getThemeNameByNo($themeNumber)
{
    global $themes;
    return $themes[$themeNumber];
}

function getExercisesByThemeNo($themeNumber)
{
    global $exThemeRelations;
    global $exNames;
    $ex = [];

    foreach ($exThemeRelations[$themeNumber] as $exNo) {
        $ex[$exNo] = $exNames[$exNo];
    }
    //var_dump($ex);
    return $ex;
}

function getExerciseData($lang, $id) {
    global $exercises;
    return $exercises[$lang][$id];
}
