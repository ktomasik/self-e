<?php
/**
 * Created by PhpStorm.
 * User: lklapa
 * Date: 25.04.2019
 * Time: 17:28
 */
?>

<style>
    blockquote {
        font-weight: 100;
        font-size: 2rem;
        max-width: 600px;
        line-height: 1.4;
        position: relative;
        padding: .5rem;
        margin: 0 auto !important;
        text-align: center;
    }

    blockquote:before,
    blockquote:after {
        position: absolute;
        color: #f1efe6;
        font-size: 8rem;
        width: 4rem;
        height: 4rem;
    }

    blockquote:before {
        content: '“';
        left: -5rem;
        top: -2rem;
    }

    blockquote:after {
        content: '”';
        right: -5rem;
        bottom: 1rem;
    }

    hr.style-two { border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0)); }

</style>
<div class="row" style="margin-bottom: 2rem;">
    <div class="col-md-3">
        <a href="<?= isset($_GET['id']) ? '/success-stories.html' : '/young-people.html' ?>" class="btn btn-success">Back</a>
    </div>
</div>

<?php
if (!isset($_GET['id'])) { ?>
    <ul>
        <li><a href="/success-stories.html?id=1">Nerijus Rutkauskas – the Blacksmith</a></li>
        <li><a href="/success-stories.html?id=2">Toma Daugnorienė and her bakery “Cherry on Top”</a></li>
        <li><a href="/success-stories.html?id=3">Da Viny music</a></li>
        <li><a href="/success-stories.html?id=4">Andreas: A true warrior that created a team of “Warriors”</a></li>
        <li><a href="/success-stories.html?id=5">DISCOVERING MY PASSION FOR SEWING</a></li>
    </ul>
<?php } else {
    $id = (int)$_GET['id'];
    if (in_array($id, range(1,5), true)) {
        include('data/ss_' . $id . '_en.html');
    }

}

