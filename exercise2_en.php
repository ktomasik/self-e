<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 31.01.2019
 * Time: 11:32
 */
?>
<style>
    .green, .red {
        font-weight: bold;
        text-align: center;
    }
    .red {
        color: #bb120e;
    }
    .row-margin {
        margin: 1.5em auto;
    }
    .questionWindow {
        max-width: 70%;
        margin: auto;
        border: 2px solid #77d0fa;
        padding: 1.5em;
        border-radius: .75em;
    }
    .ansBtn {
        margin: .5em auto;
        display: grid;
        width: 100%;
        font-size: 18px;
        font-weight: bold;
    }
    .feedbackScreen {
        max-width: 500px;
        margin: auto;
    }
    .correctBox {
        border: 2px solid #8cc92a;
        padding: 1.5em;
        border-radius: .75em;
    }
    .incorrectBox {
        border: 2px solid #bb120e;
        padding: 1.5em;
        border-radius: .75em;
    }
    .scoreMessage {
        font-size: 19px;
    }
    .scoreMessage > strong {
        color: #0e2c8e;
    }
</style>

<div class="row row-margin">
    <div class="col-md-3">
        <a href="/youth-workers-module2.html" class="btn btn-success">Back</a>
    </div>
</div>
<div class="row row-margin">
    <div class="col-md-12">
        <div class="home-screen">
            <span class="green">Theme:</span> <span class="blue">Lifestyle entrepreneurship (LSE)</span><br/>
            <span class="green">Exercise:</span> <span class="blue">Should I become a lifestyle entrepreneur?</span><br/>
            <span class="green">Developed by:</span> <span class="blue">Social Innovation Fund, LT</span><br/><br/>
            <span class="green">Aim:</span> <span class="blue">To understand what knowledge, skills and competences lifestyle entrepreneur needs.</span><br/><br/>
            <span class="green">Learning outcomes:</span>
            <ul>
                <li><span class="blue">Understand what you need to do to become a lifestyle entrepreneur.</span></li>
                <li><span class="blue">Ground the typical characteristics of an LSE.</span></li>
                <li><span class="blue">Consider whether it is worth to seek LSE position.</span></li>
            </ul>
            <span class="green">Expected duration:</span> <span class="blue">30 minutes</span><br/><br/>
            <span class="green">Description:</span><br/>
            <span>This exercise will help you learn about some various life-style entrepreneurial characteristics and give you an idea what is important if you should consider seeking an entrepreneurial opportunity.</span><br/><br/>
            <span class="green">Task:</span> <span class="blue">For each question, please select one possible answer which you think is correct.</span>

            <div class="row justify-content-center row-margin">
                <div class="col-md-2">
                    <button class="btn btn-success" id="start-quiz">Start Exercise</button>
                </div>
            </div>
        </div>
        <div class="task-desc" style="display: none">
            <span class="green">Theme:</span> <span class="blue">Lifestyle entrepreneurship (LSE)</span><br/>
            <span class="green">Exercise:</span> <span class="blue">Should I become a lifestyle entrepreneur?</span><br/>
            <span class="green">Developed by:</span> <span class="blue">Social Innovation Fund, LT</span><br/>
            <span class="green">Task:</span> <span class="blue">Please select the one possible answer which you think is correct.</span>
        </div>

        <div id="window-0" style="display: none" class="questionWindow" data-value="0">
            <div class="question">
                <h3 class="text-center">Can the lifestyle entrepreneur do business out of what he/she is passion about and enjoy?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-0" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Lifestyle entrepreneurship is the chance to do business out of what you are passion about and enjoy. It is one of the best things we can do in our life so that we can live it the way we want; we can achieve something, have money and be happy and independent.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>Lifestyle entrepreneurship is the chance to do business out of what you are passion about and enjoy. It is one of the best things we can do in our life so that we can live it the way we want; we can achieve something, have money and be happy and independent.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-1" style="display: none" class="questionWindow" data-value="1">
            <div class="question">
                <h3 class="text-center">Are the management skills as the ability to plan, organize, manage, lead and delegate essential to all types of business including Life-Style Entrepreneurship?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-1" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Management skills as the ability to plan, organize, manage, lead and delegate, analyze, communicate, debrief, evaluate and record are essential to all types of business including Life-Style Entrepreneurship.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p>
                <span>Management skills as the ability to plan, organize, manage, lead and delegate, analyze, communicate, debrief, evaluate and record are essential to all types of business including Life-Style Entrepreneurship.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-2" style="display: none" class="questionWindow" data-value="2">
            <div class="question">
                <h3 class="text-center">Could a passion about your lifestyle business help you to succeed in developing it?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-2" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>If some sincerely love his/her lifestyle as a job and believe in its success, there is always a good chance that business will succeed. At first, lifestyle entrepreneurs can earn less (which is "normal" for each new business), but if they invest in themselves, learn and develop, they succeed.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>If some sincerely love his/her lifestyle as a job and believe in its success, there is always a good chance that business will succeed. At first, lifestyle entrepreneurs can earn less (which is "normal" for each new business), but if they invest in themselves, learn and develop, they succeed.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-3" style="display: none" class="questionWindow" data-value="3">
            <div class="question">
                <h3 class="text-center">Has a Life-style entrepreneurship a strong social aspect?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-3" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Life-style entrepreneurship has a very strong social aspect, which helps to solve various problems of disadvantaged people, e.g. social, economic, health, isolation, unemployment.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>Life-style entrepreneurship has a very strong social aspect, which helps to solve various problems of disadvantaged people, e.g. social, economic, health, isolation, unemployment.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-4" style="display: none" class="questionWindow" data-value="4">
            <div class="question">
                <h3 class="text-center">Is the knowledge on opportunities for personal, professional and/or business activities, economy basics essential for Life-Style Entrepreneurship?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="true">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="false">No</button>
            </div>
        </div>
        <div id="feedback-4" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="yes">
                <p class="green">Your answer is correct!</p>
                <span>Opportunities for personal, professional and/or business activities, including ‘bigger picture’ issues that provide the context in which people live and work, understanding of the economy basics, and the opportunities and challenges including an employer or organization are essential for Life-Style Entrepreneurship.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="no">
                <p class="red">This is not the expected answer!</p><br/>
                <span>Opportunities for personal, professional and/or business activities, including ‘bigger picture’ issues that provide the context in which people live and work, understanding of the economy basics, and the opportunities and challenges including an employer or organization are essential for Life-Style Entrepreneurship.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-next">Ok</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="window-5" style="display: none" class="questionWindow" data-value="5">
            <div class="question">
                <h3 class="text-center">Can the negative entrepreneurial attitude help to run the Life-Style business?</h3>
            </div>
            <div class="answers">
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="yes" data-val="false">Yes</button>
                <button type="button" class="btn btn-outline-primary ansBtn" data-name="no" data-val="true">No</button>
            </div>
        </div>
        <div id="feedback-5" class="feedbackScreen">
            <div class="feedback correctBox" style="display: none;" data-name="no">
                <p class="green">Your answer is correct!</p>
                <span>A negative attitude will damage confidence, harm performance, paralyze your mental skills and may affect your health. With a positive attitude you will feel in control and confident and you will perform at your best. The life- style business can help run entrepreneurial attitude initiative; pro-activity; independence and innovation in personal, social and work life, motivation and determination to meet objectives.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-finish">Finish</button>
                    </div>
                </div>
            </div>
            <div class="feedback incorrectBox" style="display: none;" data-name="yes">
                <p class="red">This is not the expected answer!</p><br/>
                <span>A negative attitude will damage confidence, harm performance, paralyze your mental skills and may affect your health. With a positive attitude you will feel in control and confident and you will perform at your best. The life- style business can help run entrepreneurial attitude initiative; pro-activity; independence and innovation in personal, social and work life, motivation and determination to meet objectives.</span>
                <div class="row justify-content-center row-margin">
                    <div class="col-md-2">
                        <button class="btn btn-success btn-finish">Finish</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="finish questionWindow text-center" style="display: none;">
            <h3 class="green">Thank you for completing the exercise!</h3>
            <div class="points"></div>
            <p>If you are satisfied with your results, please continue to the exercises list.</p>
            <p>If you are not totally satisfied with your results, you are kindly advised to repeat the exercise as it will help you to deepen your knowledge on the topic.</p>
            <div class="finnishNavBtns">
                <div class="row justify-content-center">
                    <div class="col-md-4">
                        <a href="exercise2_en.html" class="btn btn-primary repeat">Repeat the exercise</a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="btn btn-success backToList">Back to exercises list</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#start-quiz').click(function(e){
        e.preventDefault();
        $('.home-screen').hide();
        $('#window-0').show(400);
        $('.task-desc').show();
    });

    var testWindows = $('.questionWindow');
    var points = 0;
    $(testWindows).each(function() {
        var current = parseInt($(this).attr('data-value'));
        var ansbtn = $('#window-' + current + ' .answers .ansBtn');
        $(ansbtn).each(function() {
            $(this).click(function() {
                var attr = $(this).attr('data-name');
                var feedbackScreen = $('#feedback-' + current + ' .feedback');
                var dataVal = $(this).attr('data-val');
                if(dataVal === 'true') {
                    points++;
                }
                $(feedbackScreen).each(function() {
                    var feedback = $(this).attr('data-name');
                    if (feedback === attr) {
                        $(this).show(400);
                        $('#window-' + current).hide();
                    }
                });
                $('.finish .points').html('<p class="scoreMessage">You answered correctly <strong>'+points+' of 6</strong> questions.</p>');
            });
        });
        var nextBtn = $('#feedback-' + current + ' .feedback .btn-next');
        $(nextBtn).click(function(){
            $('#feedback-' + current).hide();
            $('#window-' + (current+1)).show(400);
        });

        var nextFinish = $('#feedback-5 .feedback .btn-finish');
        $(nextFinish).click(function() {
            $('#feedback-5').hide();
            $('.finish').show(400);
        })
    });
</script>
