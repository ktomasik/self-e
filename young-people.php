<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 22.01.2019
 * Time: 09:07
 */
?>

<h6 style="background-color: #007bff; padding: .5rem; color: #fff" class="text-center">SELF-E: INNOVATIVE TRAINING
    COURSE FOR YOUNG LEARNERS BASED ON SOCIAL MENTORING</h6>
<h2 class="text-center young-title">PART FOR YOUNG PEOPLE</h2>
<div class="row justify-content-center">
    <div class="col-md-10 col-sm-12">
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-7">
                <a href="#">
                    <img src="/img/young-people/guide.png" class="img-fluid" alt="guide"/>
                </a>
            </div>
            <div class="col-md-6 col-sm-7">
                <a href="/pathway.html">
                    <img src="/img/young-people/pathway.png" class="img-fluid" alt="pathway"/>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-10">
                <a href="/success-stories.html">
                    <img src="/img/young-people/best_practices.png" class="img-fluid" alt="best practices"/>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-10">
                <a href="#">
                    <img src="/img/young-people/assessment.png" class="img-fluid" alt="assessment tool"/>
                </a>
            </div>
        </div>
    </div>

</div>
