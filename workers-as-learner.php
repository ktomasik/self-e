<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 21.01.2019
 * Time: 13:44
 */

$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
if (!in_array($lang, ['en', 'lt', 'gr', 'bg', 'pl'])) {
    $lang = 'en';
}

?>

    <div class="row" style="margin-bottom: 2rem;">
        <div class="col-md-3">
            <a href="/youth-workers.html" class="btn btn-success">Back to YOUTH-WORKER menu</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h6 style="background-color: #007bff; padding: .5rem; color: #fff" class="text-center">INNOVATIVE TRAINING
                ON SELF-EMPLOYMENT FOR YOUNG PEOPLE BASED ON MENTORING</h6>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-11 col-md-10">
            <h2 class="text-center worker-title">YOUTH-WORKER AS LEARNER</h2>
            <div class="row">
                <div class="col-sm-6">
                    <a href="/curriculum<?= $lang !== 'en' ? '_' . $lang : ''?>.html">
                        <img src="/img/youth-workers/learners_curriculum.png" class="img-fluid" alt="curriculum"/>
                    </a>
                </div>
                <div class="col-sm-6">
                    <a href="#">
                        <img src="/img/youth-workers/learners_guide.png" class="img-fluid" alt="guide"/>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <a href="/youth-workers-module1.html">
                        <img src="/img/youth-workers/learners_module1.png" class="img-fluid" alt="module 1"/>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a href="/youth-workers-module2.html">
                        <img src="/img/youth-workers/learners_module2.png" class="img-fluid" alt="module 2"/>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a href="/youth-workers-module3.html">
                        <img src="/img/youth-workers/learners_module3.png" class="img-fluid img-mod3" alt="module 3"/>
                    </a>
                </div>

                <div class="modules-title col-sm-8">
                    <img src="/img/youth-workers/learners_modules_name.png" alt="label" class="img-fluid"/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="/assessment_worker.html">
                        <img src="/img/youth-workers/learners_assessment.png" alt="assessment" class="img-fluid"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
