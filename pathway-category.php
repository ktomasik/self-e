<?php
include_once "functions.php";
session_start();

if(!isset($_GET['category']))
{
    header("Location: /pathway.html");
    exit;
}

$category = (int)$_GET['category'];

$exList = getExercisesByThemeNo($category);

?>

<div class="row" style="margin-bottom: 2rem;">
    <div class="col-md-4">
        <a href="/pathway.html" class="btn btn-success"><?= $backToPathway ?></a>
    </div>
</div>
<h2 class="text-center young-title"><?= $themes[$category]; ?></h2>
<p class="text-center"><strong><?= $setOfPracticle ?></strong></p>

<ul>
    <?php foreach($exList as $key => $ex):?>
        <li><a href="/pathway-exercise.html?id=<?= $key ?>&category=<?= $category ?>"><?= $ex ?></a></li>
    <?php endforeach; ?>
</ul>
