<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 offset-md-6">
            <span class="dark-blue"><strong>Project number No.: 2017-3-LT02-KA205-005536</strong></span>
        </div>
        <h1>Outcomes</h1>
        <p><span class="green"><strong>O1</strong></span> - The Toolkit for youth workers „Social mentoring as innovative training pathway to lifestyle self-employment – SELF-E”</p>
        <p><span class="green"><strong>O2</strong></span> - Set of practical exercises/OERs “ Pathway to lifestyle self- employment“<br/></p>
        <p><span class="green"><strong>O3</strong></span> - Assessment tool for validation of competence “Sense of initiative and entrepreneurship”</p>
    </div>
</div>

