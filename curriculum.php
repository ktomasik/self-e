<?php
/**
 * Created by PhpStorm.
 * User: lklapa
 * Date: 19.05.2019
 * Time: 17:26
 */
?>

<style>
    .curriculum-menu {
        border: 1px solid #8dca2a;
        -webkit-border-radius: .25rem;
        -moz-border-radius: .25rem;
        border-radius: .25rem;
        padding: 20px;
    }
    table.partners-table td {
        padding: 10px;
    }
    table.partners-table td:first-of-type {
        text-align: right;
        font-weight: bold;
        vertical-align: middle;
    }
    table.partners-table img {
        max-width: 200px;
        max-height: 80px;
    }
    .curriculum-image p {
        font-size: 0.8em;
        color: #333333;
        font-style: italic;
    }
    table.curriculum-plan tr:nth-child(even) {
        background: rgb(238,255,229);
    }
    table.curriculum-plan tr:nth-child(odd) {
        background: rgb(224,241,252);
    }
    table.curriculum-plan tr:first-child {
        background: #fefefe;
    }
    table.curriculum-plan tr > td:nth-of-type(3) {
        text-align: center;
    }
    ol.curriculum-plan-list li:nth-child(even) {
        color: rgb(79,98,40);
    }
    ol.curriculum-plan-list li:nth-child(odd) {
        color: rgb(36,64,97);
    }
    #v-pills-tabContent h3 {
        color: #7ad0fa;
        border-bottom: 1px solid #8dca2a;
        margin-bottom: 24px;
        padding-bottom: 4px;
    }
</style>
    <div class="row" style="margin-bottom: 2rem;">
        <div class="col-md-12">
            <a href="/workers-as-learner.html" class="btn btn-success">Back to YOUTH-WORKER AS LEARNER menu</a>
        </div>
    </div>

<div class="row">
    <div class="col-3 small curriculum-menu">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-tab-0" data-toggle="pill" href="#v-pills-0" role="tab" aria-controls="v-pills-0" aria-selected="true">Home</a>
            <a class="nav-link" id="v-pills-tab-1" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="false">Abbreviations</a>
            <a class="nav-link" id="v-pills-tab-2" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">1. Introduction of the Project</a>
            <a class="nav-link" id="v-pills-tab-3" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">2. Aim, objectives and content of the Curriculum</a>
            <a class="nav-link" id="v-pills-tab-4" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">3. Learning outcomes</a>
            <a class="nav-link" id="v-pills-tab-5" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false">4. Teaching and learning strategies</a>
            <a class="nav-link" id="v-pills-tab-6" data-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-6" aria-selected="false">5. Assessment strategy</a>
            <a class="nav-link" id="v-pills-tab-7" data-toggle="pill" href="#v-pills-7" role="tab" aria-controls="v-pills-7" aria-selected="false">6. Teaching and learning facilities</a>
            <a class="nav-link" id="v-pills-tab-8" data-toggle="pill" href="#v-pills-8" role="tab" aria-controls="v-pills-8" aria-selected="false">7. Training plan for youth worker</a>
            <a class="nav-link" id="v-pills-tab-9" data-toggle="pill" href="#v-pills-9" role="tab" aria-controls="v-pills-9" aria-selected="false">8. MODULE I</a>
            <a class="nav-link" id="v-pills-tab-10" data-toggle="pill" href="#v-pills-10" role="tab" aria-controls="v-pills-10" aria-selected="false">9. MODULE II</a>
            <a class="nav-link" id="v-pills-tab-11" data-toggle="pill" href="#v-pills-11" role="tab" aria-controls="v-pills-11" aria-selected="false">10. MODULE III</a>
        </div>
        <div class="text-center">
            <hr />
            <h4>Download the curriculum</h4>
            <a href="/files/Self-E%20Curriculum%20Final.pdf" target="_blank">PDF</a> |
            <a href="/files/Self-E%20Curriculum%20Final.docx" target="_blank">Word</a>
        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-0" role="tabpanel" aria-labelledby="v-pills-0">
                <h1>Curriculum For Youth Workers on training course SELF-E</h1>

                <em>Social mentoring as innovative training pathway to lifestyle self-employment</em>

                <table class="partners-table mt-5">
                <tr><td>SOCIAL INNOVATION FUND (LT)</td><td><img src="img/partners/logo-sif.png" /></td></tr>
                <tr><td>CARDET (CY)</td><td><img src="img/partners/logo-c.png" /></td></tr>
                <tr><td>CWEP (PL)</td><td><img src="img/partners/logo-cwep.png" /></td></tr>
                <tr><td>KNOW AND CAN ASSOCIATION (BG)</td><td><img src="img/partners/logo-kc.png" /></td></tr>
                <tr><td>KNJUC (LT)</td><td><img src="img/partners/logo-knjuc.png" /></td></tr>
                <tr><td>VINI (LT)</td><td><img src="img/partners/logo-vini.png" /></td></tr>
                </table>

            </div>
            <div class="tab-pane fade" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1">
                <h3>Abbreviations</h3>
                <p>EU – European Union<br />
                ICT – Information and communications technologies<br />
                LSE – Life-Style Entrepreneurship<br />
                NEET – Not in education, Employment or Training<br />
                NGO – Non-governmental organization<br />
                OER – Open education resource. The description of OER is on page 6<br />
                SE – Self-employment<br />
                </p>
            </div>
            <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2">
                <h3>1. Introduction of the Project</h3>
                <p><strong>PROJECT AIM</strong></p>
                <p>The main aim of the SELF-E project is to promote quality of youth work in order to foster lifestyle self-employment of young people with fewer opportunities, including NEETs.</p>
                <p>SELF-E project contributes to development of ET 2020 framework strategy that acknowledges the importance of individual skills in the era of the knowledge-based economy.</p>
                <p>The main impact on young people with fewer opportunities is increasing their participation in labour market, as they will get relevant and high quality skills and competence, needed to become self-employed as well they will be motivated by mentors to build self-employment on Life-Style Entrepreneurship. The impact is foreseen on decreasing unemployment and better integration of this target group to the labour market and society, and it will contribute to the achievement of Europe 2020 benchmark of increasing employment rate by 75%.</p>
                <p><strong>OBJECTIVES</strong></p>
                <ol>
                    <li>To strengthen the capacity of youth workers to organise innovative non-formal learning on youth lifestyle self-employment via mentoring.</li>
                    <li>To support youth workers in applying new methods for motivating young people with fewer opportunities, including NEETs to learn and become self-employed (with special emphasis on Life-Style Entrepreneurship).</li>
                    <li>To foster the transition of young people from youth to adulthood through integration in the labour market by lifestyle self-employment.</li>
                    <li>To develop youth competences “Sense of initiative and entrepreneurship”.</li>
                    <li>To create opportunities to validate obtained competences “Sense of initiative and entrepreneurship”.</li>
                </ol>
                <p><strong>MAIN OUTCOMES</strong></p>
                <p>Three main intellectual outputs are:</p>
                <ol>
                    <li>Toolkit for youth workers „Social mentoring as innovative training pathway to lifestyle self-employment – SELF-E”.</li>
                    <li>Set of practical exercises/OERs for young learners with fewer opportunities, including NEETs “Pathway to lifestyle self- employment”.</li>
                    <li>Assessment tool for validation competence “Sense of initiative and entrepreneurship” of young learners with fewer opportunities, including NEETs.</li>
                </ol>
                <p><strong>TARGET GROUP</strong></p>
                <ol>
                    <li>Youth workers who work with young people with fewer opportunities:
                        <ul>
                            <li>Youth workers for youth centre</li>
                            <li>Youth workers from NGO</li>
                            <li>Youth workers from organizations dealing with disabled young people</li>
                            <li>Youth workers for the migrant organizations</li>
                            <li>Youth workers from Labour exchange offices youth departments</li>
                        </ul>
                    </li>
                    <li>Young people with fewer opportunities, including NEETs</li>
                </ol>
                <div class="alert alert-info">
                    More information about the SELF-E partnership, results and outcomes may be found on the website:
                    <a href="http://self-e.lpf.lt/" target="_blank">http://self-e.lpf.lt/</a>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3">
                <h3>2. Aim, Objectives and Content of the Curriculum “Social mentoring as innovative training pathway to lifestyle self-employment -SELF-E”</h3>
                <p>The <strong>main aims</strong> of the curriculum are:</p>
                <ul>
                    <li>to define the framework of training course for youth workers to strengthen their capacity to deliver innovative non-formal training on lifestyle self-employment for young learners with fewer opportunities, including NEETs;</li>
                    <li>to introduce the innovative training pathways based on social mentoring for motivating young people with fewer opportunities, including NEET’s, to learn and become lifestyle entrepreneur.</li>
                </ul>
                <p>The <strong>objectives</strong> of the curriculum are:</p>
                <ul>
                    <li>to describe the content of the training modules;</li>
                    <li>to explain objectives and main learning outcomes of the SELF-E training course;</li>
                    <li>to define mentoring on lifestyle self-employment as a new non-formal learning pathway;</li>
                    <li>to define the concept of lifestyle self-employment;</li>
                    <li>to introduce the main peculiarities of lifestyle entrepreneurship as a useful alternative of youth self-employment in the contemporary labour market;</li>
                    <li>to find out the ways of motivation the youth to become lifestyle self-employed;</li>
                    <li>to define the Training plan for training of youth workers on innovative “SELF-E” training course based on social mentoring and OERs;</li>
                    <li>to introduce the youth learners to opportunities and importance of validation of their competences “Sense of initiative and entrepreneurship”;</li>
                    <li>to present the framework of the Lesson’s plan for SELF-E training course for young learners with fewer opportunities, including NEETs, based on social mentoring and OERs on lifestyle self-employment.</li>
                </ul>
                <p>The Curriculum defines the content of training course for youth worker „Social mentoring as innovative training pathway to lifestyle self-employment – SELF-E” and consists of three Modules:</p>
                <ol>
                    <li>Social mentoring on lifestyle self-employment (LSE) as a new non-formal learning pathway for young people with fewer opportunities, including NEETs.</li>
                    <li>Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship.</li>
                    <li>Validation of the competence “Sense of initiative and entrepreneurship” of young learners with fewer opportunities”.</li>
                </ol>
                <p>The SELF-E training course is based on social mentoring and using a set of practical exercises based on Open Education Resources (OERs).</p>
                <p>The SELF-E training course duration is 27 hours. It covers both face-to-face and online learning. Teaching and learning strategy is explained in 4<sup>th</sup> part of the Curriculum.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4">
                <h3>3. Learning Outcomes</h3>
                <p><img src="img/curriculum/image007.jpg" class="float-left"/>After successful completion of training course “Social mentoring as innovative training pathway to lifestyle self-employment SELF-E”, youth workers will improve their competences of becoming mentors of social mentoring process on lifestyle self-employment (LSE) and effectively organize training of young learners with fewer opportunities.</p>
                <p>By the end of the course, the youth workers will be able to:</p>
                <ul>
                    <li>Understand the concept of lifestyle self-employment.</li>
                    <li>Conceptualise the main peculiarities of lifestyle entrepreneurship as a useful alternative of youth self-employment in contemporary labour market.</li>
                    <li>Analyse different types of LSE for self-employment.</li>
                    <li>Understand business principles in LSE and create business plans.</li>
                    <li>Develop, implement and monitor marketing strategies for LSE.</li>
                    <li>Define soft skills needed to become lifestyle entrepreneur.</li>
                    <li>Use the different methods and tools to increase motivation of young people with fewer opportunities, including NEETs, to become lifestyle self-employed by exploring these new business opportunities.</li>
                    <li>Use Social Media to promote Life-Style Entrepreneurship.</li>
                </ul>
                <p>Training course SELF-E provides theoretical material and practical tools for youth worker to work with target group: young learners with fewer opportunities, including NEETs.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5">
                <h3>4. Teaching and Learning Strategies</h3>
                <p>The pedagogical strategy of this training course is based on the blended learning approach: combination of traditional and virtual learning via developed e-learning platform as Open educational resources with the possibility to perform self-study at convenient time and place.</p>
                <div class="alert alert-info">
                    <strong>Blended learning</strong> is an education program (formal or informal) that combines online digital media with traditional classroom methods. It requires the physical presence of both teacher and student, with some elements of student control over time, place, path, or pace.
                </div>
                <div class="row">
                    <div class="col-md-5"><img class="img-fluid" src="img/curriculum/image009.png"/></div>
                    <div class="col-md-7"><img class="img-fluid" src="img/curriculum/image008_en.png"/></div>
                </div>
                <div class="alert alert-info">
                    <strong>Open educational resources</strong> (OERs) are digital materials that can be re-used for teaching, learning, research and more, made available free through open licenses, which allow uses of the materials that would not be easily permitted under copyright alone. OERs include full courses, course materials, modules, textbooks, streaming videos, tests, software, and any other tools, materials, or techniques used to support access to knowledge.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image010.jpg" class="img-fluid"/>
                    <p>https://www.google.es/search?q=open+education+resources&amp;source=lnms&amp;tbm=isch&amp;sa=X&amp;ved=0ahUKEwi6vYTl_fPcAhVFtosKHWNGDNsQ_AUICigB&amp;biw=1920&amp;bih=974#imgrc=Cl32e5SzCoJjFM:</p>
                </div>
                <p>The course is innovative as it is built on ICT-based <strong>reversed training methodology </strong>using Open Educational Recourses (OERs).</p>
                <div class="alert alert-info">
                    <strong>A Reversed training methodology</strong> means that an educator has the role of a facilitator and guides trainees/learners (youth workers) to make an initial analysis of the on-line training materials presented as OERs by themselves. After fulfilling their independent learning tasks, the trainees discuss the results with the facilitator in face-to-face meetings. It means that trainees (youth workers) use the <strong>flipped learning method</strong> to improve their skills and competences.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image011.png" class="img-fluid"/>
                    <p>Source of the picture: https://sites.google.com/a/mahidol.edu/how-gen-z-learn-by-il-mu/techforactivelearning/7&#8212;tools-for-flipped-classroom</p>
                </div>
                <p>Deepening knowledge for each module is ensured by the possibility to use the <strong>developed e-Toolkit for youth workers </strong>as OERs. The e-learning is very important learning part of training course, which contains 27 academic hours in total. The Methodological materials for youth workers, future managers or mentors for disadvantaged young people, consists of theory and practical tasks. To ensure active participation of youth workers and exhaustive understanding of the Modules’ content, four face-to-face training sessions (8 academic hours in total) and three online sessions (19 academic hours in total) are incorporated into teaching and learning strategy.</p>
                <ul>
                    <li>The first face-to-face session is held with the aim to assess the competence of youth worker to become mentor on LSE and to run the training course for disadvantaged youth-learners. During the first session the Module I “Mentoring on lifestyle self-employment as a new non-formal learning path way for young people with fewer opportunities, including NEETs” is introduced.</li>
                    <li>The aim of the second face-to-face training session is to deepen the participants&#8217; (youth workers) knowledge through practical exercises, after they study the Module’s I material online and to introduce the Module II “Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship”. The overview of 36 (OERs is presented and the participants are suggested to fulfil at least one OER as well to fill in the “Feedback sheet” developed for each OER.</li>
                    <li>The third face-to-face session comprises two parts:</li>
                    <ul>
                        <li>Analysis of the importance of OERs to facilitate the mentoring on LSE.</li>
                        <li>Introduction to Module III “Validation of the competences “Sense of initiative and entrepreneurship””.</li>
                    </ul>
                    <li>The fourth face-to face session is a final session and consist of few exercises on Module III. It serves for the collection of feedbacks for the overall training course and the discussions of the plans on how to organize the mentoring process on LSE for disadvantaged youth. The participants also evaluate their competences and impact of the training course by using self-assessment tool. At the end of this session, it is recommended to give the participants the certificates on completing of the training course “<em>Social mentoring as innovative training pathway to lifestyle self-employment -SELF-E”</em>.</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6">
                <h3>5. Assessment Strategy</h3>
                <p>Assessment of the participants’ knowledge and competences will be performed in three ways (upon successful completion of the course, the participants will be awarded certificates of achievement):</p>
                <ul>
                    <li>taking a self-assessment test on-line (pre-assessment and post assessment);</li>
                    <li>participating in group work and completing practical assignments during the face-to-face sessions;</li>
                    <li>reflecting on one’s own learning experiences after the training course.</li>
                </ul>
                <p>At the beginning of the SELF-E training course participants will be asked to perform a self-assessment test on-line to determine the level of their knowledge and competences regarding the training material. At the end of the course, they will be requested to retake the self-assessment test on-line in order to evaluate their progress in learning.</p>
                <p><img src="img/curriculum/image012.png" class="float-left" />In the pre-assessment and final assessment, 3 aspects are evaluated:</p>
                <ul>
                    <li>Knowledge of mentoring process;</li>
                    <li>Knowledge on lifestyle entrepreneurship;</li>
                    <li>Knowledge of validating the competence “Sense of initiative and entrepreneurship”.</li>
                </ul>
                <p>There are 10 questions related with evaluation of knowledge of mentoring, 10 questions related with evaluation of knowledge on lifestyle entrepreneurship and 5 questions related with evaluation of knowledge on validation of competence ““Sense of initiative and entrepreneurship”, in total 25 closed questions. This assessment tool will be programmed and will be available on-line free of charge.</p>
                <p>As mentioned above second way of assessment is when participants during face-to-face sessions complete practical assignments by doing exercises in groups, reflect each other and discuss.</p>
                <p>Thirdly, at the end of the final session, the participants will be asked to reflect on their learning experiences during the training course, both face-to-face and via the e-learning platform. The participants will share how well they succeeded in applying the knowledge acquired during the training course. This activity will promote further learning and practical usage of the knowledge and competences in everyday teaching/training activities.</p>
                <p>Those participants, who provided the required correct answers to at least of 20 of questions in of the final self-assessment test and are were active in practical activities and reflections, will be certified.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-7">
                <h3>6. Teaching and Learning Facilities</h3>
                <p>The institutions which will organize the SELF-E training courses for youth workers –learners according to the prepared training plan, should ensure a convenient learning environment, technical equipment and tools, necessary for providing the teaching and learning process based on the blended learning approach within the following facilities:</p>
                <ul>
                    <li>classroom with multimedia projector and a computer with the possibility to use Power Point for face-to- face meetings;</li>
                    <li>access to the personal computers with the internet connection to the Open Education Resources and Learning Platform of SELF-E project (<a href="http://self-e.lpf.lt/">http://self-e.lpf.lt/</a>);</li>
                    <li>other organizational tools for face-to-face meetings (board, handouts, paper and etc.).</li>
                </ul>
                <p>The trainers of SELF-E course should be prepared for teaching process respectively.</p>
                <p>They should:</p>
                <img src="img/curriculum/image013.jpg" class="float-right" />
                <ul>
                    <li>create a psychologically friendly learning environment;</li>
                    <li>be acquainted with the background of the audiences and be aware of their needs and expectations;</li>
                    <li>have experience on how to work with young people with fewer opportunities, including NEETs;</li>
                    <li>have the knowledge and skills to organize e-learning sessions;</li>
                    <li>have a good knowledge of the teaching content (Modules I-III);</li>
                    <li>have essential personal characteristics: to have good communication skills, to be tolerant, to be able motivate learners and poses of positive self – evaluation skills;</li>
                    <li>have to have be good facilitator’s skills like abilities to initiate discussions, collect reflections, react on feedback, provide summarized answers, etc.</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-8" role="tabpanel" aria-labelledby="v-pills-8">
                <h3>7. Training Plan for youth worker</h3>
                <p>The training plan is developed to help to organize training for youth worker on how to deliver effectively the “SELF-E” training course for youth with fewer opportunities. This plan gives a step-by-step overview of the blended learning process using reversed training methodology defined in the Part 4.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>No</strong></td>
                        <td><strong>Method</strong></td>
                        <td><strong>Duration (*a. h)</strong></td>
                        <td><strong>Content/Topics</strong></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Face-to-face</td>
                        <td>2</td>
                        <td>
                            <p>Introduction of the SELF-E Training Course for youth workers.</p>
                            <p>Initial assessment of competence of youth worker to become mentor on LSE and to run the training course for disadvantaged youth-learners.</p>
                            <p>Introduction of Module I “Social Mentoring on lifestyle self-employment as a new non-formal learning pathway”.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Online session</td>
                        <td>6</td>
                        <td>
                            <p>Self-learning of Module I “Social Mentoring on lifestyle self-employment as a new non-formal learning pathway”.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Face-to-face</td>
                        <td>2</td>
                        <td>
                            <p>Group work exercises on Module I, discussions.</p>
                            <p>Introduction of the Module II “Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship”.</p>
                            <p>Fulfilment of at least one OERs and filling the “Feedback sheet” for this OER.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Online session</td>
                        <td>9</td>
                        <td>
                            <p>Self-learning of Module II “Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship”.</p>
                            <p>Fulfilling the set of OERs and filling in the “Feedback sheet” for all OERs in order to bring it to 5<sup>th</sup> session.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Face-to-face</td>
                        <td>2</td>
                        <td>
                            <p>Discussions on results of self-learning on Module II on the basis of “Feedback sheet” form filled in by learners. Analyse the importance of OERs to facilitate the mentoring on LSE.</p>
                            <p>Introduction to Module III “Validation of the competences “Sense of initiative and entrepreneurship””.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Online session</td>
                        <td>4</td>
                        <td>
                            <p>Self-learning of Module III “Validation of the Competences “Sense of initiative and entrepreneurship””. Fulfilling the test for assessment, which will be provided to youth with few opportunities during the SEL-E training course.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Face-to-face</td>
                        <td>2</td>
                        <td><p>Final session:</p>
                            <ul>
                                <li>Work group exercises on Module III.</li>
                                <li>Feedback collection for the whole training course, discussions.</li>
                                <li>Post-assessment for youth workers-learners.</li>
                                <li>Awards – certificates.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>TOTAL</strong></td>
                        <td class="text-center"><strong>27</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <p class="text-right small"><em>*Note: 1 academic hour = 45 minutes</em></p>
                <p>As seen in table above there are seven main stages.</p>
                <img src="img/curriculum/image014.jpg" class="float-right" />
                <ol class="curriculum-plan-list">
                    <li>During the first face-to-face session a brief introduction of the SELF-E Training Course is given. Participants are invited to make initial assessment of competence of youth worker to become mentor on LSE and to run the training course for disadvantaged youth-learners. After completing it, they are introduced to Module I: “Social Mentoring on lifestyle self-employment as a new non-formal learning pathway”.</li>
                    <li>After this face-to-face session, an online session on Module I follows up. It is a self-learning session.</li>
                    <li>During the second face-to-face session, there are group work exercises on Module I and discussions for participants to show what they learned, to share experience of self-learning and to raise questions for further learning. Then they are introduced with Module II “<em>Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship</em>”. At least one OER is fulfilled and participants share their opinion and experience on filling in the “Feedback sheet”.</li>
                    <li>After this face-to-face session, an online session on Module II follows up. It is a self-learning session. In addition to study of the theoretical material for Module II, learners fulfil the set of OERs and fill in the “Feedback sheet” for all OERs in order to bring it to third face-to-face session. A great importance is given to “Feedback sheet”, where learners during learning process have to take notes, answer questions. In such way learners have a great base for giving feedback related to using of set of OERs in facilitating mentoring, thus making discussions during the third face-to-face session more structured.</li>
                    <li>During the third face-to-face session, participants share their experience of self-learning on Module II on the basis of “Feedback sheet” form filled by learners. They also discuss and analyse the importance of OERs to facilitate the mentoring on LSE. They are also introduced with Module III “Validation of the Competences “Sense of initiative and entrepreneurship””.</li>
                    <li>After this face-to-face session, an online session on Module III follows up. In additional to studying the theoretical materials, the learners are asked to try the on-line test for assessment the skills of youth with few opportunities on competence “Sense of initiative and entrepreneurship”. Thus, it is important task for this session, as youth workers are prepared to provide this test to youth with few opportunities during the SEL-E training course.</li>
                    <li>During the final session, learners participate in work group exercises on Module III, discuss and share their results of self-learning and assessment tool. In order to assess the progress of the training course, the learners will fulfil post-assessment questionnaire for youth workers following with the Certifications.</li>
                </ol>
            </div>
            <div class="tab-pane fade" id="v-pills-9" role="tabpanel" aria-labelledby="v-pills-9">
                <h3>8. MODULE I. Social Mentoring on lifestyle self-employment as a new non-formal learning pathway</h3>
                <p><strong>The aim of module is:</strong></p>
                <p>To introduce the youth workers to the social mentoring on lifestyle self-employment as a new non-formal learning pathway in order to encourage, promote and support mentees’ personal and professional development towards starting their own business.</p>
                <p><strong>The objectives of the Module I are:</strong></p>
                <ul>
                    <li>to define the concepts of mentoring on lifestyle self-employment: Why social mentoring is innovative and valuable non-formal learning pathway for lifestyle self-employment for young people with fewer opportunities, including NEETs;</li>
                    <li>to define the main roles and tasks of mentee and mentor in social mentoring on lifestyle self-employment;</li>
                    <li>to explain explicitly the main roles and tasks of a mentor in the social mentoring on lifestyle self-employment;</li>
                    <li>to explain the benefits and challenges of mentoring on lifestyle self-employment;</li>
                    <li>to define the main steps of a social mentoring on LSE process;</li>
                    <li>to define the ways on how to ensure an effective social mentoring on LSE;</li>
                    <li>to introduce methods on how to select mentors and to match them with young people-mentees;</li>
                    <li>to explain how to ensure the desired quality standards of social mentoring on lifestyle self-employment;</li>
                    <li>to introduce how to organize initial, mid-term and final sessions of social mentoring on lifestyle self-employment;</li>
                    <li>to define the aims of monitoring of quality of social mentoring on lifestyle self-employment;</li>
                    <li>to define the ethical and professional codes for both youth-workers mentors and young mentees;</li>
                    <li>to introduce the managing tools for the social mentoring on lifestyle self-employment.</li>
                </ul>
                <p><strong>Description of the Module I:</strong></p>
                <p>In this Module learner will understand the meaning of mentoring &#8211; it is a developmental partnership through which one person shares knowledge, skills, information and perspective to foster the personal and professional growth of someone else. Different types of mentoring are analysed as there are two types: pair (individual) and group. Mentoring may also have many forms: e.g. face-to-face, e-Mentoring and blended mentoring. Learners’ trough case studies and theoretical material will have a possibility to explore it.</p>
                <p>Learners in this Module will understand how mentors support, courage and open their own networks for Mentees. Special focus will be given to mentoring on employment-related issues, where Mentors help Mentees to get employed or to start an enterprise.</p>
                <p>Mentoring process is analysed in this Module from both perspectives – Mentor and Mentee, as it requires both side involvement, respect, responsibility, and equal expectations. Strong connection between mentee, mentor and manager is the most important point of successful mentoring. For the Mentee it is a process offering excellent opportunities for personal and professional development. Supported by the Mentor, the Mentee can develop his/her employment-related skills and competences. Mentor gets benefits as well, they are provided and discussed with learners in this Module.</p>
                <p>Learners will learn about effective discussions taking place during the mentoring relationship &#8211; that it should be relevant to the basic aim and the Mentee should focus on the entrepreneurial plan and issues which affect it.</p>
                <p>Mentoring benefits are explained in this Module as well, as it often raises understanding between generations, cultures and different kinds of people. Mentoring is a good method of learning and development and, for the Mentee, it gives courage to meet eventual conflicts and to take risks if needed.</p>
                <p><strong>Learning outcomes</strong></p>
                <p>By the end of the Module I the participants (youth-workers) will be able to:</p>
                <ul>
                    <li>understand the concepts of mentoring on lifestyle self-employment, mentee, mentor and manager;</li>
                    <li>distinguish the roles of the manager, mentor and mentee in the process of social mentoring on lifestyle self-employment;</li>
                    <li>be able to identify and explain the benefits, strong points and challenges of social mentoring on lifestyle self-employment;</li>
                    <li>understand the importance of facilitating tools used during the process of social mentoring on lifestyle self-employment;</li>
                    <li>be able to organise and manage effectively the social mentoring process on lifestyle self-employment (including matching of mentees and mentors, organizing initial session, mid-term monitoring and final sessions);</li>
                    <li>know how to set the quality standards and ethical and professional codes for mentors and mentees.</li>
                </ul>
                <p>Social mentoring is based on the blended learning approach: combination of traditional and virtual learning via developed e-learning platform with the set of OERs. The Module II represents how to use OERs to facilitate the social mentorship.</p>
                <p><strong>MAIN TRAINING PHASES USING MENTORING:</strong></p>
                <img src="img/curriculum/image015_en.png" class="img-fluid" />
            </div>
            <div class="tab-pane fade" id="v-pills-10" role="tabpanel" aria-labelledby="v-pills-10">
                <h3>9. Module II “Facilitating the social mentoring process on LSE by using the set of open educational resources (OERs) on Life-Style Entrepreneurship”</h3>
                <p><strong>The aim of module is:</strong></p>
                <p>To introduce the concept of lifestyle self-employment with special focus on Life-Style Entrepreneurship.</p>
                <p><strong>The objectives of Module II are:</strong></p>
                <ul>
                    <li>to define concept of lifestyle self-employment;</li>
                    <li>to introduce the main peculiarities of Life-Style Entrepreneurship as a useful alternative of self- employment to youth in nowadays labour market;</li>
                    <li>to introduce the methods for motivation of youth to become lifestyle self-employed;</li>
                    <li>to overview the different types of Life-Style Entrepreneurship using success stories;</li>
                    <li>to define main Business principles in LSE and main parts of the business plan;</li>
                    <li>to analyse Marketing strategies in LSE;</li>
                    <li>to introduce the Set of practical exercises as Set of OERs ‘Pathway to lifestyle self-employment’ to be used to facilitate the social mentoring process on LSE;</li>
                    <li>to analyse the Lesson plan for innovative ‘SELF-E training course’ for young people with fewer opportunities, including NEETs, based on social mentoring and OERs.</li>
                </ul>
                <p><strong>Description of the Module II:</strong></p>
                <p>In this Module learner will compare main principles of entrepreneurship with the concept of Life-Style Entrepreneurship. The general definition of an entrepreneur is given and explained through cases. Similarities and differences between entrepreneurship and Life-Style Entrepreneurship are analysed. Evaluation tools for the possibility of becoming a lifestyle entrepreneur are given as not everyone can be a lifestyle entrepreneur &#8211; it requires specific personal traits, along with the desire to make a business out of their passion whether this is for financial or social gain or to prevent isolation.</p>
                <p>Also in Module II the business principles of entrepreneurship, which are essential to all types of business including Life-Style Entrepreneurship are presented. It is important to understand the principles of running and managing a business, including setting a business mission, completing thorough market research, developing a marketing strategy, financial planning and forecasting, people management and so on. A business plan is not just a fund-raising tool. In fact, it is a tool for understanding how and why the business is put together. It can be used to monitor progress; it forces regular reviews of the value proposition, marketing assumptions, operations plan and financial plan.</p>
                <p>Life-Style Entrepreneurship can be useful as an alternative to lifestyle self-employment. Therefore, Module II provides number of different drivers and barriers that Life-Style Entrepreneurship face and overcome and why they choose to start their business.</p>
                <p>Module II will use different methods of learning with the learner being engaged in activities through interactive sessions including discussions and quizzes, instructional methods, with a series of on-line tools. Both will be used: face-to-face learning and on-line learning. This Module will meet the needs of youth workers to get familiar with the basic knowledge about Life-Style Entrepreneurship and lifestyle self-employment in general as well as about OER’s: How to use OERs within the mentoring process?</p>
                <p>24 OERs will presented in three following thematic areas</p>
                <ul>
                    <li>Entrepreneurship and self-employment (SE)</li>
                    <li>Life-Style Entrepreneurship</li>
                    <li>Business principles and Marketing in Life-Style Entrepreneurship</li>
                </ul>
                <p>12 success stories will be used as motivation tool for young learners with fewer opportunities, including NEETs, to become life-style entrepreneurs.</p>
                <p>Youth worker will get familiar with created OERs during the SELF-E project and will get to know how to use them within the process of social mentoring on lifestyle self-employment.</p>
                <p><strong>Sample of lesson plan</strong> for innovative SELF-E training course for disadvantage learners based on social mentoring which includes face-to-face mentoring sessions and on-line learning using set of the practical exercises-OERs.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>SELF-E training phase</strong></td>
                        <td><strong>Sessions</strong></td>
                        <td><strong>Duration hours</strong></td>
                        <td><strong>Content; OERs</strong></td>
                    </tr>
                    <tr>
                        <td>Preparation</td>
                        <td>&nbsp;</td>
                        <td>At least two weeks prior 1<sup>st</sup> session</td>
                        <td>
                            <ul>
                                <li>Selecting mentors and mentees;</li>
                                <li>Matching mentors and mentees</li>
                                <li>Check eligibility</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Initial Mentoring meeting</td>
                        <td><strong>1<sup>st</sup> session. </strong>
                            <p>Initial face-to-face mentoring meeting</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Getting to know each other</li>
                                <li>Introduction to the social mentoring</li>
                                <li>Short presentation of SELF-E training course</li>
                                <li>Assessment of learners’ competences – using online tool</li>
                                <li>Developing individual plans for mentee</li>
                                <li>Introduction to learner’s guide</li>
                                <li>Setting tasks for individual on-line session</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>First thematic area – self-learning</td>
                        <td><strong>2<sup>nd</sup> session.</strong>
                            <p>Online sessions on “<em>Entrepreneurship and self-employment (SE)</em>”</p></td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Working individually online on first thematic area: “<em>Entrepreneurship and self-employment (SE)”</em></li>
                                <li>Learners choose OER’s from the first thematic area</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>First thematic area – deepening knowledge in the classroom</td>
                        <td><strong>3<sup>rd</sup> session. </strong>
                            <p>Face-to-face meeting on “<em>Entrepreneurship and self-employment (SE)”</em></p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Working in the group to deepen knowledge on first thematic area through practical exercises</li>
                                <li>Motivation to become self-employed</li>
                                <li>Giving tasks for the fourth on-line self e-learning session on second thematic area</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Second thematic area – self-learning</td>
                        <td><strong>4<sup>th</sup> session</strong>.
                            <p>Online sessions on “<em>Life-Style Entrepreneurship”</em></p>
                        </td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Working individually online on second thematic area: “<em>Life-Style Entrepreneurship</em>”</li>
                                <li>Learners choose OER’s from the second thematic area</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Mid-term monitoring &amp; Second thematic area</td>
                        <td><strong>5<sup>th</sup> session.</strong>
                            <p>Face-to-face meeting on “<em>Life-Style Entrepreneurship</em>”</p>
                            <p>Mid-term monitoring of the mentoring</p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Working in the group to deepen knowledge on second thematic area through practical exercises</li>
                                <li>Motivation to become self-employed</li>
                                <li>Giving tasks for the 6<sup>th</sup> session on-line self e-learning session on third thematic area</li>
                                <li>Mid-term monitoring of mentoring sessions</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Third thematic area – self-learning</td>
                        <td><strong>6<sup>th</sup> session. </strong>
                            <p>Online sessions on “<em>Business principles and marketing in Life-Style Entrepreneurship“</em></p>
                        </td>
                        <td>8</td>
                        <td>
                            <ul>
                                <li>Working individually online on third thematic area: “<em>Business principles and Marketing in Life-Style Entrepreneurship” </em></li>
                                <li>Learners choose OER’s from the third thematic area</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Third thematic area – deepening knowledge in the classroom</td>
                        <td><strong>7<sup>th</sup> session. </strong>
                            <p>Face-to-face meeting on Online sessions on “<em>Business principles and marketing in Life-Style Entrepreneurship”</em></p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Working in the group to deepen knowledge on third thematic area through practical exercises</li>
                                <li>Giving tasks for the 8<sup>th</sup> session on-line self e-learning session based success stories on LSE</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Self-learning on success stories</td>
                        <td><strong>8<sup>th</sup> session. </strong>
                            <p>Online sessions on <em>“Life-Style Entrepreneurship – a w</em>ay to success”</p></td>
                        <td>6</td>
                        <td>
                            <ul>
                                <li>Working individually online on area: “Success stories on n self-employment based on Life-Style Entrepreneurship”</li>
                                <li>Filling in the Feedback sheet in order to bring it to 9<sup>th</sup> session</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Final mentoring meeting</td>
                        <td><strong>9<sup>th</sup> session.</strong>
                            <p>Final face-to-face meeting</p>
                            <p>Finalizing mentoring process</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Feedback on the success stories</li>
                                <li>Finalization of course and personal development plan.</li>
                                <li>Assessment of learners’ competences – using online tool</li>
                                <li>Feedback collection and awards</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><strong>Total</strong></td>
                        <td>54 hours</td>
                        <td>2 credits</td>
                    </tr>
                    </tbody>
                </table>
                <p><strong>Learning outcomes</strong></p>
                <p>By the end of Module II the participant will (youth-worker) will:</p>
                <ul>
                    <li>understand the concept of lifestyle self-employment and be able to provide it to young learners with fewer opportunities, including NEETs;</li>
                    <li>be able explain the main peculiarities of Life-Style Entrepreneurship as a useful alternative of self- employment to youth in nowadays labour market;</li>
                    <li>be able give motivation of youth to become lifestyle self-employed;</li>
                    <li>be able to present examples of different types of Life-Style Entrepreneurship;</li>
                    <li>know how to use OERs on lifestyle self-employment within the social mentoring process for disadvantaged youth.</li>
                </ul>
                <p><em>Validation of learning progress is very important criteria of learner‘s motivation. The Module III presents the methods of assessing the knowledge obtained during youth education, for example, on-line tests, learner’s guides, etc</em>.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-11" role="tabpanel" aria-labelledby="v-pills-11">
                <h3>10. MODULE III. Validation of the Competences “Sense of initiative and entrepreneurship”</h3>
                <p><strong>The aim of Module III is:</strong></p>
                <p>To develop youth workers’ competences to assess and validate young learners’ skills and competence “Sense of initiative and entrepreneurship”.</p>
                <p><strong>The objectives of Module III are:</strong></p>
                <ul>
                    <li>to review the development of validation processes in EU countries;</li>
                    <li>to analyse the methods, benefits of validation of youth informal and non-formal learning;</li>
                    <li>to present the most commonly used non-formal methods of assessment of knowledge obtained during the youth education;</li>
                    <li>to introduce the on-line test ‘Sense of initiative and entrepreneurship’ for evaluation of skills and competencies of Life-Style Entrepreneurship of young learners with fewer opportunities;</li>
                    <li>to introduce the learners guide ‘Why validation is important for me’ as tool for learner’s motivation to learn and validate;</li>
                    <li>to introduce the set of skills and knowledge which are included in the test for youth learners ‘Sense of initiative and entrepreneurship’;</li>
                    <li>to present the knowledge portfolio conception and structure for assessment of learners’ skills and competences.</li>
                </ul>
                <p><strong>Description of the Module III:</strong></p>
                <p>Module contains theoretical material about the strategies of assessment, their purpose, goals, the principles, methods and tools in non-formal youth education. The Module briefly reviews the development of validation processes in EU countries as well as methods and benefits of validation of youth informal and non-formal learning. Furthermore, the Module presents the most often used assessment methods in non-formal youth education such as discussion, test, conversation with teacher (interview), case analysis, observation, assessment by other group members and reflection. Each method is briefly described as such: name of the method, size of learners’ group, learning environment (situation), purpose and object of assessment (skills, knowledge, competencies). It is also stressed within this Module that the validation process is based on the assessment of the skills and competences, thus, the tools and methods are similar. However, assessment itself is usually oriented towards checking the level of understanding of the training course and its content. Validation is the following step and aims to recognize the skills and competences, which could be obtained within few training courses, or through informal learning and life experiences.</p>
                <p>Module is dedicated to show the importance of validation of competences obtained by youth learners with fewer opportunities within SELF-E training course. It is stressed that youth workers have to convince their LSE’s learners to assess and validate competences as it will an opportunity to improve their CVs by adding important soft skills, thus will increase their access into the labour market. The usage of methodology of experiential learning using online Self-Assessment Tool is presented and will help youth workers to motivate youth learners with fewer opportunities to validate their competences.</p>
                <p>The Self-Assessment tool helps youth learners with fewer opportunities to assess skills and knowledge for competence “Sense of initiative and entrepreneurship”. The Module presents the list of the entrepreneurship skills, which are important for learners and could be added to their CVs. The list of the skills is selected by project partners. The learners will be asked to assess their soft skill before the social mentoring (re-assessment) and after the training process (post-assessment). The training via social mentoring process (see Module II) develops these skills by using the OERs. Thus, after the social mentoring it is expected that the level of soft skills on the competence “Sense of initiative and entrepreneurship” will be increased and it will be visible through the post-assessment process. Thus, Self-Assessment is a great motivation tool for learners as it shows their progress in learning.</p>
                <p>The learners guide “Why validation is important for me?” is also presented within this Module as it is an important tool for young learner’s motivation to learn and further validate obtained skills related with the competence “Initiative and sense of entrepreneurship”. It is important to stress that the concept of Knowledge portfolio, it structure and importance in further validation of learners’ skills and competences is also introduced in the Learner’s Guide.</p>
                <p>As a practical part of the Module III, youth workers will fulfil the Self-Assessment test by themselves, in order to learn how to provide it to youth with few opportunities during the SEL-E training course. Youth workers will learn how to measure the impact of the SELF-E trainings course on young people by assessing their entrepreneurial skills before and after the SELF-E training using the SELF-E online assessment test, which consist at least of 30 questions. During the face-to-face session, youth workers will deepen their knowledge of the assessment and validation of young learners’ entrepreneurial competence.</p>
                <p><strong>Learning outcomes</strong></p>
                <p>By the end of Module III the youth worker will:</p>
                <ul>
                    <li>Be aware about EU regulations on Validation of Non-Formal &amp; Informal Learning in context of entrepreneurship.</li>
                    <li>Know different tools and methods for assessment and validation of the competences ‘Sense of initiative and entrepreneurship’.</li>
                    <li>Be acquainted with the set of skills and knowledge which are included in the test for youth learners ‘Sense of initiative and entrepreneurship’.</li>
                    <li>Be able to use the developed within the project online Assessment tool to assess the learners’ skills, to measure the impact of the learning process.</li>
                    <li>Be able to motivate youth learners to validate their competence obtained within SELF-E training course.</li>
                    <li>Be able to introduce for youth learners the learners guide ‘Why validation is important for me’.</li>
                    <li>Understand the importance of the concept of the knowledge portfolio.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

