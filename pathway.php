<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 22.01.2019
 * Time: 09:26
 */
?>
<div class="row" style="margin-bottom: 2rem;">
    <div class="col-md-3">
        <a href="/young-people.html" class="btn btn-success">Back to PART FOR YOUNG PEOPLE menu</a>
    </div>
</div>
<h6 style="background-color: #007bff; padding: .5rem; color: #fff" class="text-center">SELF-E: INNOVATIVE TRAINING
    COURSE FOR YOUNG LEARNERS BASED ON SOCIAL MENTORING</h6>
<h2 class="text-center young-title">Pathway to lifestyle self-employment</h2>
<p class="text-center"><strong>Set of practical exercises - OERs</strong></p>
<div class="row justify-content-center">
    <div class="col-md-10 col-sm-12">
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-8">
                <a href="/pathway-category.html?category=1">
                    <img src="/img/young-people/pathway_self_emplyment.png" class="img-fluid" alt="self-employment"/>
                </a>
            </div>
            <div class="col-md-6 col-sm-8">
                <a href="/pathway-category.html?category=2">
                    <img src="/img/young-people/pathway_life_style.png" class="img-fluid" alt="life-style"/>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-8">
                <a href="/pathway-category.html?category=3">
                    <img src="/img/young-people/pathway_business.png" class="img-fluid" alt="business principles"/>
                </a>
            </div>
        </div>
    </div>
</div>
