<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 21.01.2019
 * Time: 12:10
 */

$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
if (!in_array($lang, ['en', 'lt', 'gr', 'bg', 'pl'])) {
    $lang = 'en';
}
?>
<div style="background: url('img/youth-workers/youth_worker_2level-background.png'); background-size: cover; padding: 2em;">
            <h2 class="text-center yw-title">INNOVATIVE TRAINING ON SELF-EMPLOYMENT FOR YOUNG PEOPLE BASED ON MENTORING<br/>E-TOOLKIT FOR YOUTH
                WORKERS</h2>
            <div class="row justify-content-center">
                <div class="outline col-sm-10">
                    <p>This E-toolkit for youth workers consists of two parts:</p>
                    <ul>
                        <li>the first part is dedicated for youth worker as a trainer. The main aim of this part is to help youth worker to learn how work with the developed SELF-E  Training course.</li>
                        <li>the second part is dedicated for youth worker as a teacher with the main aim to introduce him the developed within the project training materials for work with the youth learners.</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-top: 8em; text-transform: uppercase;">
                <div class="col-sm-4 text-center" style="font-weight: bold; font-size: 1.75rem">
                    <span style="color: #fff">Youth-worker as</span><br/>
                    <a class="btn btn-primary" href="/workers-as-learner.html?lang=<?=$lang?>" style="font-weight: bold; font-size: 1.75rem">learner</a>
                </div>
                <div class="col-sm-4 text-center" style="font-weight: bold; font-size: 1.75rem">
                    <span style="color: #fff">Youth-worker as</span><br/>
                    <a class="btn btn-success" href="/workers-as-teacher.html" style="font-weight: bold; font-size: 1.75rem">teacher</a>
                </div>
            </div>
</div>

