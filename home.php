<div class="row justify-content-center align-items-end home-p">
        <div class="col-xl-12 col-md-12 col-lg-12 project_title">
            <div id="project-info">
                <p style="margin-bottom: 0px;">New pathways of youth to labour market through
                    lifestyle<br/>self-employment</p>
                <p>
                    <small>Project No. 2017-3-LT02-KA205-005536</small>
                </p>
            </div>
        </div>

        <div class="col-sm-10 col-md-5 offset-md-1 col-lg-5 col-xl-5 mainColA laptop-screen">
            <div>
                <img src="/img/boxes/for_youth_workers.png" class="img-fluid" />
                <a href="/youth-workers.html?lang=en" class="for_youth_uk for_youth1 action_btn"><span class="youth1" style="display: none">EN</span></a>
                <a href="/youth-workers.html?lang=lt" class=" for_youth_lt for_youth2 action_btn"><span class="youth2" style="display: none">LT</span></a>
                <a href="/youth-workers.html?lang=gr" class=" for_youth_cy for_youth3 action_btn"><span class="youth3" style="display: none">GR</span></a>
                <a href="/youth-workers.html?lang=bg" class=" for_youth_bg for_youth4 action_btn"><span class="youth4" style="display: none">BG</span></a>
                <a href="/youth-workers.html?lang=pl" class=" for_youth_pl for_youth5 action_btn"><span class="youth5" style="display: none">PL</span></a>
            </div>
        </div>
        <div class="col-sm-8 col-md-4 col-lg-4 col-xl-4 mainColB">
            <div class="young_col">
                <img src="/img/boxes/for_young_people.png" class="img-fluid" />
                <a href="/young-people.html" class="for_young_uk action_btn_yellow for_young1"><span class="young1" style="display: none">EN</span></a>
                <a href="#" class="for_young_lt action_btn_yellow for_young2"><span class="young2" style="display: none">LT</span></a>
                <a href="#" class="for_young_cy action_btn_yellow for_young3"><span class="young3" style="display: none">GR</span></a>
                <a href="#" class="for_young_bg action_btn_yellow for_young4"><span class="young4" style="display: none">BG</span></a>
                <a href="#" class="for_young_pl action_btn_yellow for_young5"><span class="young5" style="display: none">PL</span></a>
            </div>
            <img src="/img/boxes/cups.png" class="img-fluid" />

        </div>

        <div class="col-md-12 table" style="background-image: url('img/boxes/table.png');"></div>
    </div>

    <script>
        jQuery(function(){
            for (var i = 1; i < 6; i++) {
                (function(i) {
                    $('.for_youth'+i).mouseover(
                        function() {
                            $('a span.youth'+i).show();
                        }).mouseout(
                        function() {
                            $('a span.youth'+i).hide();
                        });
                    $('.for_young'+i).mouseover(
                        function() {
                            $('a span.young'+i).show();
                        }).mouseout(
                        function() {
                            $('a span.young'+i).hide();
                        });

                })(i);

            }

        });
    </script>
