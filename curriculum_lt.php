<?php
/**
 * Created by PhpStorm.
 * User: lklapa
 * Date: 19.05.2019
 * Time: 17:26
 */
?>

<style>
    .curriculum-menu {
        border: 1px solid #8dca2a;
        -webkit-border-radius: .25rem;
        -moz-border-radius: .25rem;
        border-radius: .25rem;
        padding: 20px;
    }
    table.partners-table td {
        padding: 10px;
    }
    table.partners-table td:first-of-type {
        text-align: right;
        font-weight: bold;
        vertical-align: middle;
    }
    table.partners-table img {
        max-width: 200px;
        max-height: 80px;
    }
    .curriculum-image p {
        font-size: 0.8em;
        color: #333333;
        font-style: italic;
    }
    table.curriculum-plan tr:nth-child(even) {
        background: rgb(238,255,229);
    }
    table.curriculum-plan tr:nth-child(odd) {
        background: rgb(224,241,252);
    }
    table.curriculum-plan tr:first-child {
        background: #fefefe;
    }
    table.curriculum-plan tr > td:nth-of-type(3) {
        text-align: center;
    }
    ol.curriculum-plan-list li:nth-child(even) {
        color: rgb(79,98,40);
    }
    ol.curriculum-plan-list li:nth-child(odd) {
        color: rgb(36,64,97);
    }
    #v-pills-tabContent h3 {
        color: #7ad0fa;
        border-bottom: 1px solid #8dca2a;
        margin-bottom: 24px;
        padding-bottom: 4px;
    }
</style>
    <div class="row" style="margin-bottom: 2rem;">
        <div class="col-md-12">
            <a href="/workers-as-learner.html?lang=lt" class="btn btn-success">Back to YOUTH-WORKER AS LEARNER menu</a>
        </div>
    </div>

<div class="row">
    <div class="col-3 small curriculum-menu">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-tab-0" data-toggle="pill" href="#v-pills-0" role="tab" aria-controls="v-pills-0" aria-selected="true">Home</a>
            <a class="nav-link" id="v-pills-tab-1" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="false">Sutrumpinimai</a>
            <a class="nav-link" id="v-pills-tab-2" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">1. Projekto pristatymas</a>
            <a class="nav-link" id="v-pills-tab-3" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">2. Mokymo programos tikslas, uždaviniai ir turinys</a>
            <a class="nav-link" id="v-pills-tab-4" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">3. Mokymosi pasiekimai</a>
            <a class="nav-link" id="v-pills-tab-5" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false">4. Mokymo ir mokymosi strategijos</a>
            <a class="nav-link" id="v-pills-tab-6" data-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-6" aria-selected="false">5. Vertinimo strategija</a>
            <a class="nav-link" id="v-pills-tab-7" data-toggle="pill" href="#v-pills-7" role="tab" aria-controls="v-pills-7" aria-selected="false">6. Mokymo ir mokymosi aplinka bei įranga</a>
            <a class="nav-link" id="v-pills-tab-8" data-toggle="pill" href="#v-pills-8" role="tab" aria-controls="v-pills-8" aria-selected="false">7. PJaunimo darbuotojo mokymo programa</a>
            <a class="nav-link" id="v-pills-tab-9" data-toggle="pill" href="#v-pills-9" role="tab" aria-controls="v-pills-9" aria-selected="false">8. MODULIS I</a>
            <a class="nav-link" id="v-pills-tab-10" data-toggle="pill" href="#v-pills-10" role="tab" aria-controls="v-pills-10" aria-selected="false">9. MODULIS II</a>
            <a class="nav-link" id="v-pills-tab-11" data-toggle="pill" href="#v-pills-11" role="tab" aria-controls="v-pills-11" aria-selected="false">10. MODULIS III</a>
        </div>
        <div class="text-center">
            <hr />
            <h4>Download the curriculum</h4>
            <a href="/files/Self-E%20Curriculum%20Final_LT.pdf" target="_blank">PDF</a> |
            <a href="/files/Self-E%20Curriculum%20Final_LT.docx" target="_blank">Word</a>
        </div>
    </div>
    <div class="col-9">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-0" role="tabpanel" aria-labelledby="v-pills-0">
                <h1>Mokymo programa jaunimo darbuotojų kursams SELF-E</h1>

                <em>Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui</em>

                <table class="partners-table mt-5">
                <tr><td>SOCIAL INNOVATION FUND (LT)</td><td><img src="img/partners/logo-sif.png" /></td></tr>
                <tr><td>CARDET (CY)</td><td><img src="img/partners/logo-c.png" /></td></tr>
                <tr><td>CWEP (PL)</td><td><img src="img/partners/logo-cwep.png" /></td></tr>
                <tr><td>KNOW AND CAN ASSOCIATION (BG)</td><td><img src="img/partners/logo-kc.png" /></td></tr>
                <tr><td>KNJUC (LT)</td><td><img src="img/partners/logo-knjuc.png" /></td></tr>
                <tr><td>VINI (LT)</td><td><img src="img/partners/logo-vini.png" /></td></tr>
                </table>

            </div>
            <div class="tab-pane fade" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1">
                <h3>Sutrumpinimai</h3>
                <p>AŠI – atviri švietimo ištekliai. AŠI apibrėžimą galite rasti 6 puslapyje.<br />
                    ES – Europos Sąjunga.<br />
                    IKT – Informacinės ir komunikacinės technologijos.<br />
                    NEET – nedirbantis, nesimokantis ir mokymuose nedalyvaujantis asmuo.<br />
                    NVO – nevyriausybinė organizacija.<br />
                    SĮ – savarankiškas įsidarbinimas.<br />
                    SĮV – savarankiškas įsidarbinimas, įkuriant verslą pagal gyvenimo būdą.<br />
                </p>
            </div>
            <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2">
                <h3>1. Projekto pristatymas </h3>
                <p><strong>PROJEKTO TIKSLAS</strong></p>
                <p>Projektu siekiama Europiniu mastu prisidėti prie darbo su jaunimu kokybės gerinimo, skatinant mažiau galimybių turinčių jaunuolių (įskaitant nedirbančių ir nesimokančių jaunuolių – NEET) socialinę įtrauktį ir gerinant jų galimybes įsidarbinti.</p>
                <p>SELF-E projektas prisideda prie 2020 m. švietimo ir mokymo programos („ET 2020“) įgyvendinimo, kuri patvirtina, jog individualūs įgūdžiai žinių ekonomikos laikmečiu yra labai svarbūs.</p>
                <p>Pagrindinis poveikis mažiau galimybių turintiems jaunuoliams bus jų dalyvavimo darbo rinkoje paskatinimas. Jie taip pat įgis aktualių aukštos kokybės įgūdžių ir kompetencijų, būtinų sėkmingam savarankiškam įsidarbinimui. Mažiau galimybių turinčius jaunuolius mentoriai motyvuos savarankiškai įsidarbinti, įkuriant verslus pagal gyvenimo būdą. Numatomas poveikis – sumažinti tikslinės grupės nedarbo lygį ir pagerinti jos integraciją į darbo rinką ir visuomenę, taip prisidėti prie strategijos „Europa 2020“ tikslų: pasiekti 75 procentų užimtumo lygį bei 15 procentų suaugusiųjų dalyvavimą mokymosi visą gyvenimą procese.</p>
                <p><strong>UŽDAVINIAI</strong></p>
                <ol>
                    <li>Stiprinti jaunimo darbuotojų gebėjimus organizuoti novatorišką neformalųjį, mentoryste grindžiamą, mokymą apie jaunimo galimybes dirbti savarankiškai.</li>
                    <li>Padėti jaunimo darbuotojams taikyti naujus motyvavimo metodus, skirtus mažiau galimybių turinčių jaunuolių (įskaitant nedirbančių ir nesimokančių jaunuolių) mokymosi ir savarankiško darbo (ypatingą dėmesį skiriant verslui pagal gyvenimo būdą) skatinimui.</li>
                    <li>Palengvinti jaunimo perėjimo į suaugusiuosius etapą, parodant jiems savarankiško darbo perspektyvas ir tokiu būdu išplečiant jų integracijos į darbo rinką galimybes.</li>
                    <li>Ugdyti jaunimo bendruosius gebėjimus „Iniciatyva ir verslumas“.</li>
                    <li>Sukurti galimybes įvertinti ir pripažinti įgytus bendruosius gebėjimus „Iniciatyva ir verslumas“.</li>
                </ol>
                <p><strong>PAGRINDINIAI PRODUKTAI</strong></p>
                <p>Trys pagrindiniai intelektiniai produktai:</p>
                <ol>
                    <li>Priemonių rinkinys „Kaip organizuoti novatoriškąjį neformalųjį mokymąsi apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui“;</li>
                    <li>Praktinių pratimų (atvirų švietimo išteklių) rinkinys „Kelias į savarankišką įsidarbinimą“;</li>
                    <li>Mažiau galimybių turinčių jaunuolių (įskaitant nedirbančių ir nesimokančių jaunuolių) bendrųjų gebėjimų „Iniciatyva ir verslumas“ vertinimo priemonė.</li>
                </ol>
                <p><strong>TIKSLINĖ GRUPĖ</strong></p>
                <ol>
                    <li>Su mažiau galimybių turinčiais jaunuoliais dirbantys jaunimo darbuotojai:
                        <ul>
                            <li>Jaunimo centruose;</li>
                            <li>NVO;</li>
                            <li>Organizacijose, dirbančiose su neįgaliais jaunuoliais;</li>
                            <li>Organizacijose, dirbančiose su migrantais;</li>
                            <li>Darbo biržų jaunimo skyriuose;</li>
                        </ul>
                    </li>
                    <li>Mažiau galimybių turintys jaunuoliai (įskaitant nedirbančius ir nesimokančius jaunuolius).</li>
                </ol>
                <div class="alert alert-info">
                    Daugiau informacijos apie projekto SELF-E partnerystę, rezultatus ir produktus galite rasti interneto tinklalapyje:
                    <a href="http://self-e.lpf.lt/" target="_blank">http://self-e.lpf.lt/</a>
                </div>
            </div>
            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3">
                <h3>2. Mokymo programos „Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui – SELF-E“ tikslas, uždaviniai ir turinys</h3>
                <p><strong>Pagrindiniai</strong> mokymo programos <strong>tikslai</strong>:</p>
                <ul>
                    <li>Apibrėžti mokymo kurso, skirto jaunimo darbuotojų gebėjimų rengti inovatyvius neformalius mokymus apie savarankišką įsidarbinimą mažiau galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius), struktūrą;</li>
                    <li>Pristatyti inovatyvius mokymo metodus, paremtus socialine mentoryste, skirtus motyvuoti mažiau galimybių turinčius jaunuolius (įskaitant nedirbančius ir nesimokančius jaunuolius) mokytis ir savarankiškai įsidarbinti.</li>
                </ul>
                <p>Mokymo programos <strong>uždaviniai</strong>:</p>
                <ul>
                    <li>aprašyti mokymo modulių turinį;</li>
                    <li>pateikti SELF-E mokymo kurso uždavinius ir pagrindinius mokymosi pasiekimus;</li>
                    <li>apibūdinti socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui, kaip naują mokymosi metodą;</li>
                    <li>apibrėžti savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sąvoką;</li>
                    <li>pristatyti pagrindinius verslumo pagal gyvenimo būdą, kaip naudingos jaunimo savarankiško įsidarbinimo alternatyvos šiuolaikinėje darbo rinkoje, ypatumus;</li>
                    <li>pasiūlyti jaunimo motyvavimo savarankiškai įsidarbinti būdus;</li>
                    <li>aprašyti jaunimo darbuotojų mokymų pagal inovatyvų “SELF-E” mokymo kursą, pagrįstą socialine mentoryste ir AŠI programą;</li>
                    <li>pristatyti besimokantiems jaunuoliams jų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimo ir pripažinimo galimybes ir svarbą;</li>
                    <li>pristatyti SELF-E mokymo kurso, paremto socialine mentoryste ir AŠI, skirto mažiau galimybių turintiems jaunuoliams, užsiėmimų planą.</li>
                </ul>
                <p>Mokymo programa apibrėžia mokymo kurso, skirto jaunimo darbuotojams „Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui – SELF-E” turinį ir yra sudaryta iš trijų modulių:</p>
                <ol>
                    <li>Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui, skirtas mažiau galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius).</li>
                    <li>AŠI apie savarankišką įsidarbinimą naudojimas socialinėje mentorystėje.</li>
                    <li>Mažiau galimybių turinčių jaunuolių (įskaitant nedirbančių ir nesimokančių jaunuolių) bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas.</li>
                </ol>
                <p>SELF-E mokymo kursas yra pagrįstas socialinės mentorystės metodo taikymu ir pratimų, sukurtų kaip AŠI, naudojimu.</p>
                <p>SELF-E mokymo kurso trukmė – 27 valandos. Kursą sudaro tiesioginis ir e. mokymasis. Mokymo ir mokymosi strategijos yra aprašytos 4 mokymo programos dalyje.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4">
                <h3>3. Mokymosi pasiekimai</h3>
                <p><img src="img/curriculum/image007.jpg" class="float-left"/>Sėkmingai pabaigę mokymo kursą “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui SELF-E ”, jaunimo darbuotojai patobulins savo gebėjimus būti mentoriais savarankiško įsidarbinimo per socialinę mentorystę procese ir efektyviai organizuoti mokymus mažiau galimybių turintiems jaunuoliams.</p>
                <p>Pabaigę mokymo kursą, jaunimo darbuotojai:</p>
                <ul>
                    <li>Supras savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sąvoką.</li>
                    <li>Žinos pagrindinius savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, kaip naudingos jaunimo savarankiško įsidarbinimo alternatyvos šiuolaikinėje darbo rinkoje,</li>
                    <li>Gebės analizuoti skirtingas SĮV rūšis.</li>
                    <li>Supras SĮV verslo principus ir kurti verslo planus.</li>
                    <li>Žinos kaip kurti, įgyvendinti, stebėti ir tikrinti SĮV marketingo strategijas.</li>
                    <li>Žinos bendruosius gebėjimus, būtinus turėti norint įsteigti verslą pagal gyvenimo būdą.</li>
                    <li>Mokės naudoti skirtingus metodus ir priemones, padedančius sustiprinti mažiau galimybių turinčių jaunuolių (įskaitant nedirbančių ir nesimokančių jaunuolių) motyvaciją įsidarbinti savarankiškai, įkuriant verslą pagal gyvenimo būdą, taip išbandant šias naujas verslo galimybes.</li>
                    <li>Naudoti socialinės žiniasklaidos priemones savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, populiarinimui.</li>
                </ul>
                <p>Mokymo kursas SELF-E suteikia teorinę medžiagą ir praktines priemones jaunimo darbuotojui dirbti su tiksline grupe: mažiau galimybių turinčiais jaunuoliais (įskaitant nedirbančius ir nesimokančius jaunuolius).</p>
            </div>
            <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5">
                <h3>4. Mokymo ir mokymosi strategijos</h3>
                <p>Šio mokymų kurso pedagoginė strategija paremta mišraus mokymosi strategija: tradicinio ir e.  mokymosi sukurtoje e-mokymosi platformoje su AŠĮ deriniu. Tokia platforma suteikia galimybę savarankiškai mokytis patogiu laiku ir tinkamoje vietoje.</p>
                <div class="alert alert-info">
                    <strong>Mišrus mokymasis</strong> yra (formalaus ar neformalaus) švietimo programa, kuri derina nuotolinius e. mokymo metodus su tradiciniais mokymo klasėje metodais. Tokiam mokymuisi yra būtinas dėstytojo ir studento dalyvavimas, taikant studentui mokymosi laiko, vietos, krypties ar tempo kontrolę.
                </div>
                <div class="row">
                    <div class="col-md-5"><img class="img-fluid" src="img/curriculum/image009.png"/></div>
                    <div class="col-md-7"><img class="img-fluid" src="img/curriculum/image008_lt.png"/></div>
                </div>
                <div class="alert alert-info">
                    <strong>Atvirieji švietimo ištekliai</strong> (AŠI) yra skaitmeninė medžiaga, kuri gali būti perdirbama ir panaudojama mokymui, mokymuisi, tyrimams ir kitiems tikslams. Šie ištekliai yra viešai pasiekiami pagal intelektinės nuosavybės licenciją, kuri leidžia jų laisvą naudojimą, ko nebūtų galima lengvai padaryti su autorių teisių saugomais  ištekliais. AŠI apima išsamius kursus, kursų medžiagą, modulius, vadovėlius, vaizdo medžiagą (video), testus, programinę įrangą ir bet kokias kitas priemones, medžiagas ar metodus, naudojamus siekiant padidinti galimybes gauti žinių.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image010.jpg" class="img-fluid"/>
                    <p>https://www.google.es/search?q=open+education+resources&amp;source=lnms&amp;tbm=isch&amp;sa=X&amp;ved=0ahUKEwi6vYTl_fPcAhVFtosKHWNGDNsQ_AUICigB&amp;biw=1920&amp;bih=974#imgrc=Cl32e5SzCoJjFM:</p>
                </div>
                <p>Mokymo kursas yra inovatyvus tuo, kad yra sukurtas pagal IKT orientuotą <strong>apverstos klasės metodologiją</strong>, panaudojant AŠI.</p>
                <div class="alert alert-info">
                    <strong>Apverstos klasės metodologija</strong> paremta tokiu principu – švietėjas prisiima fasilitatoriaus vaidmenį ir nurodo besimokantiesiems (jaunimo darbuotojams) savarankiškai atlikti pirminę e. mokomosios medžiagos, pateiktos AŠI pavidalu, analizę. Kai besimokantieji pasiekia savo individualius mokymosi tikslus, tada jie aptaria rezultatus su fasilitatoriumi tiesioginių susitikimų metu. Tai reiškia, kad besimokantieji (jaunimo darbuotojai) naudoja <strong>mišraus mokymosi metodą</strong> jų įgūdžių ir gebėjimų tobulinimui.
                </div>
                <div class="text-center curriculum-image">
                    <img src="img/curriculum/image011.png" class="img-fluid"/>
                    <p>Source of the picture: https://sites.google.com/a/mahidol.edu/how-gen-z-learn-by-il-mu/techforactivelearning/7&#8212;tools-for-flipped-classroom</p>
                </div>
                <p>Panaudojant AŠI pavidalu <strong>sukurtą e-Priemonių rinkinį jaunimo darbuotojams</strong> yra užtikrinamas dalyvių žinių gilinimas kiekviename modulyje. E-mokymasis yra labai svarbi mokymosi kurso, trunkančio 27 akademines valandas, dalis. Metodologinė medžiaga jaunimo darbuotojams, būsimiems moderatoriams ar mentoriams, dirbantiems su mažai galimybių turinčiais jaunuoliais, yra sudaryta iš teorijos ir praktinių užduočių. Siekiant užtikrinti aktyvų jaunimo darbuotojų dalyvavimą ir išsamų modulių turinio suvokimą, keturi tiesioginio mokymosi užsiėmimai (iš viso 8 akademinės valandos) ir trys e. mokymosi užsiėmimai (iš viso 19 akademinių valandų) yra įtraukti į mokymo ir mokymosi strategiją.</p>
                <ul>
                    <li>Pirmasis tiesioginis užsiėmimas yra pravedamas siekiant įvertinti jaunimo darbuotojų kompetenciją tapti socialiniais mentoriais ir pravesti mokymo kursą mažai galimybių turintiems jaunuoliams. Pirmojo užsiėmimo metu yra pristatomas Modulis I “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui, skirtas mažai galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius)”.</li>
                    <li>Antro tiesioginio užsiėmimo tikslas yra pagilinti dalyvių (jaunimo darbuotojų) gebėjimus per praktinius pratimus, kai dalyviai virtualiai jau yra susipažinę su Modulio I medžiaga, ir pristatyti Modulį II “Socialinė mentorystė, naudojant atvirus švietimo išteklius (AŠI) apie savarankišką įsidarbinimą”. 36 AŠI apžvalga yra pristatoma dalyviams ir jiems yra rekomenduojama atlikti nors vieną AŠI, taip pat užpildyti „Įvertinimo formą“, sukurtą kiekvienam AŠI.</li>
                    <li>Trečias tiesioginis užsiėmimas yra sudarytas iš dviejų dalių:</li>
                    <ul>
                        <li>AŠI panaudojimo svarbos socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procese analizė.</li>
                        <li>Įžanga į Modulį III “Įgytų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas”.</li>
                    </ul>
                    <li>Ketvirtas tiesioginis užsiėmimas yra paskutinis ir yra sudarytas iš kelių Moduliui III priklausančių pratimų. Šis užsiėmimas yra svarbus, kadangi jo metu yra surenkami viso mokymo kurso įvertinimai ir organizuojamos diskusijos apie planus, kaip turėtų būti organizuojamas socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procesas mažai galimybių turintiems jaunuoliams. Dalyviai taip pat įvertina savo gebėjimus ir mokymo kurso poveikį jiems, panaudodami įsivertinimo priemonę. Šio užsiėmimo pabaigoje rekomenduojama dalyviams išduoti mokymo kurso “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui -SELF-E ” baigimo pažymėjimus.</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6">
                <h3>5. Vertinimo strategija</h3>
                <p>Dalyvių žinių ir gebėjimų vertinimas atliekamas trimis būdais (sėkmingai užbaigus kursą dalyviams bus išduodami mokymo kurso baigimo pažymėjimai):</p>
                <ul>
                    <li>Atliekant virtualų įsivertinimo testą (prieš pradedant mokymo kursą ir jį pabaigus);</li>
                    <li>Dalyvaujant grupiniame darbe ir atliekant praktines užduotis tiesioginių užsiėmimų metu;</li>
                    <li>Pasidalinant savo mokymosi patirtimi pabaigus mokymo kursą.</li>
                </ul>
                <p>SELF-E mokymo kurso pradžioje dalyvių bus paprašyta atlikti virtualų įsivertinimo testą, siekiant įvertinti jų žinias ir gebėjimus, susijusius su mokymo medžiaga. Mokymo kurso pabaigoje dalyviai turės pakartoti virtualų įsivertinimo testą, siekiant įvertinti jų mokymosi pasiekimus.</p>
                <p><img src="img/curriculum/image012.png" class="float-left" />Atliekant įvertinimą mokymų pradžioje ir pabaigoje, yra vertinami 3 aspektai:</p>
                <ul>
                    <li>Žinios apie mentorystės procesą;</li>
                    <li>Žinios apie verslumą pagal gyvenimo būdą;</li>
                    <li>Žinios apie gebėjimų „Iniciatyva ir verslumas“ įvertinimą ir pripažinimą.</li>
                </ul>
                <p>Iš viso testą sudaro 25 uždari klausimai: 10 klausimų skirtų įvertinti žinias apie mentorystę, 10 klausimų skirtų įvertinti žinias apie verslumą pagal gyvenimo būdą ir 5 klausimai skirti įvertinti žinias apie gebėjimų „Iniciatyva ir verslumas“ įvertinimą ir pripažinimą. Ši vertinimo priemonė bus suprogramuota ir patalpinta internete su nemokama prieiga.</p>
                <p>Kaip buvo minėta aukščiau, vertinimas yra tęsiamas, kai dalyviai tiesioginių užsiėmimų metu atlieka praktines užduotis grupėse, vertina vieni kitus ir diskutuoja.</p>
                <p>Galiausiai, paskutinio užsiėmimo pabaigoje, dalyvių bus paprašyta pasidalinti savo mokymosi patirtimi, sukaupta mokymo kurso metu dalyvaujant tiesioginiuose užsiėmimuose ir mokantis e-mokymosi platformoje. Dalyviai pasidalins patirtimi, kaip jiems sekėsi praktiškai pritaikyti žinias, įgytas mokymo kurso metu. Ši veikla paskatins tolesnį mokymąsi ir praktinį žinių bei gebėjimų panaudojimą kasdienėse mokymo/mokymosi veiklose.</p>
                <p>Dalyviams, kurie teisingai atsakys į bent 20 galutinio įsivertinimo testo klausimų, ir kurie bus aktyviai dalyvavę praktinėse veiklose bei refleksijose, bus išduodami mokymo kurso baigimo pažymėjimai.</p>
            </div>
            <div class="tab-pane fade" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-7">
                <h3>6. Mokymo ir mokymosi aplinka ir įranga</h3>
                <p>Įstaigos, kurios organizuos SELF-E mokymo kursus jaunimo darbuotojams-besimokantiesiems pagal paruoštą mokymo planą, turi užtikrinti tinkamą mokymosi aplinką, techninę įrangą ir priemones, būtinas mišraus mokymo ir mokymosi procesui, t.y. turi turėti:</p>
                <ul>
                    <li>Mokymo patalpą su multimedija projektoriumi ir kompiuteriu, per kurį būtų galima rodyti pristatymų skaidres tiesioginių susitikimų metu;</li>
                    <li>Asmeninių kompiuterių su interneto prieiga, būtina pasiekti atviruosius švietimo išteklius ir SELF-E projekto mokymosi platformą (<a href="http://self-e.lpf.lt/">http://self-e.lpf.lt/</a>);</li>
                    <li>Kitas organizacines priemones tiesioginiams susitikimams (lentą, dalomąją medžiagą, popieriaus ir t.t.).</li>
                </ul>
                <p>SELF-E mokymo kurso dėstytojai turi būti tinkamai paruošti mokymo procesui.</p>
                <p>Jie turi:</p>
                <img src="img/curriculum/image013.jpg" class="float-right" />
                <ul>
                    <li>gebėti sukurti psichologiškai palankią mokymosi aplinką;</li>
                    <li>būti susipažinę su auditorijos biografijos faktais bei suprasti jų poreikius ir lūkesčius;</li>
                    <li>turėti darbo su mažai galimybių turinčiais jaunuoliais (įskaitant nedirbančius ir nesimokančius jaunuolius) patirtį;</li>
                    <li>turėti žinių ir įgūdžių, kaip organizuoti e-mokymosi užsiėmimus;</li>
                    <li>gerai žinoti mokymo turinį (Modulius I-III);</li>
                    <li>turėti būtinas asmenines savybes: gerus bendravimo įgūdžius, būti tolerantiški, gebėti motyvuoti besimokančiuosius ir demonstruoti teigiamus savęs vertinimo įgūdžius;</li>
                    <li>turėti gerus fasilitatoriaus įgūdžius, tokius kaip gebėjimą inicijuoti diskusijas, surinkti refleksijas, reaguoti į atsiliepimus, pateikti apibendrintus atsakymus ir t.t.</li>
                </ul>
            </div>
            <div class="tab-pane fade" id="v-pills-8" role="tabpanel" aria-labelledby="v-pills-8">
                <h3>7. Jaunimo darbuotojo mokymo planas</h3>
                <p>Mokymo planas yra sukurtas siekiant padėti organizuoti mokymus jaunimo darbuotojams apie tai, kaip efektyviai pravesti “SELF-E” mokymo kursą mažiau galimybių turintiems jaunuoliams. Šis planas pateikia išsamią mišraus mokymosi proceso, naudojančio atvirkštinio mokymo metodologiją, aprašytą 4 dalyje, apžvalgą.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>Nr.</strong></td>
                        <td><strong>Metodas</strong></td>
                        <td><strong>Trukmė (ak. val.)</strong></td>
                        <td><strong>Turinys/Temos</strong></td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Tiesioginis mokymas</td>
                        <td>2</td>
                        <td>
                            <p>SELF-E mokymo kurso jaunimo darbuotojams pristatymas. Pradinis jaunimo darbuotojų gebėjimų tapti socialiniais mentoriais ir vesti mokymo kursą mažai galimybių turintiems jaunuoliams įvertinimas.</p>
                            <p>Modulio I “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui” pristatymas.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>E. mokymas</td>
                        <td>6</td>
                        <td>
                            <p>Savarankiškas Modulio I “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui” studijavimas.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Tiesioginis mokymas</td>
                        <td>2</td>
                        <td>
                            <p>Grupinis darbas – Modulio I pratimų atlikimas, diskusijos.</p>
                            <p>Modulio II “Socialinė mentorystė, naudojant atvirus švietimo išteklius (AŠI) apie savarankišką įsidarbinimą” pristatymas.</p>
                            <p>Nors vieno AŠI atlikimas ir įvertinimo formos atitinkamam AŠI užpildymas.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>E. mokymas</td>
                        <td>9</td>
                        <td>
                            <p>Savarankiškas Modulio II “Socialinė mentorystė, naudojant atvirus švietimo išteklius (AŠI) apie savarankišką įsidarbinimą ” studijavimas.</p>
                            <p>Visų rinkinį sudarančių AŠI atlikimas ir įvertinimo formų visiems AŠI užpildymas. Užpildytos formos bus aptariamos 5 užsiėmimo metu.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Tiesioginis mokymas</td>
                        <td>2</td>
                        <td>
                            <p>Savarankiško Modulio II studijavimo rezultatų aptarimas pagal besimokančiųjų užpildytas įvetinimo formas. AŠI svarbos socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procesui analizė.</p>
                            <p>Modulio III “Įgytų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas” pristatymas.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>E. mokymas</td>
                        <td>4</td>
                        <td>
                            <p>Savarankiškas Modulio III “Įgytų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas” studijavimas. Vertinimo testo, kuris taip pat bus duodamas pildyti mažiau galimybių turintiems jaunuoliams SELF-E mokymų metu, užpildymas.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Tiesioginis mokymas</td>
                        <td>2</td>
                        <td><p>Baigiamasis užsiėmimas:</p>
                            <ul>
                                <li>Modulio III grupinio darbo pratimai.</li>
                                <li>Atsiliepimų apie visą mokymo kursą surinkimas, diskusijos.</li>
                                <li>Besimokančiųjų jaunimo darbuotojų galutinis vertinimas.</li>
                                <li>Mokymo kurso baigimo pažymėjimų išdavimas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>TOTAL</strong></td>
                        <td class="text-center"><strong>27</strong></td>
                        <td>&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
                <p class="text-right small"><em>*Pastaba: 1 akademinė valanda = 45 minutės</em></p>
                <p>Kaip nurodyta lentelėje aukščiau, yra numatyti septyni pagrindiniai etapai.</p>
                <img src="img/curriculum/image014.jpg" class="float-right" />
                <ol class="curriculum-plan-list">
                    <li>Pirmojo tiesioginio užsiėmimo metu yra trumpai pristatomas SELF-E mokymo kursas. Dalyvių yra paprašoma atlikti pradinį įsivertinimą, siekiant patikrinti jaunimo darbuotojų gebėjimus tapti socialiniais mentoriais ir pravesti mokymo kursą mažai galimybių turintiems jaunuoliams. Atlikus įsivertinimą dalyviams yra pristatomas Modulis I: “Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui”.</li>
                    <li>Po šio tiesioginio užsiėmimo seka e. mokymasis, kurio metu savarankiškai studijuojamas Modulis I.</li>
                    <li>Antro tiesioginio užsiėmimo metu atliekami grupiniai Modulio I pratimai ir vedamos dalyvių diskusijos, skirtos sužinoti, ką jie išmoko, pasidalinti savarankiško mokymosi patirtimi ir iškelti klausimus tolesniam mokymuisi. Vėliau dalyviams yra pristatomas Modulis II “<em>Socialinė mentorystė, naudojant atvirus švietimo išteklius (AŠI) apie savarankišką įsidarbinimą</em>”. Yra atliekamas bent vienas AŠI ir dalyviai pasidalina savo nuomonėmis bei patirtimi užpildydami įvertinimo formas.</li>
                    <li>Po šio tiesioginio užsiėmimo seka e. mokymasis, kurio metu savarankiškai studijuojamas Modulis II. Besimokantieji ne tik studijuoja teorinę Modulio II medžiagą, bet ir atlieka visus rinkinį sudarančius AŠI. Besimokantieji taip pat užpildo įvertinimo formas visiems AŠI, kurias atsineša į trečią tiesioginį užsiėmimą. Šiose formose dalyviai mokymų metu turi rašytis pastabas ir atsakinėti į klausimus. Tokiu būdu besimokantieji sukaupia atsiliepimus apie AŠI panaudojimą mentorystėje, kuriuos aptaria diskusijose trečiojo tiesioginio užsiėmimo metu, taip diskusijos tampa labiau struktūruotos.</li>
                    <li>Trečiojo tiesioginio užsiėmimo metu dalyviai pasidalina savo savarankiško Modulio II mokymosi patirtimi, remdamiesi savo užpildytomis įvertinimo formomis. Jie taip pat aptaria ir išanalizuoja AŠI svarbą socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procese. Dalyviams taip pat yra pristatomas Modulis III “Įgytų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas”.</li>
                    <li>Po šio tiesioginio užsiėmimo seka e. mokymasis, kurio metu savarankiškai studijuojamas Modulis III. Dalyviai ne tik studijuoja teorinę medžiagą, bet ir išbando virtualų testą, skirtą įvertinti mažiau galimybių turinčių jaunuolių bendruosius gebėjimus „Iniciatyva ir verslumas“. Taip jaunimo darbuotojai pasirengia vėliau pateikti testą mažiau galimybių turintiems jaunuoliams SELF-E mokymo kurso metu.</li>
                    <li>Baigiamojo užsiėmimo metu dalyviai atlieka grupinius Modulio III pratimus, aptaria ir pasidalina savo savarankiško mokymosi pasiekimais bei pateikia savo nuomonę apie vertinimo priemonę. Siekiant įvertinti mokymo kurso progresą, dalyviai užpildo galutinio vertinimo klausimyną, skirtą jaunimo darbuotojams, ir jiems yra išduodami mokymo kurso baigimo pažymėjimai.</li>
                </ol>
            </div>
            <div class="tab-pane fade" id="v-pills-9" role="tabpanel" aria-labelledby="v-pills-9">
                <h3>8. MODULIS I. Novatoriškas neformalusis mokymasis apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui</h3>
                <p><strong>Modulio tikslas yra:</strong></p>
                <p>Pristatyti jaunimo darbuotojams novatorišką neformalųjį mokymąsi apie socialinę mentorystę nuosavo verslo pagal gyvenimo būdą kūrimui, naudojamą paskatinti ir paremti ugdytinių asmeninį ir profesionalų tobulėjimą link savo verslo įkūrimo.</p>
                <p><strong>Modulio I uždaviniai yra:</strong></p>
                <ul>
                    <li>apibrėžti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui sąvoką: Kodėl socialinė mentorystė nuosavo verslo pagal gyvenimo būdą kūrimui yra inovatyvus ir vertingas neformalaus mokymosi metodas, skirtas mažiau galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius);</li>
                    <li>apibrėžti pagrindinius ugdytinio ir mentoriaus vaidmenis ir užduotis socialinėje mentorystėje nuosavo verslo pagal gyvenimo būdą kūrimui;</li>
                    <li>detaliai išaiškinti pagrindinius ugdytinio ir mentoriaus vaidmenis ir užduotis socialinėje mentorystėje nuosavo verslo pagal gyvenimo būdą kūrimui;</li>
                    <li>paaiškinti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui naudą ir iššūkius;</li>
                    <li>apibrėžti pagrindinius socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui proceso žingsnius;</li>
                    <li>nusakyti būdus, kaip užtikrinti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui efektyvumą;</li>
                    <li>pristatyti metodus, kaip atrinkti mentorius ir kaip juos suporuoti su jaunuoliais-ugdytiniais;</li>
                    <li>paaiškinti kaip užtikrinti norimus socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui kokybės standartus;</li>
                    <li>pristatyti, kaip organizuoti pradinį, vidurio periodo ir baigiamąjį socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui užsiėmimus;</li>
                    <li>apibrėžti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui kokybės monitoringo tikslus;</li>
                    <li>nustatyti etikos ir profesinį kodeksus jaunimo darbuotojams-mentoriams ir jaunuoliams-ugdytiniams;</li>
                    <li>pristatyti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui proceso valdymo priemones.</li>
                </ul>
                <p><strong>Modulio I aprašymas:</strong></p>
                <p>Studijuodamas šį modulį, besimokantysis susipažins su mentorystės sąvoka – tai yra tobulėjimo partnerystė, kai vienas asmuo pasidalina žiniomis, įgūdžiais, informacija ir savo požiūriu, siekdamas paskatinti kito asmens asmeninį ir profesinį tobulėjimą. Yra išanalizuojami skirtingi mentorystės tipai: individualus ir grupinis. Mentorystė taip pat gali būti įvairių formų: pvz., tiesioginio kontakto, nuotolinė ir mišri. Besimokantieji gali susipažinti su minėtais mentorystės tipais ir formomis gilindamiesi į teorinę medžiagą ir atlikdami pratimus.</p>
                <p>Besimokantieji, studijuodami šį modulį, sužinos kaip mentoriai padeda ugdytiniams, juos skatina ir jiems atveria savo asmeninius tinklus. Ypatingas dėmesys bus skiriamas mentorystei, susijusiai su darbo klausimais, kai mentoriai padeda ugdytiniams įsidarbinti arba įkurti savo verslą.</p>
                <p>Šiame modulyje mentorystės procesas yra analizuojamas dvejomis perspektyvomis – mentoriaus ir ugdytinio, kadangi sklandžiam mentorystės procesui yra būtinas abiejų pusių įsitraukimas, pagarba vienas kitam, atsakingumas ir lygiaverčiai lūkesčiai. Taip pat sėkmingai mentorystei užtikrinti yra būtinas stiprus ryšys tarp ugdytinio ir mentoriaus. Mentorystė ugdytiniui yra procesas, siūlantis puikias galimybes asmeniniam ir profesiniam tobulėjimui. Su mentoriaus parama ugdytinis taip pat gali tobulinti savo įgūdžius ir gebėjimus, svarbius įsidarbinimui. Mentorystės  procesas  mentoriui  taip pat yra naudingas; nauda yra pristatoma šiame modulyje ir aptariama su besimokančiaisiais.</p>
                <p>Besimokantieji sužinos jog diskusijos, vykstančios mentorystės proceso metu turi būti susijusios su pagrindiniu ugdytinių tikslu. Taip pat jiems bus pabrėžta, kad ugdytinis turi orientuotis į savo verslo planą ir su juo susijusius aspektus.</p>
                <p>Šiame modulyje taip pat yra aprašyta mentorystės nauda, pabrėžta jog mentorystė dažnai pagerina supratimą tarp skirtingų kartų, kultūrų ir skirtingų žmonių. Mentorystė yra puikus mokymosi ir tobulėjimo metodas ir ugdytiniui šis metodas suteikia drąsos spręsti iškylančius konfliktus bei prisiimti būtiną riziką.</p>
                <p><strong>Mokymosi pasiekimai</strong></p>
                <p>Pabaigę šį modulį, dalyviai (jaunimo darbuotojai) gebės:</p>
                <ul>
                    <li>suprasti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui, ugdytinio, mentoriaus ir moderatoriaus sąvokas;</li>
                    <li>atskirti moderatoriaus, mentoriaus ir ugdytinio vaidmenis socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procese;</li>
                    <li>identifikuoti ir paaiškinti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui naudą, stipriąsias puses ir iššūkius;</li>
                    <li>suvokti pagalbinių priemonių, naudojamų socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procese, svarbą;</li>
                    <li>organizuoti ir efektyviai valdyti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui procesą (ugdytinių ir mentorių suporavimą, pradinio, vidurio periodo ir baigiamojo susitikimų organizavimą);</li>
                    <li>nustatyti kokybės standartus bei etikos ir profesinius kodeksus mentoriams ir ugdytiniams.</li>
                </ul>
                <p>Socialinė mentorystė yra pagrįsta mišraus mokymosi principu: tradicinio ir e. mokymosi sukurtoje e-mokymosi platformoje su AŠI rinkiniu deriniu. Modulyje II yra pristatoma, kaip reikia naudoti AŠI socialinės mentorystės procese.</p>
                <p><strong>PAGRINDINIAI MOKYMO, NAUDOJANT MENTORYSTĘ, ETAPAI:</strong></p>
                <img src="img/curriculum/image015_lt.png" class="img-fluid" />
            </div>
            <div class="tab-pane fade" id="v-pills-10" role="tabpanel" aria-labelledby="v-pills-10">
                <h3>9. Modulis II “Socialinė mentorystė, naudojant atvirus švietimo išteklius (AŠI) apie savarankišką įsidarbinimą”</h3>
                <p><strong>Modulio tikslas:</strong></p>
                <p>Pristatyti savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sąvoką, ypatingą dėmesį skiriant verslumui pagal gyvenimo būdą.</p>
                <p><strong>Modulio II uždaviniai:</strong></p>
                <ul>
                    <li>apibrėžti savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sąvoką;</li>
                    <li>pristatyti pagrindinius verslumo pagal gyvenimo būdą, kaip naudingos jaunimo savarankiško įsidarbinimo alternatyvos šiuolaikinėje darbo rinkoje, ypatumus;</li>
                    <li>pristatyti jaunimo motyvavimo kurti savo verslą metodus;</li>
                    <li>apžvelgti skirtingus verslumo pagal gyvenimo būdą tipus, panaudojant sėkmės istorijas;</li>
                    <li>apibrėžti pagrindinius SĮV verslo principus ir pagrindines verslo plano dalis;</li>
                    <li>išanalizuoti SĮV marketingo strategijas;</li>
                    <li>pristatyti praktinių pratimų rinkinį, t.y. AŠI „Kelias į savarankišką įsidarbinimą, įkuriant verslą pagal gyvenimo būdą“ rinkinį, kuris turi būti naudojamas socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui proceso fasilitavimui;</li>
                    <li>išanalizuoti inovatyvaus „SELF-E mokymų kurso“, paremto socialinės mentorystės metodo ir AŠI naudojimu, skirto mažiau galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius), pamokų planą.</li>
                </ul>
                <p><strong>Modulio II aprašymas:</strong></p>
                <p>Šiame modulyje besimokantysis palygins pagrindinius verslumo principus su verslumo pagal gyvenimo būdą sąvoka. Čia taip pat yra pateikiamas bendrasis verslininko apibrėžimas ir jo paaiškinamas pasitelkiant pavyzdžius. Išanalizuojami verslumo ir verslumo pagal gyvenimo būdą panašumai ir skirtumai. Pateikiamos galimybių tapti verslininku, įkuriant verslą pagal gyvenimo būdą, vertinimo priemonės, kadangi ne kiekvienas gali įkurti verslą pagal gyvenimo būdą – tam yra būtini specifiniai asmenybės bruožai ir troškimas savo pomėgį paversti verslu, siekiant finansinės ar socialinės naudos, ar aktyvesnio bendravimo su kitais žmonėmis.</p>
                <p>Taip pat Modulyje II yra pristatomi esminiai verslo principai, būdingi visiems verslo tipams, įskaitant verslumą pagal gyvenimo būdą. Svarbu suprasti pagrindinius verslo įkūrimo ir valdymo principus, t.y. verslo misijos nustatymą, nuodugnios rinkos analizės atlikimą, marketingo strategijos sukūrimą, finansinį planavimą ir prognozavimą, žmonių valdymą ir t.t. Verslo planas nėra vien tik lėšų rinkimo priemonė. Faktiškai, tai yra priemonė, skirta suprasti, kaip ir kodėl yra įkurtas verslas. Verslo planas gali būti panaudotas eigos stebėjimui; jis skatina reguliarias pasiūlymų vertės klientams, rinkodaros prielaidų, veiksmų ir finansinio planų peržiūras.</p>
                <p>Verslumas pagal gyvenimo būdą gali būti vertinga alternatyva savarankiškam įsidarbinimui pagal gyvenimo būdą. Taigi, Modulis II pateikia daugybę skirtingų iššūkių ir kliūčių, su kuriais susiduria verslumas pagal gyvenimo būdą ir juos įveikia, taip pat ir priežasčių, kodėl žmonės nusprendžia įkurti savo verslus, pavyzdžių.</p>
                <p>Modulyje II bus naudojami skirtingi mokymosi metodai, siekiant išlaikyti besimokančiojo susidomėjimą veiklomis per interaktyvius užsiėmimus, tokius kaip diskusijos, viktorinos ir mokomieji metodai, panaudojant daug e. mokymosi priemonių. Bus naudojamas dviejų rūšių mokymasis: tiesioginis ir e. mokymasis. Šis modulis atitiks jaunimo darbuotojų poreikius – gauti pagrindines žinias apie verslumą pagal gyvenimo būdą ir savarankišką įsidarbinimą, įkuriant verslą pagal gyvenimo būdą, taip pat apie AŠI: Kaip panaudoti AŠI mentorystės procese?</p>
                <p>24 sukurti AŠI apims tris temines sritis:</p>
                <ul>
                    <li>verslumas ir savarankiškas įsidarbinimas (SĮ);</li>
                    <li>verslumas pagal gyvenimo būdą;</li>
                    <li>verslo principai ir marketingas verslume pagal gyvenimo būdą.</li>
                </ul>
                <p>12 sėkmės istorijų bus naudojamos mažiau galimybių turinčių jaunuolių (įskaitant nedirbančius ir nesimokančius jaunuolius) motyvavimui įkurti verslą pagal gyvenimo būdą.</p>
                <p>Jaunimo darbuotojai susipažins su AŠI, sukurtais SELF-E projekto metu ir išmoks juos panaudoti socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui proceso metu.</p>
                <p>Inovatyvaus SELF-E mokymų kurso, skirto mažiau galimybių turintiems jaunuoliams <strong>pamokų plano pavyzdys</strong>. Šis kursas paremtas socialine mentoryste ir yra sudarytas iš tiesioginės mentorystės užsiėmimų ir e. mokymosi, naudojant praktinių pratimų rinkinį – AŠI.</p>
                <table class="table table-sm table-bordered curriculum-plan small">
                    <tbody>
                    <tr>
                        <td><strong>SELF-E mokymo etapas</strong></td>
                        <td><strong>Užsiėmimai</strong></td>
                        <td><strong>Trukmė, val.</strong></td>
                        <td><strong>Turinys; AŠI</strong></td>
                    </tr>
                    <tr>
                        <td>Pasiruošimas</td>
                        <td>&nbsp;</td>
                        <td>Bent dvi savaitės prieš 1 užsiėmimą</td>
                        <td>
                            <ul>
                                <li>Mentorių ir ugdytinių atranka;</li>
                                <li>Mentorių ir ugdytinių suporavimas;</li>
                                <li>Tinkamumo patikrinimas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Pradinis mentorystės užsiėmimas</td>
                        <td><strong>1 užsiėmimas.</strong>
                            <p>Pradinis tiesioginis mentorystės užsiėmimas</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Susipažinimas tarpusavyje;</li>
                                <li>Įvadas į socialinę mentorystę;</li>
                                <li>Trumpas SELF-E mokymo kurso pristatymas;</li>
                                <li>Besimokančiųjų gebėjimų įvertinimas – naudojant virtualią priemonę;</li>
                                <li>Individualių ugdytinių planų sukūrimas;</li>
                                <li>Įvadas į besimokančiųjų vadovą;</li>
                                <li>Užduočių e. mokymosi individualiam užsiėmimui nustatymas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Pirmoji teminė sritis – savarankiškas mokymasis</td>
                        <td><strong>2 užsiėmimas.</strong>
                            <p>E. mokymosi užsiėmimai “Verslumo ir savarankiško įsidarbinimo (SĮ)” tema</p></td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Individualus virtualus darbas pirmoje teminėje srityje: “Verslumas ir savarankiškas įsidarbinimas (SĮ)”;</li>
                                <li>Besimokantieji pasirenka AŠI iš pirmos teminės srities.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Pirmoji teminė sritis – žinių gilinimas klasėje</td>
                        <td><strong>3 užsiėmimas.</strong>
                            <p>Tiesioginis užsiėmimas “Verslumo ir savarankiško įsidarbinimo (SĮ)” tema</p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Darbas grupėje gilinant žinias pirmoje teminėje srityje per praktinius pratimus;</li>
                                <li>Motyvavimas savarankiškai įsidarbinti;</li>
                                <li>Užduočių ketvirtam savarankiško e-mokymosi užsiėmimui antroje teminėje srityje nustatymas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Antroji teminė sritis – savarankiškas mokymasis</td>
                        <td><strong>4 užsiėmimas.</strong>.
                            <p>E. mokymosi užsiėmimai “Verslumo pagal gyvenimo būdą” tema</p>
                        </td>
                        <td>10</td>
                        <td>
                            <ul>
                                <li>Individualus virtualus darbas antroje teminėje srityje: “Verslumas pagal gyvenimo būdą ”;</li>
                                <li>Besimokantieji pasirenka AŠI iš antros teminės srities.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Vidurio periodo monitoringas ir antroji teminė sritis</td>
                        <td><strong>5 užsiėmimas.</strong>
                            <p>Tiesioginis užsiėmimas “Verslumo pagal gyvenimo būdą” tema</p>
                            <p>Vidurio periodo mentorystės monitoringas</p></td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Darbas grupėje gilinant žinias antroje teminėje srityje per praktinius pratimus;</li>
                                <li>Motyvavimas savarankiškai įsidarbinti;</li>
                                <li>Užduočių šeštam savarankiško e-mokymosi užsiėmimui trečioje teminėje srityje nustatymas;</li>
                                <li>Vidurio periodo mentorystės užsiėmimų monitoringas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Trečioji tematinė sritis – savarankiškas mokymasis</td>
                        <td><strong>6 užsiėmimas.</strong>
                            <p>E. mokymosi užsiėmimai “Verslo principai ir marketingas verslume pagal gyvenimo būdą“ tema</p>
                        </td>
                        <td>8</td>
                        <td>
                            <ul>
                                <li>Individualus virtualus darbas trečioje teminėje srityje: “Verslo principai ir marketingas verslume pagal gyvenimo būdą”;</li>
                                <li>Besimokantieji pasirenka AŠI iš trečios teminės srities.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Trečioji tematinė sritis – žinių gilinimas klasėje</td>
                        <td><strong>7 užsiėmimas.</strong>
                            <p>Tiesioginis užsiėmimas “Verslo principai ir marketingas verslume pagal gyvenimo būdą” tema</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Darbas grupėje gilinant žinias trečioje teminėje srityje per praktinius pratimus;</li>
                                <li>Užduočių, susijusių su SĮV sėkmės istorijomis, aštuntam savarankiško e-mokymosi užsiėmimui trečioje teminėje srityje, nustatymas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Savarankiškas sėkmės istorijų studijavimas</td>
                        <td><strong>8 užsiėmimas.</strong>
                            <p>E. mokymosi užsiėmimai “Verslumas pagal gyvenimo būdą – kelias į sėkmę” tema</p></td>
                        <td>6</td>
                        <td>
                            <ul>
                                <li>Individualus virtualus darbas “Savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sėkmės istorijos” srityje;</li>
                                <li>Atsiliepimų formų užpildymas, kurios turės būti atsinešamos į devintą užsiėmimą.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>Baigiamasis mentorystės užsiėmimas</td>
                        <td><strong>9 užsiėmimas.</strong>
                            <p>Baigiamasis tiesioginis užsiėmimas</p>
                            <p>Mentorystės proceso užbaigimas</p>
                        </td>
                        <td>4</td>
                        <td>
                            <ul>
                                <li>Atsiliepimai apie sėkmės istorijas;</li>
                                <li>Kurso ir asmeninio tobulėjimo plano užbaigimas;</li>
                                <li>Besimokančiųjų gebėjimų įvertinimas – naudojant virtualią priemonę;</li>
                                <li>Atsiliepimų surinkimas ir baigimo pažymėjimų įteikimas.</li>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><strong>Viso:</strong></td>
                        <td>54 val.</td>
                        <td>2 kreditai</td>
                    </tr>
                    </tbody>
                </table>
                <p><strong>Mokymosi pasiekimai</strong></p>
                <p>Pabaigę šį modulį, dalyviai (jaunimo darbuotojai) gebės:</p>
                <ul>
                    <li>suprasti savarankiško įsidarbinimo, įkuriant verslą pagal gyvenimo būdą, sąvoką ir paaiškinti ją mažiau galimybių turintiems jaunuoliams (įskaitant nedirbančius ir nesimokančius jaunuolius);</li>
                    <li>paaiškinti pagrindinius verslumo pagal gyvenimo būdą, kaip naudingos jaunimo savarankiško įsidarbinimo alternatyvos šiuolaikinėje darbo rinkoje, ypatumus;</li>
                    <li>motyvuoti jaunuolius savarankiškai įsidarbinti, įkuriant verslą pagal gyvenimo būdą;</li>
                    <li>pristatyti skirtingus verslumo pagal gyvenimo būdą tipus;</li>
                    <li>panaudoti AŠI socialinės mentorystės nuosavo verslo pagal gyvenimo būdą kūrimui, procese.</li>
                </ul>
                <p><em>Mokymosi proceso įvertinimas ir pripažinimas yra labai svarbus kriterijus besimokančiojo motyvacijai. Modulyje III pristatomi jaunuolių žinių, įgytų mokymosi metu, vertinimo metodai, pavyzdžiui, e. testai, besimokančiųjų vadovai ir t.t.</em></p>
            </div>
            <div class="tab-pane fade" id="v-pills-11" role="tabpanel" aria-labelledby="v-pills-11">
                <h3>10. MODULIS III. Įgytų bendrųjų gebėjimų „Iniciatyva ir verslumas“ įvertinimas ir pripažinimas”</h3>
                <p><strong>Modulio III tikslas yra:</strong></p>
                <p>Sustiprinti jaunimo darbuotojų gebėjimus įvertinti ir pripažinti besimokančių jaunuolių įgūdžius ir gebėjimus „Iniciatyva ir verslumas“.</p>
                <p><strong>Modulio III uždaviniai yra:</strong></p>
                <ul>
                    <li>apžvelgti įvertinimo ir pripažinimo procesų tobulėjimą ES šalyse;</li>
                    <li>išanalizuoti jaunimo neinstitucinio ir neformalaus mokymosi įvertinimo ir pripažinimo metodus ir naudą;</li>
                    <li>pristatyti dažniausiai naudojamus neformalius žinių, įgytų jaunimo mokymosi metu, vertinimo metodus;</li>
                    <li>pristatyti e. testą „Iniciatyva ir verslumas“, skirtą mažiau galimybių turinčių jaunuolių verslumo pagal gyvenimo būdą įgūdžių ir gebėjimų vertinimui;</li>
                    <li>pristatyti besimokančiųjų vadovą „Kodėl įvertinimas ir pripažinimas yra svarbus man, kaip priemonė besimokančiųjų motyvavimui mokytis, įvertinti ir pripažinti“;</li>
                    <li>pristatyti įgūdžių ir žinių rinkinį, kuris yra įtrauktas į testą „Iniciatyva ir verslumas“, skirtą mažiau galimybių turintiems jaunuoliams;</li>
                    <li>pristatyti žinių portfelio, skirto įvertinti besimokančiųjų įgūdžius ir gebėjimus, sąvoką ir struktūrą.</li>
                </ul>
                <p><strong>Modulio III aprašymas:</strong></p>
                <p>Modulį sudaro teorinė medžiaga apie vertinimo strategijas, naudojamas neformaliame jaunimo švietime, jų paskirtį, tikslus, principus, metodus ir priemones. Modulyje glaustai apžvelgiama įvertinimo ir pripažinimo procesų ES šalyse raida, taip pat jaunimo neinstitucinio ir neformalaus mokymosi įvertinimo bei pripažinimo metodai ir nauda. Taip pat modulyje yra pristatomi dažniausiai neformaliame jaunimo švietime naudojami vertinimo metodai, tokie kaip diskusijos, testai, pokalbis su dėstytoju (interviu), atvejo analizė, stebėjimas, kitų grupės narių atliekamas vertinimas ir refleksijos. Kiekvienas metodas yra glaustai aprašytas, t.y. pateikiama: metodo pavadinimas, besimokančiųjų grupės dydis, mokymosi aplinka (situacija), vertinimo paskirtis ir objektas (įgūdžiai, žinios, gebėjimai). Šiame modulyje taip pat yra pabrėžiama, jog įvertinimo ir pripažinimo procesas yra paremtas įgūdžių ir gebėjimų vertinimu, taigi, priemonės ir metodai yra panašūs. Tačiau pats vertinimas dažniausiai yra orientuotas į mokymo kurso ir jo turinio supratimo lygio įvertinimą. Įvertinimas ir pripažinimas yra tolesnis žingsnis ir siekia pripažinti įgūdžius ir gebėjimus, kurie gali būti įgyti kelių mokymų kursų metu arba per neformalų mokymąsi ir gyvenimo patirtį.</p>
                <p>Modulis yra skirtas parodyti besimokančių, mažiau galimybių turinčių jaunuolių, gebėjimų, įgytų SELF-E mokymų kurso metu, įvertinimo ir pripažinimo svarbą. Yra pabrėžiama, kad jaunimo darbuotojai turi įtikinti SĮV besimokančiuosius įvertinti ir pripažinti savo gebėjimus, kadangi taip jie galės atnaujinti savo CV, įtraukdami svarbius bendruosius gebėjimus, taip jie padidins savo galimybes integruotis į darbo rinką. Modulyje jaunimo darbuotojams yra pristatomas eksperimentinio mokymosi metodologijos, naudojant virtualią įsivertinimo priemonę, panaudojimas. Ši metodologija padės jaunimo darbuotojams motyvuoti mažiau galimybių turinčius besimokančiuosius įvertinti ir pripažinti savo gebėjimus.</p>
                <p>Įsivertinimo priemonė padės besimokantiems mažiau galimybių turintiems jaunuoliams įvertinti bendruosius įgūdžius ir gebėjimus, priklausančius kompetencijai „Iniciatyva ir verslumas“. Modulyje yra pristatomas besimokantiesiems svarbių verslumo įgūdžių, kurie gali būti įtraukti į jų CV, sąrašas. Įgūdžių sąrašą sudaro projekto partneriai. Besimokančiųjų bus paprašyta įvertinti savo minkštuosius įgūdžius prieš pradedant dalyvauti socialinėje mentorystėje (pradinis vertinimas) ir pabaigus mokymo procesą (galutinis vertinimas). Mokymasis socialinės mentorystės proceso metu (žiūr. Modulį I) tobulina šiuos įgūdžius panaudojant AŠI. Tikimasi, jog besimokantiesiems baigus dalyvauti socialinėje mentorystėje, bus patobulėję jų bendrieji įgūdžiai, priklausantys kompetencijai „Iniciatyva ir verslumas“, kas bus matoma atlikus galutinį vertinimą. Taigi, įsivertinimas yra puiki besimokančiųjų motyvavimo priemonė, nes jis parodo mokymosi progresą.</p>
                <p>Šiame modulyje taip pat yra pristatomas besimokančiųjų vadovas “Kodėl vertinimas ir pripažinimas yra man svarbus?”. Ši priemonė yra svarbi, kadangi ji yra skirta motyvuoti jaunus besimokančiuosius mokytis ir vėliau įvertinti bei pripažinti įgytus įgūdžius, susijusius su gebėjimais “Iniciatyva ir verslumas”. Svarbu pabrėžti, kad besimokančiųjų vadove taip pat yra pristatoma žinių portfelio sąvoka, jo struktūra ir svarba tolesniam besimokančiųjų įgūdžių ir gebėjimų įvertinimui ir pripažinimui.</p>
                <p>Kaip praktinę Modulio III dalį, jaunimo darbuotojai atliks įsivertinimo testą, taip išmokdami jį pateikti mažiau galimybių turintiems jaunuoliams SELF-E mokymo kurso metu. Jaunimo darbuotojai išmoks išmatuoti SELF-E mokymo kurso poveikį jaunuoliams, įvertindami jų verslumo įgūdžius prieš ir po SELF-E mokymų, panaudojant SELF-E virtualią vertinimo priemonę, sudarytą iš mažiausiai 30 klausimų. Tiesioginio užsiėmimo metu jaunimo darbuotojai pagilins savo žinias apie besimokančių jaunuolių verslumo kompetencijos įvertinimą ir pripažinimą.</p>
                <p><strong>Mokymosi pasiekimai</strong></p>
                <p>Pabaigę šį modulį, dalyviai (jaunimo darbuotojai):</p>
                <ul>
                    <li>Bus susipažinę su ES įstatymais apie neinstitucinio ir neformalaus mokymosi verslumo kontekste įvertinimą ir pripažinimą.</li>
                    <li>Žinos skirtingas priemones ir metodus, skirtus įvertinti ir pripažinti gebėjimus „Iniciatyva ir verslumas“.</li>
                    <li>Bus susipažinę su įgūdžių ir žinių rinkiniu, įtrauktu į testą „Iniciatyva ir verslumas“, skirtą mažiau galimybių turintiems jaunuoliams.</li>
                    <li>Gebės panaudoti projekto metu sukurtą virtualią vertinimo priemonę, skirtą įvertinti besimokančiųjų gebėjimus ir išmatuoti mokymosi proceso poveikį.</li>
                    <li>Gebės motyvuoti besimokančiuosius įvertinti ir pripažinti savo gebėjimus, įgytus SELF-E mokymo kurso metu.</li>
                    <li>Gebės besimokantiems jaunuoliams pristatyti besimokančiųjų vadovą „Kodėl įvertinimas ir pripažinimas yra man svarbus?“.</li>
                    <li>Gebės suprasti žinių portfelio sampratos svarbą.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

