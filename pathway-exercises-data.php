<?php

$exercises['en'] = [
    1 => [
        'theme' => 'Lifestyle entrepreneurship (LSE)',
        'exercise' => 'What traits are typical of lifestyle entrepreneur?',
        'developedBy' => 'Social Innovation Fund, LT',
        'aim' => 'To understand what lifestyle Entrepreneurship is.',
        'learningOutcomes' => [
            'Understand your possibilities to become a lifestyle entrepreneur.',
            'Ground the main differences between entrepreneurship and LSE.',
            'Understand the benefits of LSE.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'This exercise will help you learn about some typical entrepreneurial traits and give you an idea if you should consider seeking an entrepreneurial opportunity. There are some typical lifestyle entrepreneurial principles that many entrepreneurs share. In addition, there are some entrepreneurial traits that are undesirable and, if not managed, could be destructive.',
        'task' => 'For each question, please select possible answer that you think describes the typical entrepreneur',
        'questions' => [
            'q1' => [
                'q' => 'Can everyone be a lifestyle entrepreneur?',
                'a' => [
                    'a1' => [
                        'Yes',
                        false,
                    ],
                    'a2' => [
                        'No',
                        true,
                    ]
                ],
                'f' => 'A lifestyle entrepreneur requires specific personal traits, along with the desire to make a business out of their passion whether this is for financial or social gain.',
            ],
            'q2' => [
                'q' => 'Please find incorrect statement',
                'intro' => 'Both entrepreneur and LSE are oriented towards:',
                'a' => [
                    'a1' => [
                        'managing a business',
                        false,
                    ],
                    'a2' => [
                        'seeking profit',
                        true,
                    ],
                    'a3' => [
                        'completing market research',
                        false,
                    ],
                    'a4' => [
                        'developing a marketing strategy',
                        false,
                    ],
                    'a5' => [
                        'financial planning',
                        false,
                    ],
                    'a6' => [
                        'people management',
                        false,
                    ]
                ],
                'fy' => 'Lifestyle entrepreneurs desire adventure. Motivated by quality of life rather than growth of profit. Running and managing a business, including setting a business mission, completing thorough market research, developing a marketing strategy, financial planning and forecasting, people management are essential principles to all types of business.',
                'fn' => 'Running and managing a business, including setting a business mission, completing thorough market research, developing a marketing strategy, financial planning and forecasting, people management are essential principles to all types of business. Entrepreneurs just want profit. They typically want to grow profit as fast as possible.',
            ],
            'q3' => [
                'q' => 'Can different styles of LSE be a useful alternative to self-employment for disadvantages learners?',
                'a' => [
                    'a1' => [
                        'Yes',
                        true,
                        'There are different styles of LSE. All of them can be a useful alternative to self-employment for disadvantaged people. There are many role model businesses that could identify their journey and experiences of lifestyle entrepreneurship across many different business sectors.'
                    ],
                    'a2' => [
                        'No',
                        false,
                        'There are different styles of LSE. All of them can be a useful alternative to self-employment for disadvantaged people. There are many role model businesses that could identify their journey and experiences of lifestyle entrepreneurship across many different occupational areas.'
                    ],
                ],
            ],
            'q4' => [
                'q' => 'Does Lifestyle Entrepreneurship (LSE) create opportunities for self-employment around one’s passions, hobbies and lifestyle skills?',
                'a' => [
                    'a1' => [
                        'Yes',
                        true,
                        'It is usually a home- based business that can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle.'
                    ],
                    'a2' => [
                        'No',
                        false,
                        'It is usually a home- based business that can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle. Examples of businesses that Lifestyle entrepreneurs run include: cake-making, homemade jams, chutneys etc., catering from home, childcare, dog walking, pet-sitting, virtual office services, online businesses and mobile hairdressing.'
                    ],
                ],
            ],
            'q5' => [
                'q' => 'Which of the following statements describes the typical LSE pursuing a new entrepreneurial opportunity?',
                'introPreQ' => 'Life style entrepreneur:',
                'a' => [
                    'a1' => [
                        'Does everything on his/her own and rarely asks for help from others,',
                        false,
                        'Successful beginning entrepreneurs recognize what they do not know about starting a new enterprise and seek out help from others with more experience.'
                    ],
                    'a2' => [
                        'hires others to perform all the tasks required for the new enterprise,',
                        false,
                        'Usually LSE have limited money to hire new employees'
                    ],
                    'a3' => [
                        'relies on others to do the "hard work",',
                        false,
                        'Usually LSE have limited money to hire new employees'
                    ],
                    'a4' => [
                        'does most things on his/her own and asks for help when needed.',
                        true,
                        'Successful life style entrepreneur performs many of the tasks required to start a new enterprise, but seeks expert advice in specific areas in which he/she is unfamiliar and needs help.'
                    ],
                ],
            ],
            'q6' => [
                'q' => 'Does successful life style entrepreneur realize the value of money?',
                'a' => [
                    'a1' => [
                        'Yes',
                        true,
                        'Successful entrepreneurs work hard for their money and usually spend it very carefully since money saved goes directly to the bottom line generating more profit.'
                    ],
                    'a2' => [
                        'No',
                        false,
                        'If you do not understand the value of money, it is nearly impossible to succeed as entrepreneur. Successful entrepreneurs do not over spend when buying materials, always negotiate for a better price, and save money for an unexpected expense or decrease in sales. Foolish entrepreneurs tend to spend too much money on things that do not generate profit or help the enterprise like more office space than needed, fancy restaurants, and expensive cars.'
                    ],
                ],
            ],
        ]
    ],
    2 => [
        'theme' => 'Lifestyle entrepreneurship (LSE)',
        'exercise' => 'Should I become a lifestyle entrepreneur?',
        'developedBy' => 'Social Innovation Fund, LT',
        'aim' => 'To understand what knowledge, skills and competences lifestyle entrepreneur needs.',
        'learningOutcomes' => [
            'Understand what you need to do to become a lifestyle entrepreneur.',
            'Ground the typical characteristics of an LSE',
            'Consider whether it is worth to seek LSE position.',
        ],
        'expectedDuration' => '30 minutes',
        'description' => 'This exercise will help you learn about some various life-style entrepreneurial characteristics and give you an idea what is important if you should consider seeking an entrepreneurial opportunity.',
        'task' => 'For each question, please select possible answer that you think describes the typical entrepreneur.',
        'questions' => [
            'q1' => [
                'q' => 'Can the lifestyle entrepreneur do business out of what he/she is passion about and enjoy?',
                'c' => 'yes',
                'f' => 'Lifestyle entrepreneurship is the chance to do business out of what you are passion about and enjoy. It is one of the best things we can do in our life so that we can live it the way we want; we can achieve something, have money and be happy and independent.',
            ],
            'q2' => [
                'q' => 'Are the management skills as the ability to plan, organize, manage, lead and delegate essential to all types of business including Life-Style Entrepreneurship?',
                'c' => 'yes',
                'f' => 'Management skills as the ability to plan, organize, manage, lead and delegate, analyze, communicate, debrief, evaluate and record are essential to all types of business including Life-Style Entrepreneurship.',
            ],
            'q3' => [
                'q' => 'Could a passion about your lifestyle business help you to succeed in developing it?',
                'c' => 'yes',
                'f' => 'If some sincerely love his/her lifestyle as a job and believe in its success, there is always a good chance that business will succeed. At first, lifestyle entrepreneurs can earn less (which is "normal" for each new business), but if they invest in themselves, learn and develop, they succeed.',
            ],
            'q4' => [
                'q' => 'Has a Life-style entrepreneurship a strong social aspect?',
                'c' => 'yes',
                'f' => 'Life-style entrepreneurship has a very strong social aspect, which helps to solve various problems of disadvantaged people, e.g. social, economic, health, isolation, unemployment.',
            ],
            'q5' => [
                'q' => 'Is the knowledge on opportunities for personal, professional and/or business activities, and basics of economics, essential for Life-Style Entrepreneurship?',
                'c' => 'yes',
                'f' => 'Opportunities for personal, professional and/or business activities, including ‘bigger picture’ issues that provide the context in which people live and work, understanding of the basics of economics, and the opportunities and challenges including an employer or organization are essential for Life-Style Entrepreneurship.',
            ],
            'q6' => [
                'q' => 'Can the negative entrepreneurial attitude help to run the Life-Style business?',
                'c' => 'no',
                'f' => 'A negative attitude will damage confidence, harm performance, paralyze your mental skills and may affect your health. With a positive attitude you will feel in control and confident and you will perform at your best. The life- style business can help run entrepreneurial attitude initiative; pro-activity; independence and innovation in personal, social and work life, motivation and determination to meet objectives.',
            ],
        ]
    ],
    3 => [
        'theme' => 'Business principles and Marketing in Life-Style Entrepreneurship',
        'exercise' => 'Main parts of the Business plan',
        'developedBy' => 'Social Innovation Fund, LT',
        'aim' => 'To deepen your understanding of the main parts of a business plan.',
        'learningOutcomes' => [
            'Ability to describe the main parts of the business plan.',
        ],
        'expectedDuration' => '15 minutes',
        'description' => 'This is the matching exercise. It presents the most important sections of a business plan and briefly introduces in the contents when writing a business plan. By completing this exercise you will learn the main parts of the business plan and why each of them is important.',
        'task' => 'Please read the descriptions of the key sections of a business plan on the left side below and then match them with the key sections on the right side.',
        'questions' => [
            'q1' => [
                'q' => 'Please read the descriptions of the key sections of a business plan on the left side below and then match them with the key sections on the right side.',
                'leftTitle' => 'The section of a business plan',
                'left' => [
                    'a' => 'Executive Summary',
                    'b' => 'Financial Plan',
                    'c' => 'Marketing Plan',
                    'd' => 'Management Team',
                    'e' => 'Product or Service',
                    'f' => 'Operating and control system',
                ],
                'rightTitle' => 'Description of the section',
                'right' => [
                    'a' => 'This section presents an overview of the key points in the business plan.',
                    'b' => 'This section demonstrates the financial viability of your new ventures. Typically this section includes an Income Statement, Balance Sheet and Cash Flow which show projected revenue, expenses, profit and needed funding for the next three years.',
                    'c' => 'This section characterizes the size of your target market, pricing strategy, channels of distribution, promotional strategy and sales strategy.',
                    'd' => 'This section demonstrates capability to implement this business plan.',
                    'e' => 'This section describes your new business idea, your product or service and explains the problem you are solving or the need that needs to be filled.',
                    'f' => 'This section explains how you will schedule and control operations, monitoring budgets and address security issues.',
                ],
            ]
        ]
    ],
    4 => [
        'theme' => 'Business principles and Marketing in Life-Style Entrepreneurship',
        'exercise' => 'Business principles in Life-Style Entrepreneurship',
        'developedBy' => 'Social Innovation Fund, LT',
        'aim' => 'To deepen your understanding of the principles of running and managing a business (including Life-Style entrepreneurship), such as setting a business mission, completing thorough market research and making financial planning.',
        'learningOutcomes' => [
            'Ability to describe the principles of running and managing a business.',
            'Discussing the important issues in starting Life-Style Entrepreneurship.',
        ],
        'expectedDuration' => '30 minutes',
        'description' => 'Every entrepreneur, including Life-Style entrepreneurs should be aware of main business principles in order to be successful in his/her business. This exercise will help you to learn what the main business principles are and what main aspects you should know when you decide to start your own business.',
        'task' => 'For each question, please select possible answer that you think is correct.',
        'questions' => [
            'q1' => [
                'q' => 'What is the purpose of mission statement of newly created business including Life-Style entrepreneurship?',
                'yes' => 'It is road map for business',
                'no' => 'It is key features what the business is about.',
                'c' => 'no',
                'f' => 'The Mission Statement will determine how others see the business, including customers and competitors; the reason why the business exists, what standards or ethics the business has; what the business is about (its key features). Ex. Ebay mission is “To provide a global trading platform where practically anyone can trade practically anything”.',
            ],
            'q2' => [
                'q' => 'Why SWOT analysis is important in Life-Style Entrepreneurship?',
                'yes' => 'Helps to develop business idea',
                'no' => 'Helps you to list your strengths and opportunities, and to address those areas you have identified as weaknesses or threats.',
                'c' => 'no',
                'f' => 'SWOT analysis is a list of your personal and business:<ul><li>Strengths</li><li>Weaknesses</li><li>Opportunities</li><li>Threats</li></ul>It takes into consideration factors such as: the current economic climate; social trends; personal experience. SWOT analysis helps you to list your strengths and opportunities which could be used for developing strong business strategy. Also it helps to address those areas you have identified as weaknesses or threats for your business. You should understand that by using strengths business could diminish possible threats; that by using strengths business could take advantage of opportunities; and that by using opportunities business could diminish weaknesses.',
            ],
            'q3' => [
                'q' => 'On which main areas should the market research concentrate?',
                'yes' => 'The areas such as: Legal entity; Close community; Neighbors; Taxation.',
                'no' => 'The areas such as: Industry; Customers; Competitors; Suppliers.',
                'c' => 'no',
                'f' => 'Market research is the foundation of every business plan. It provides the essential information required to establish if there is a market for your product/service. The market research should concentrate on the areas such as: industry, customers, competitors and suppliers, because it is important to know industry background, who is your customer (age, gender, income, culture, lifestyle); and who is your competitors and possible suppliers.',
            ],
            'q4' => [
                'q' => 'Is the desk research appropriate for market research?',
                'c' => 'yes',
                'f' => 'Desk research means finding relevant data which already exists and could provide you with useful information, much of it free. Using desk research method for market research you could collect useful data which could help you to make informed business decisions. Desk research includes Internet, newspapers, journals, magazines, etc. The other research method which could also be used for market research is Field research: visits, interview, focus groups, and questionnaires.',
            ],
            'q5' => [
                'q' => 'For what purpose the initial business finances are needed?',
                'yes' => 'the finances needed to cover business start-up costs;',
                'no' => 'the finances needed to hire employees',
                'c' => 'yes',
                'f' => 'Initial money is needed to cover your business start-up costs. Amount will depend on your circumstances, nature of your business, location of your business, etc. These costs may include:<ul><li>Equipment</li><li>Insurance</li><li>Rent</li><li>Stock</li><li>Stationery</li><li>Marketing materials</li></ul>',
            ],
        ]
    ],
    5 => [
        'theme' => 'Entrepreneurship and self-employment (SE)',
        'exercise' => 'Understanding of entrepreneurship and self-employment (SE)',
        'developedBy' => 'CARDET',
        'aim' => 'To understand the match between entrepreneurial qualification and the skill set and environment for self-employment',
        'learningOutcomes' => [
            'Assess your skills in order to aim for the employment structure best suited to it',
            'Finding the balance between room for growth and entrepreneurial adventures',
            'Making professional decisions',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'This exercise will help you to check your knowledge and better understand the importance of assessing your skills before taking the decision becoming a self-employed entrepreneur. It will also help you to identify whether your skill set is in line with the entrepreneurial qualifications needed.',
        'task' => 'For each question, please select possible answer that you think.',
        'questions' => [
            'q1' => [
                'q' => 'Identifying your skills will help you to better decide the areas that your enterprise or business should focus on.',
                'c' => 'yes',
                'f' => 'Identifying your skills will definitely help you to decide on the areas that your enterprise or business should focus on. Self-employment is not for everyone. Assess your skills and make your research in terms of what skills are necessary for each area of activity. The harder you work to build your skills on a particular sector, the more successful you can become on that specific area.',
            ],
            'q2' => [
                'q' => 'Assessing your skills can help you to do what you love as a self-employed entrepreneur.',
                'c' => 'no',
                'f' => 'Assessing your skills will not help you to do what you love as a self-employed entrepreneur. “Do what you love” is a piece of a big business mistake. Becoming an entrepreneur or self-employed should focus on doing what you’re good at and what people will pay you (well) for. Becoming self-employed or establishing a new business should be first and foremost a profitable situation and then an activity you love.',
            ],
            'q3' => [
                'q' => 'Being aware of your strengths can put you in a position to ignore the competition.',
                'c' => 'no',
                'f' => 'Knowing your strengths is very useful for every entrepreneur, but it will not put you in a position to ignore the competition. Ignoring the competition is a potentially serious business mistake. Market saturation does not allow “room” for all. Thus, should you want to make an opening to a service/product/business, you need to make sure that the market is not “saturated” with yours type of business.',
            ],
            'q4' => [
                'q' => 'A person who is self-employed is also classified as a self-employed entrepreneur.',
                'c' => 'no',
                'f' => 'A person who is self-employed does not mean that is also an entrepreneur. Self-employed is a situation in which individuals work for themselves instead of working for an employer who pays a salary or a wage. Self-employed individuals, or independent contractors, earns their income through conducting profitable operations from a trade or business that they operate directly.',
            ],
            'q5' => [
                'q' => 'Does the ability to manage a variety of tasks and people, will set you apart from others in becoming a successful self-employed entrepreneur?',
                'c' => 'yes',
                'f' => 'If you want to become a successful self-employed entrepreneur, you need to be able to manage a variety of tasks and people (sometimes simultaneously). As a self-employed entrepreneur you will be called to delicate many complex situations related with finances, business administration and employees. Thus, in order to succeed as a self-employed entrepreneur and stand out from others, it is important to learn how to operate different tasks and people at the same time as well as to know how to exploit the knowledge from your own resources.',
            ],
            'q6' => [
                'q' => 'A broad knowledge of fields and abilities (being an expert in several areas) will help you to become a successful self-employed entrepreneur.',
                'c' => 'yes',
                'f' => 'A successful self-employed entrepreneur needs to have a broad knowledge of fields and abilities (being an expert in several areas). Of course having a deep knowledge of the business field that you decide to pursue as a self-employed entrepreneur is very important, yet, self-employed entrepreneurs are characterized from their ability to develop multifaceted skills and initiatives necessary to anticipate current and future needs and bring good new ideas to market.',
            ],
            'q7' => [
                'q' => 'Being isolated and working hard to achieve your goal will set you apart from others in becoming a successful self-employed entrepreneur.',
                'c' => 'no',
                'f' => 'Working hard to achieve your goal is important but working hard and being isolated is not what successful self-employed entrepreneurs do. To distinguish from others, besides being focus and working hard, you need to be in the position to identify the key stakeholders, building trust and set up relationships.',
            ],
            'q8' => [
                'q' => 'Trusting your feeling will set you apart from others in becoming a successful self-employed entrepreneur. Do you agree with this statement?',
                'c' => 'no',
                'f' => 'Trusting your feeling is not enough in order to become a successful self-employed entrepreneur. You need to be in a position to identify the risk as well as to know how to solve problems and overcome barriers/dealing with failures. Also, it is important to have a vision and purpose to what you are doing.',
            ],
        ]
    ],
    6 => [
        'theme' => 'Entrepreneurship and self-employment (SE)',
        'exercise' => 'What are today’s benefits and challenges that self-employed professionals face?',
        'developedBy' => 'CARDET',
        'aim' => 'To understanding the benefits and challenges of self-employment',
        'learningOutcomes' => [
            'Understanding the benefits of self-employment',
            'Understanding the challenges of self-employment',
            'Understanding the balance between the benefits and challenges of self-employment',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'This exercise will help you check your knowledge and better understand the opportunities and challenges that a self-employed person faces as well as it will help you identify the balance line that a successful self-employed person needs to have in his/her professional life.',
        'task' => 'Please read each statement and select the correct answer.',
        'questions' => [
            'q1' => [
                'q' => 'Self-employed professionals should demonstrate confident decision making abilities.',
                'c' => 'yes',
                'f' => 'Being able to make important decision for your business is a constant challenge. Self-employed individuals do not always feel confident of the business decisions they make. To be in a position to overcome this barrier as a self-employed professional you should make sure that ongoing training and education is consistently listed as one of your main priorities. This will make you more confident in making decisions and be adaptable to every situation.',
            ],
            'q2' => [
                'q' => 'Self-employment business offers you financial security.',
                'c' => 'no',
                'f' => 'Self-employment is not a guaranteed source of income. One of the main risks you are called to face as a self-employed professional is the irregular income, especially at the beginning. There is also the possibility that some moths you will have no profit at all, while you need to continue to pay running costs (i.e rent, insurance and internet access). You need to be ready to face the unsteady pay structure of being self-employed and plan your finance accordingly having in mind that some months there will be minimal income or no income at all.',
            ],
            'q3' => [
                'q' => 'Self-employed individuals are usually at the risk of social isolation.',
                'c' => 'yes',
                'f' => 'Self-employed people seem to suffer from loneliness and isolation. In order to overcome this challenge, it is important to set some standards that will help you not falling in the loop of losing your inspiration or motivation. For example you can arrange to meet in person with your clients rather than send them an email, or even discuss over the phone. Another way to prevent loneliness and isolation is to develop a network of people around you who share the same passion and concerns.',
            ],
            'q4' => [
                'q' => 'Self-employment gives you the ability to work flexible hours.',
                'c' => 'yes',
                'f' => 'One of the biggest benefits of self-employment is the ability to work flexible hours and at your own pace. Nevertheless, this often means that you need to work harder than you would as an employee, since being your own boss comes with various responsibilities. When there is a particular work that needs to be done with specific deadline along with having to manage different clients and deal with several issues means that you need to work a lot of hours giving your best to what you do.',
            ],
            'q5' => [
                'q' => 'Self-employment requires discipline and motivation.',
                'c' => 'yes',
                'f' => 'Self—employment indeed requires discipline and motivation. To make each workday more productive, you need to be discipline and have motivation. This means that as a self-employed you need to firmly structure your day, block out distractions (including social media), carefully choose work assignments and be well organized with your work environment. At the same time, it is equally important to take breaks (including exercise breaks), create an environment that is conductive to work as well as to give yourself a weekend off in order to get personal tasks done as well as to boost your overall productivity and creativity.',
            ],
            'q6' => [
                'q' => 'Being self-employed can lead to starting your own business.',
                'c' => 'yes',
                'f' => 'Self-employed individuals often end up starting their own businesses or startups. This is because self-employment put people in a position to deal constantly with situations and decisions as they would in the case they were running their own business. Still, starting a business as well as having employees, involves a lot more responsibility than being self-employed in terms of taking risks and constant challenges.',
            ],
            'q7' => [
                'q' => 'Being self-employed requires a personal commitment.',
                'c' => 'yes',
                'f' => 'Being self-employed indeed requires a personal commitment that is greater than what most jobs will require. The success or the failure of the work of self-employed people rests on their shoulders. Being self-employed can be more exciting at times or more stressful than working for another person. This also means that self-employed people need to work very long hours, many days of each week.',
            ],
        ]
    ],
    9 => [
        'theme' => 'The use of ICT in Life-Style Entrepreneurship',
        'exercise' => 'The importance of ICT for life style entrepreneurship',
        'developedBy' => 'CWEP, PL',
        'aim' => 'To deepen your understanding of the importance of using ICT in Lifestyle Entrepreneurship.',
        'learningOutcomes' => [
            'Understand the benefits of using ICT for lifestyle Entrepreneurship.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'Nowadays most of the business operate also online and take advantage of various technological solutions. There are different ways of using ICT in lifestyle entrepreneurship therefore it is advisable to consider this option. ICT can be very helpful in expanding to other markets, running an online shop, promotional campaign and for many other purposes. This exercise will help you learn about the importance of ICT for the lifestyle entrepreneur and benefits it provides.',
        'task' => 'Which of the following statements describes the benefits offered by ICT for lifestyle entrepreneur?',
        'settings' => [
            'intro' => 'Using ICT in lifestyle entrepreneurship:'
        ],
        'questions' => [
            'q1' => [
                'q' => 'makes it easier to expand to other markets',
                'c' => 'yes',
                'f' => 'Using ICT for a lifestyle entrepreneurship is definitely a good choice that enables for example to find new customers and makes it easier to expand to other markets. The most typical example is selling online which greatly improves the visibility of the product and provides means of reaching out to many customers from all over the world.',
            ],
            'q2' => [
                'q' => 'guarantees Financial Profit',
                'c' => 'no',
                'f' => 'While ICT can be very beneficial, it is not a magical remedy for all the troubles. As such, it will not guarantee a financial profit without a good idea for the business and the skilled lifestyle entrepreneur behind it.',
            ],
            'q3' => [
                'q' => 'gives various options for advertising',
                'c' => 'yes',
                'f' => 'Using ICT for a lifestyle entrepreneurship enables to advertise the business. Especially with the products that you make yourself, an Instagram account or a YouTube channel can be utilised to maximise the reach of marketing activities.',
            ],
            'q4' => [
                'q' => 'makes the communication with customers faster',
                'c' => 'yes',
                'f' => 'Nowadays customers require almost immediate response to whatever queries they might have. A customer who is waiting for an answer for a long time is a lost customer indeed. This is why specific social media such as Facebook or Twitter can be used for near-instant direct communication with your customers. They will surely appreciate a fast response time.',
            ],
            'q5' => [
                'q' => 'allows to sell the products online',
                'c' => 'yes',
                'f' => 'Using ICT for a lifestyle entrepreneurship enables to sell the products online. Even if you do not have an expert-level ICT skills, there are ready platforms that allow you to set up your own online store shop literally within the minutes.',
            ],
            'q6' => [
                'q' => 'guarantees that many customers place the order',
                'c' => 'no',
                'f' => 'Just by using ICT it cannot be guaranteed that many customers will place the order. It depends on the type of business rather than the fact if ICT is being used or not. ICT greatly increases the chances, but, again, does not guarantee that.',
            ],
            'q7' => [
                'q' => 'allows not to pay the taxes from sold goods',
                'c' => 'no',
                'f' => 'ICT and taxes are two separate things and the lifestyle entrepreneurs have to be in line with current financial regulations.',
            ],
        ]
    ],
    10 => [
        'theme' => 'The use of ICT in Life-Style Entrepreneurship',
        'exercise' => 'Prerequisites of using ICT for life style entrepreneurship',
        'developedBy' => 'CWEP, PL',
        'aim' => 'To deepen your understanding how important ICT is for lifestyle Entrepreneurship.',
        'learningOutcomes' => [
            'Understand the limitations and scope of application of ICT for lifestyle Entrepreneurship.',
            'Understand the prerequisites that the lifestyle entrepreneur should have to use ICT for their business.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'Nowadays most of the business operate also online and take advantage of various technological solutions. There are different ways of using ICT in lifestyle entrepreneurship therefore it is advisable to consider this option. The generally understood online presence (online store, social media account) can be very beneficial for your business, but all of them share some common prerequisites. It is important to be aware of them, and to be comfortable with the solutions you would like to implement. This exercise will help you learn about specific prerequisites and will also debunk some myths about implementing ICT solutions for your business.',
        'task' => 'For each question, please select the answer that you think is the correct one',
        'settings' => [

        ],
        'questions' => [
            'q1' => [
                'q' => 'Are ICT skills mandatory to become a lifestyle entrepreneur?',
                'c' => 'no',
                'f' => 'A lifestyle entrepreneur does not in fact require any ICT skills. While they are clearly beneficial, all of the ICT tasks (like setting up an online shop) can also be done by external experts usually for a small fee.',
            ],
            'q2' => [
                'q' => 'Should a lifestyle entrepreneur hire an IT person on a permanent contract to handle all technical tasks?',
                'c' => 'no',
                'fy' => 'A lifestyle entrepreneur does not need to hire an IT person as vast majority of ICT solutions suitable for lifestyle entrepreneurship can be implemented very easily.',
                'fn' => 'A lifestyle entrepreneur is capable of setting the most typical ICT solutions on their own. Even if the help is required, it is better to be done on a temporary and not permanent basis.',
            ],
            'q3' => [
                'q' => 'Should lifestyle entrepreneur demonstrate strong technical skill to incorporate ICT in their business?',
                'c' => 'no',
                'fy' => 'Having strong technical skills can be beneficial, but thanks to the solutions available today it is no longer a requirement.',
                'fn' => 'Nowadays all online solutions are prepared with simplicity in mind, so that even non-technical people can handle them on their own. For example, setting up the online presence with the use of social media is very easy and can be handled literally within minutes even by someone who has never done it.',
            ],
            'q4' => [
                'q' => 'Is setting up an online shop very difficult and therefore not advised for beginning lifestyle entrepreneurs?',
                'c' => 'no',
                'fy' => 'While some years ago it was true in most of the cases, it is no longer nowadays. It is usually very simple to use already existing online shops platforms that require very little effort to set everything up.',
                'fn' => 'It is usually very simple to use already existing online shops platforms that require very little effort to set everything up.',
            ],
            'q5' => [
                'q' => 'Does it require to have own hardware to run the website, online store etc.?',
                'c' => 'no',
                'f' => 'The website and the online store, as well as many different services that use ICT are nowadays supplied by professionals in the area. The lifestyle entrepreneur just needs to get access to relevant hosting services.',
            ],
            'q6' => [
                'q' => 'Is using ICT for the purpose of lifestyle entrepreneurship very costly?',
                'c' => 'no',
                'f' => 'Because of growing competition in the ICT sector, many services are available either for free or for a small fee. In either case, they cannot be described as very costly.',
            ],
            'q7' => [
                'q' => 'Do the ICT solutions require constant supervision and maintenance?',
                'c' => 'no',
                'f' => 'Most of the solutions have already a team of technical experts that will make sure that the ICT service you are using is fully operational at all times.',
            ],
            'q8' => [
                'q' => 'Is the implementation of English language mandatory?',
                'c' => 'no',
                'fy' => 'Obviously you are not forced to implement English language and provide descriptions of your products in English.',
                'fn' => 'While it is not mandatory to implement English language for your lifestyle business, it is highly recommended. Doing so will enable you to reach customers even from outside your country.',
            ],
        ]
    ],
    13 => [
        'theme' => 'Life-Style Entrepreneurship',
        'exercise' => 'Skills of the self-employed entrepreneurs',
        'developedBy' => 'KNJUC, LT',
        'aim' => 'To deepen the understanding of what skills successful self-employed entrepreneur needs and why he/she needs to have such skills.',
        'learningOutcomes' => [
            'Understanding which skills are important for self-employed entrepreneur to be successful.',
            'Understanding the importance of certain skills for successful self-employed entrepreneur.',
        ],
        'expectedDuration' => '15 minutes',
        'description' => 'Nowadays most of the business operate also online and take advantage of various technological solutions. There are different ways of using ICT in lifestyle entrepreneurship therefore it is advisable to consider this option. The generally understood online presence (online store, social media account) can be very beneficial for your business, but all of them share some common prerequisites. It is important to be aware of them, and to be comfortable with the solutions you would like to implement. This exercise will help you learn about specific prerequisites and will also debunk some myths about implementing ICT solutions for your business.',
        'task' => 'For each question, please select the answer which you think is correct.',
        'settings' => [],
        'questions' => [
            'q1' => [
                'q' => 'Ability to make  decisions is important for a self-employed entrepreneur because',
                'yes' => 'it will help to overcome frustration, to seek and learn more and to be patient working for the success',
                'no' => 'it will help to be make realistic decisions in various life situations, put them into practice and reach conclusions',
                'c' => 'no',
                'f' => 'Ability to make decisions allows choosing between different possibilities, understanding the impact of decision making on yourself and others, making realistic decisions, putting them into practice and reaching conclusions.',
            ],
            'q2' => [
                'q' => 'Tolerance is an essential skill of a successful self-employed entrepreneur because',
                'yes' => 'it will help to overcome difficulties at work with respect to tasks',
                'no' => 'it will help to be open minded to other opinions, practices, cultures, religions',
                'c' => 'no',
                'f' => 'Tolerant people demonstrate fair, objective and open attitude toward those whose opinions, beliefs, practices, racial or ethnic origins, etc., are different from yours; are open minded to other cultures, religions and value diversity. Tolerance is valuable for self-employed entrepreneur when the interaction with clients, customers, and suppliers happens as they all have different attitudes and perspectives which have to be tolerated.',
            ],
            'q3' => [
                'q' => 'A successful self-employed entrepreneur has to tolerate risk because',
                'yes' => 'it will help clearly understand challenges in self-employment and be ready to deal with them effectively',
                'no' => 'it will help to get things done without putting huge efforts',
                'c' => 'yes',
                'f' => 'A successful self-employed entrepreneurs are open-minded toward accepting the possibility of risks in employment and/or entrepreneurship in order to improve employment situation, to try new jobs or to start own business. It could help clearly understand challenges in self-employment and be ready to deal with them effectively.',
            ],
            'q4' => [
                'q' => 'Having a vision is very important for successful self-employed entrepreneur because',
                'yes' => 'it will help to focus attention for a task, activity, object during the specific period of time.',
                'no' => 'it will help you to focus attention on future goals and prioritize of tasks',
                'c' => 'no',
                'f' => 'Successful self-employed entrepreneur focus attention on future goals and prioritize of tasks, identify what needs to be done to achieve it, plans actions which enable to reach specified objectives or deadlines.',
            ],
        ]
    ],
    14 => [
        'theme' => 'Life-Style Entrepreneurship',
        'exercise' => 'What skills does the self-employed entrepreneur need?',
        'developedBy' => 'KNJUC, LT',
        'aim' => 'To deepen the understanding of what skills are needed for successful self-employed entrepreneur.',
        'learningOutcomes' => [
            'Indicating the main skills a successful self-employed entrepreneur should have.',
            'Understanding why these skills are important for successful self-employment.',
        ],
        'expectedDuration' => '15 minutes',
        'description' => 'This exercise will help you learn about some skills of a self-employed entrepreneur. Characteristics of skills will give you an idea why such skills are important for successful self-employed entrepreneurs.',
        'task' => 'For each question, please select the answer which you think is correct.',
        'settings' => [],
        'questions' => [
            'q1' => [
                'q' => 'Which of the two skills characterize a self-employed entrepreneur?',
                'yes' => 'Discipline',
                'no' => 'Putting extensive effort and maintaining a single idea',
                'c' => 'yes',
                'fy' => 'Disciplined people value their time and the time of others. They arrive early to meetings and are fully prepared when their customers come to visit them. Disciplined people make it a habit to keep their meetings, goals and deadlines on a calendar and do all they can to meet their goals and objectives in a timely manner. They tend to keep notes, make lists, have calendars, reminders set on their phones and a daily schedule of events they organize their efforts around.',
                'fn' => 'To put extensive effort to focus on a single idea is not a main characteristic of self-employed entrepreneur. Discipline is more important as it is an appreciation of own time and the time of others (clients, partners, etc.).',
            ],
            'q2' => [
                'q' => 'Which of the two skills characterize a self-employed entrepreneur?',
                'yes' => 'Creativity',
                'no' => 'Working on the clock',
                'c' => 'yes',
                'f' => 'Successful self-employed entrepreneurs are those who never work on the clock. They always seek for new creative ideas to improve their vision and do not focus on a single idea. Passion and determination is all it takes to become successful.',
            ],
            'q3' => [
                'q' => 'Which of the two skills characterize a self-employed entrepreneur?',
                'yes' => 'Slothfulness',
                'no' => 'Motivation',
                'c' => 'no',
                'fy' => 'Being slothful means being lazy. A successful self-employed entrepreneur could never be slothful otherwise he/she could never make his/her life-style business to succeed. Successful self-employed entrepreneurs understand, that motivation it is a key point for success. They are always ready to do what should be done, have the strength to complete their tasks, can work on own initiative.',
                'fn' => 'Successful self-employed entrepreneurs understand, that motivation it is a key point for success. They are always ready to do what should be done, have the strength to complete their tasks, can work on own initiative. A successful self-employed entrepreneur could never been slothful otherwise he/she could never make his/her life-style business to succeed.',
            ],
            'q4' => [
                'q' => 'Which of the two social skills better characterize a self-employed entrepreneur?',
                'yes' => 'Confidence',
                'no' => 'Modesty',
                'c' => 'yes',
                'fy' => 'This skill of confidence demonstrates to others self-employed entrepreneurs know how to do things and accept the consequences of own decisions. Feel comfortable with new circumstances in the labour market and be able to instill confidence in other people in own decisions.',
                'fn' => 'Successful life-style entrepreneurs should never be modest. They should not be afraid showing confidence as being confident will demonstrate to others self-employed entrepreneurs know how to do things and accept the consequences of own decisions. Forget about being modest. Instead, share your excitement, your passion and your proudest moments in order to show your success to others.',
            ],
            'q5' => [
                'q' => 'Which of the two social skills better characterize a self-employed entrepreneur?',
                'yes' => 'Flexibility',
                'no' => 'Rigidity',
                'c' => 'yes',
                'fy' => 'Being flexible means adapting to the changing outward environment as quickly as possible. A successful self-employed entrepreneur should be ready to make use of such changes and turn them into opportunity to reach success in their businesses. A successful self-employed entrepreneur should also be flexible when attempting to satisfy customers\' needs, as it could lead to bigger sales and help to gain bigger acknowledgement.',
                'fn' => 'The successful self-employed entrepreneur should not be rigid. Rigidity limits acceptance and value changes in employability, ability to deal with uncertainty, understanding different points of view. On the contrary a successful self-employed entrepreneur should be flexible and be ready to make use of such changes and turn them into opportunity to reach success in their businesses. Also a successful self-employed entrepreneur should be flexible when attempting to satisfy customers\' needs, as it could lead to bigger sales and help to gain bigger acknowledgement.',
            ],
        ]
    ],
    15 => [
        'theme' => 'Entrepreneurship and Life-style Entrepreneurship ',
        'exercise' => 'What are differences between Entrepreneurship and Life-style Entrepreneurship?',
        'developedBy' => 'KNJUC, LT',
        'aim' => 'To deepen the understanding of differences between Entrepreneurship and Life-style Entrepreneurship.',
        'learningOutcomes' => [
            'Ability to compare Entrepreneurship and Life-style Entrepreneurship.',
            'Analysing the differences between Entrepreneurship and Life-style Entrepreneurship.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'This exercise will help you learn about some typical entrepreneurial characteristics when comparing Entrepreneurship and Life-style Entrepreneurship. The traditional Entrepreneurship and Life-Style Entrepreneurship has some similarities, but they are also rather different. This exercise will reveal the main differences between two of them.',
        'task' => 'For each question, please select possible answer that you think is correct. ',
        'settings' => [],
        'questions' => [
            'q1' => [
                'q' => 'Do Entrepreneurs and Life-style Entrepreneurs both need to have an exit strategy?',
                'c' => 'no',
                'f' => 'An exit strategy is a way to get out of your business. Traditional entrepreneurs always have an exit strategy in order to be sure that they can work 100 hours a week, make a nice big valuable company, and then sell it before they go completely insane. Lifestyle Entrepreneurs don’t care about exit strategy because they are creating something simple that they are passionate about to support the life they want to live.',
            ],
            'q2' => [
                'q' => 'Is it equally important for both Entrepreneurs and Life-style Entrepreneurs to seek for investors who care about a return on their investment?',
                'c' => 'no',
                'fy' => 'Lifestyle entrepreneurs do not seek for investors. The life-style business could start with almost no initial cost. Entrepreneurs seek for investors who could support their business in order to make it bigger; however investors seek for a return on their investment.',
                'fn' => 'Entrepreneurs seek for investors who could support their business in order to make it bigger; however investors seek for a return on their investment. Lifestyle entrepreneurs do not seek investors. The life-style business could start with almost no initial cost.',
            ],
            'q3' => [
                'q' => 'Is profit the main motivator for Entrepreneurs and Life-style Entrepreneurs?',
                'c' => 'no',
                'f' => 'Entrepreneurs typically want to grow profits as fast as possible. Lifestyle entrepreneurs usually desire adventure more. They could be motivated by quality of life rather than growth of profit.',
            ],
            'q4' => [
                'q' => 'Are there any differences between Entrepreneurs and Life-style Entrepreneurs when thinking about scope of business?',
                'c' => 'yes',
                'f' => 'Entrepreneurs could form a huge corporation. Life-style Entrepreneurs form a sole proprietorship. The difference is that most of the profit for life-style entrepreneurs is generated as the owner’s income. Keeping cost low and personal profit high will make a very sustainable lifestyle without massive business profits.',
            ],
            'q5' => [
                'q' => 'Are both Entrepreneurs and Life-style Entrepreneurs are willing to work 80 hours per week?',
                'c' => 'no',
                'fy' => 'Entrepreneurs are willing to sink all their time into their business, staying late, waking up early, and doing all the tasks no-one else wants to do. Lifestyle entrepreneur’s aim to simplify and automate to have more freedom.',
                'fn' => 'Lifestyle entrepreneur’s aim to simplify and automate to have more freedom. Entrepreneurs are willing to sink all their time into their business, staying late, waking up early, and doing all the tasks no-one else wants to do.',
            ],
        ]
    ],
    16 => [
        'theme' => 'Entrepreneurship and Life-style Entrepreneurship ',
        'exercise' => 'What are the similarities between Entrepreneurship and Life-style Entrepreneurship?',
        'developedBy' => 'KNJUC, LT',
        'aim' => 'To deepen the understanding of similarities between Entrepreneurship and Life-style Entrepreneurship.',
        'learningOutcomes' => [
            'Ability to compare Entrepreneurship and Life-style Entrepreneurship.',
            'Explaining the similarities between Entrepreneurship and Life-style Entrepreneurship.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'The traditional Entrepreneurship and Life-Style Entrepreneurship have some typical common entrepreneurial characteristics. This exercise will introduce you some similarities between Entrepreneurship and Life-style Entrepreneurship.',
        'task' => 'For each question, please select possible answer that you think is correct. ',
        'settings' => [],
        'questions' => [
            'q1' => [
                'q' => 'Is it important for both Entrepreneurs and Life-style Entrepreneurs to define personal goals and purpose?',
                'c' => 'yes',
                'f' => 'In both cases personal goals should drive your business goals. Both Entrepreneurs and Life-style Entrepreneurs will never be satisfied or happy about their business if it is not true to core beliefs and personal interests.',
            ],
            'q2' => [
                'q' => 'Is learning and growing more important for Life-style Entrepreneurship than Entrepreneurship?',
                'c' => 'no',
                'f' => 'The world of both Entrepreneurship and Life-style Entrepreneurship is changing quite rapidly, so learning and growing is important for both Life-style Entrepreneurship and Entrepreneurship. Successful Entrepreneurs and Life-Style Entrepreneurs should understand that if you aren’t changing and learning, you are failing behind in business.',
            ],
            'q3' => [
                'q' => 'Is it important for both Life-style Entrepreneurs and Entrepreneurs to have positive attitude and consider problems as a challenge not as a failure?',
                'c' => 'yes',
                'f' => 'Both Life-style Entrepreneurs and Entrepreneurs should have positive attitude and consider problems as a challenge not as a failure. Setbacks must be seen as normal and expected challenges, not as indications or failure. Successfully recovering from problems should be a key source for satisfaction for both of them.',
            ],
            'q4' => [
                'q' => 'Is satisfaction from team work and success more important for Entrepreneurship than Life-style Entrepreneurship?',
                'c' => 'no',
                'f' => 'In both Entrepreneurship and Life-style Entrepreneurship to build a team and enjoy the success together would be great achievement. Building a successful work team is difficult and challenging. But an effective team can complement you and support in various situations helping to overcome business challenges.',
            ],
        ]
    ],
    17 => [
        'theme' => 'Businesses principles and marketing in LSE Entrepreneurship',
        'exercise' => 'How do I develop my business plan? What do I have to know about the main principles of the business plan?',
        'developedBy' => 'Know and Can Association',
        'aim' => 'To understand the basics of the business plan development and gain skills to develop one.',
        'learningOutcomes' => [
            'Understand the structure of a business plan',
            'To understand the conception of marketing as a whole',
        ],
        'expectedDuration' => '30 minutes',
        'description' => 'This exercise will help you learn understand some of the basic business principles and marketing in LSE Entrepreneurship and to evaluate the level of your knowledge regarding these topics. There are some basic questions providing the essential information you need to know in order to know the business world and marketing principles better. If you acquire the whole information, you will know how to develop a simple business plan by yourself.',
        'task' => 'Please select the answer/s you think is/are the most correct',
        'questions' => [
            'q1' => [
                'q' => 'Which are the necessary parts on the business plan? There is more than 1 correct answer.',
                'multiple' => true,
                'a' => [
                    'a1' => [
                        'Summary',
                        true,
                    ],
                    'a2' => [
                        'Mission',
                        true,
                    ],
                    'a3' => [
                        'Aims',
                        true,
                    ],
                    'a4' => [
                        'Objectives',
                        true,
                    ],
                    'a5' => [
                        'SWOT analysis',
                        true,
                    ],
                    'a6' => [
                        'Market Research',
                        true,
                    ],
                    'a7' => [
                        'Marketing Plan',
                        true,
                    ],
                    'a8' => [
                        'Finances',
                        true,
                    ],
                    'a9' => [
                        'Supporting documents',
                        true,
                    ],
                ],
                'fy' => 'The necessary parts of the business plan are Summary, Mission, Aims, Objectives, SWOT analysis, Market Research, Marketing Plan, Finances and Supporting documents;',
                'fn' => 'The complete business plan should have all the necessary parts: Summary, Mission, Aims, Objectives, SWOT analysis, Market Research, Marketing Plan, Finances and Supporting documents;',
            ],
            'q2' => [
                'q' => 'Which of the statements describes a good business’s Mission? There is only 1 correct answer.',
                'a' => [
                    'a1' => [
                        'It should clearly describe the purpose of business, explain what the key features of the business are and be based on the key words which describe the business the best',
                        true,
                        'The strong business Mission should be concrete and simple, it should clearly describe the purpose and the key features of the business and before developing the final business Mission statement, you should make a list of main key words which describe the business the best.'
                    ],
                    'a2' => [
                        'It should describe only the key features of the business',
                        false,
                        'The business mission has to describe the key features of the business but not only them. The strong business Mission should be concrete and simple, it should not consist of complex words and phrases, it should clearly describe the purpose and the key features of the business and before developing the final business Mission statement, you should make a list of main key words which describe the business the best.'
                    ],
                    'a3' => [
                        'It has to be very long in order to help the future customers understand our business better',
                        false,
                        'The business mission has to be short and simple. The strong business Mission should be concrete and simple, it should not consist of complex words and phrases, it should clearly describe the purpose and the key features of the business and before developing the final business Mission statement, you should make a list of main key words which describe the business the best.'
                    ],
                ],
            ],
            'q3' => [
                'q' => 'What is SWOT analysis? Choose the most corrects answer. There is only 1 correct answer.',
                'a' => [
                    'a1' => [
                        'It is a strategic planning technique used to identify strengths, weaknesses, opportunities, and threats connected with business competition.',
                        true,
                        'SWOT analysis is a strategic planning technique used to help to identify strengths, weaknesses, opportunities, and threats related to business competition or planning. Its aim is to specify the objectives of the business climate and identify the internal and external factors that are favorable and unfavorable to achieving the business objectives.'
                    ],
                    'a2' => [
                        'It is a technique for measuring the weak and strong points of the business in order to improve it.',
                        false,
                        'SWOT analysis does measure strengths and weaknesses but it also focuses on opportunities and threats. SWOT analysis is a strategic planning technique used to help to identify strengths, weaknesses, opportunities, and threats related to business competition or planning. Its aim is to specify the objectives of the business climate and identify the internal and external factors that are favorable and unfavorable to achieving the business objectives.'
                    ],
                    'a3' => [
                        'It is a technique for measuring the opportunities and strengths of the market in order to start a new business.',
                        false,
                        'SWOT analysis does measure opportunities and strengths but is also focuses on weaknesses and threats. SWOT analysis is a strategic planning technique used to help to identify strengths, weaknesses, opportunities, and threats related to business competition or planning. Its aim is to specify the objectives of the business climate and identify the internal and external factors that are favorable and unfavorable to achieving the business objectives.'
                    ],
                ],
            ],
            'q4' => [
                'q' => 'Which are the 4 main elements (the 4 Ps) of marketing? There is only 1 correct answer.',
                'a' => [
                    'a1' => [
                        'Products, Prices, Place, Promotion;',
                        true,
                        'Marketing is the function of any organization or program whose goal is to plan, price, promote, and distribute the organization’s programs and products by keeping in constant touch with the organization’s various constituencies, uncovering their needs and expectations for the organization and themselves, and building a program of communication to not only express the organization’s purpose and goals, but also their mutually beneficial want-satisfying products” (Philip Kotler).'
                    ],
                    'a2' => [
                        'Products, People, Placement, Purchasing;',
                        false,
                        'Marketing is the function of any organization or program whose goal is to plan, price, promote, and distribute the organization’s programs and products by keeping in constant touch with the organization’s various constituencies, uncovering their needs and expectations for the organization and themselves, and building a program of communication to not only express the organization’s purpose and goals, but also their mutually beneficial want-satisfying products” (Philip Kotler).<br />The 4 Ps are:<br />-Product: A product can be either a tangible good or an intangible service that fulfills.<br />-Price: Price determinations will impact profit margins, supply, demand and marketing strategy.<br />-Promotion: Promotion is a way to disseminate relevant product information to consumers and differentiate a particular product or service. It includes elements like: advertising, public relations, social media marketing, email marketing, search engine marketing, video marketing and more.<br />-Place: The place is the ideal channels are to convert potential clients into actual clients.'
                    ],
                    'a3' => [
                        'Prices, Promotion, Perfection, Products;',
                        false,
                        'Marketing is the function of any organization or program whose goal is to plan, price, promote, and distribute the organization’s programs and products by keeping in constant touch with the organization’s various constituencies, uncovering their needs and expectations for the organization and themselves, and building a program of communication to not only express the organization’s purpose and goals, but also their mutually beneficial want-satisfying products” (Philip Kotler)<br />The 4 Ps are:<br />-Product: A product can be either a tangible good or an intangible service that fulfills.<br />-Price: Price determinations will impact profit margins, supply, demand and marketing strategy.<br />-Promotion: Promotion is a way to disseminate relevant product information to consumers and differentiate a particular product or service. It includes elements like: advertising, public relations, social media marketing, email marketing, search engine marketing, video marketing and more.<br />-Place: The place is the ideal channels are to convert potential clients into actual clients.'
                    ],
                ],
            ],
        ]
    ],
    21 => [
        'theme' => 'Lifestyle entrepreneurship (LSE)',
        'exercise' => 'What are the basics of Life-Style Entrepreneurship?',
        'developedBy' => 'Entrepreneurship Initiative (VINI), LT',
        'aim' => 'To understand what lifestyle Entrepreneurship is.',
        'learningOutcomes' => [
            'Understand and recognize the lifestyle entrepreneurship',
            'Understand the principles of LSE.',
        ],
        'expectedDuration' => '20 minutes',
        'description' => 'This exercise will help you to learn about some traditional and LSE entrepreneurship. There are some typical lifestyle entrepreneurial principles that many entrepreneurs share. In addition, LSE sometimes is very similar to traditional entrepreneurship. This exercise will help you to recognize and identify LSE and traditional entrepreneurship.',
        'task' => 'For each question, please select possible answer',
        'questions' => [
            'q1' => [
                'q' => 'Are LSE and traditional entrepreneurshbased on the same business principles?',
                'a' => [
                    'a1' => [
                        'LSE and traditional entrepreneurship are based on the same business principles',
                        false,
                    ],
                    'a2' => [
                        'LSE and traditional entrepreneurship haven’t any common business principles',
                        false,
                    ],
                    'a3' => [
                        'LSE and traditional entrepreneurship are based on the same business principles, except earning profit and high level of risk.',
                        true,
                    ],
                ],
                'f' => 'The definition of LSE reflects the similarities to Entrepreneurship of discovering new opportunities and mobilizing resources to generate profit, echoing the framework of entrepreneurship competences: Ideas and Opportunities; Resources and Into action. But LSE is different from traditional entrepreneurship, which aims at financial profit and huge risk. LSE has a very strong social aspect which helps to solve various problems.',
            ],
            'q2' => [
                'q' => 'What is “Lifestyle Entrepreneurship” (LSE)?',
                'a' => [
                    'a1' => [
                        'self-employment based on one’s education',
                        false,
                    ],
                    'a2' => [
                        'requires post-secondary education in business management',
                        false,
                    ],
                    'a3' => [
                        'discovering new opportunities based on one\'s talents, hobbies',
                        true,
                    ],
                ],
                'fy' => 'Lifestyle Entrepreneurship (LSE) is a process of discovering new opportunities for self-employment around one’s passions, hobbies and Lifestyle skills, the ability to express oneself and create financial and social profit and improve the quality of one’s own Lifestyle.',
                'fn' => 'Lifestyle Entrepreneurship (LSE) is a process of discovering new opportunities for self-employment around one’s passions, hobbies and Lifestyle skills, the ability to express oneself and create financial and social profit and improve the quality of one’s own Lifestyle. LSE is not based on one’s education.',
            ],
            'q3' => [
                'q' => 'Person has it’s own business. His main goal is to earn as much profit as possible no matter how much risk he needs to take. Please mark, what is the principle of this business:',
                'a' => [
                    'a1' => [
                        'Traditional Entrepreneurship',
                        true,
                    ],
                    'a2' => [
                        'Lifestyle Entrepreneurship',
                        false,
                    ],
                ],
                'fy' => 'Traditional Entrepreneurship aim is to get financial profit and taking huge risk. While LSE has a very strong social aspect which helps to solve various problems of disadvantaged people, e.g. social, economic, health, isolation, unemployment.',
                'fn' => 'LSE has a very strong social aspect which helps to solve various problems of disadvantaged people, e.g. social, economic, health, isolation, unemployment. Only traditional Entrepreneurship aim is to get financial profit and taking huge risk',
            ],
            'q4' => [
                'q' => 'Please select LSE style business examples:',
                'multiple' => true,
                'a' => [
                    'a1' => [
                        'cake-making',
                        true,
                    ],
                    'a2' => [
                        'homemade ice-cream',
                        true,
                    ],
                    'a3' => [
                        'catering from home',
                        true,
                    ],
                    'a4' => [
                        'childcare',
                        true,
                    ],
                    'a5' => [
                        'virtual office services',
                        true,
                    ],
                    'a6' => [
                        'mobile hairdressing',
                        true,
                    ],
                    'a7' => [
                        'real-estate investments',
                        false,
                    ],
                    'a8' => [
                        'constructions company',
                        false,
                    ],
                    'a9' => [
                        'big restaurant in city center',
                        false,
                    ],
                    'a10' => [
                        'Hi-tech shop',
                        false,
                    ],
                ],
                'fy' => 'It is usually a home- based business which can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle. Some businesses are run to provide an additional income to a permanent job, some to prevent social isolation.',
                'fn' => 'It is usually a home- based business that can be managed in a flexible way to fit in with the entrepreneur’s Lifestyle. Examples of businesses that Lifestyle entrepreneurs run include: cake-making, homemade jams, catering from home, childcare, virtual office services, mobile hairdressing. Meanwhile real-estate investments, constructions company, café in city center or Hi-tech shop requires more resources, are related to higher risk and the main aim for these companies should be the profit in order to survive.',
            ],
        ]
    ],
];
