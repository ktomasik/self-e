<?php
/**
 * Created by PhpStorm.
 * User: ktomasik
 * Date: 21.01.2019
 * Time: 15:37
 */

?>
<div class="row" style="margin-bottom: 2rem;">
    <div class="col-md-3">
        <a href="/workers-as-learner.html" class="btn btn-success">Back to YOUTH-WORKER AS LEARNER menu</a>
    </div>
</div>
<h6 style="background-color: #007bff; padding: .5rem; color: #fff" class="text-center">INNOVATIVE TRAINING ON SELF-EMPLOYMENT FOR YOUNG PEOPLE BASED ON MENTORING </h6>

<div class="row justify-content-center">
    <div class="col-sm-10 bordered">
        <h3 class="text-center worker-title" style="text-transform: uppercase">Module III</h3>
        Validation of the Competences “Sense of initiative and entrepreneurship”
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="row justify-content-center">
            <div class="col-sm-7 col-md-6">
                <a href="#">
                    <img src="/img/youth-workers/learners_modules_theory.png" class="img-fluid" alt="theory"/>
                </a>
            </div>
            <div class="col-sm-7 col-md-6">
                <a href="#">
                    <img src="/img/youth-workers/learners_modules_practical.png" class="img-fluid img-practical" alt="exercises"/>
                </a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-7 col-md-6">
                <a href="#">
                    <img src="/img/youth-workers/learners_modules_ppt.png" class="img-fluid" alt="presentation"/>
                </a>
            </div>
        </div>
    </div>
</div>
