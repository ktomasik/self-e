<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 offset-md-6">
            <span class="dark-blue"><strong>Project number No.: 2017-3-LT02-KA205-005536</strong></span>
        </div>
        <h1>Events</h1>
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <a class="p-event" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><h4><span class="blue"><strong>National workshop for Multiplying</strong></span></h4></a>
                    </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <p>During the project there will be 4 National workshop for Multiplying of O1, O2, O3 for youth workers and stakeholders in each partner country. It is foreseen that there will be 60 participants in total.</p>
                        <p>The main aim of National workshops in partners‘ countries is to multiply the developed intellectual outputs e-Toolkit for youth workers “Social mentoring as innovative training pathway to lifestyle self-employment – SELF-E” (O1), Set of OERs “Pathway to lifestyle self- employment (SE)” (O2), and Assessment tool for validation of competence “Sense of initiative and entrepreneurship” (O3) to the staff/youth workers/managers from organizations working with youth with fewer opportunities and youth’s consultants from Labour Exchange office in order to ensure the sustainability of the project results beyond the project’s partnership and project’s lifetime.</p>
                        <p>During these workshops, the new innovative educational methods – reversed training, mentor based learning, Open Educational Resources (OERs), blended-learning approach and new role of youth worker as mentor using learners’ participatory approach, and innovative approach for self-employment via Lifestyle Entrepreneurship will be introduced to the participants in order to enhance their professional development.</p>
                        <p>All participants will have a possibility to try an assessment tool for youth workers to become managers/mentors of mentoring process on SE.</p>
                        <p>Participants of the workshop will have a possibility to try OERs and assessment tool, prepared for youth with fewer opportunities, by themselves.</p>
                        <p>All tools and products of SELF-E will be presented to the participants explaining how they can be applied in practice and how the youth workers-managers and mentors could use them to foster self-employment of young people with fewer opportunities, including NEETS.</p>
                        <p>During this workshop the participants will be trained to become multipliers and equipped with the e-Toolkit (O1) consisting of the instructions on how to manage mentoring on SE, how to select and prepare the youth worker-mentor, who will be able to mentor the tailored learning process of youth on the Sense of initiative and entrepreneurship competence. This will help them to develop innovative mentor-support services within their youth organizations, thus, increasing participation of young people with disadvantaged background and fewer opportunities in learning and employability.</p>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <a class="p-event collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><h4><span class="blue"><strong>The 5-days learning/training activity in Cyprus by hosting partner CARDET in October 2019</strong></span></h4></a>
                    </h5>
                </div>

                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <p>Partners will select 2-4 participants, in total 14 participants for training. Partners will also invite the representatives of the associated partners to participate in order to ensure the sustainability of the project’s outputs. The new role of the trainer as a mentor within blended learning approach; learners’ participatory approach using ICT-enhanced training materials will be stressed during these days of the training. The added value of the training is professional development of the staff members of the digital and sense of initiative and entrepreneurship competences.</p>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                        <a class="p-event collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><h4><span class="blue"><strong>Final SELF-E project European conference in Lithuania</strong></span></h4></a>
                    </h5>
                </div>

                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <p>Final SELF-E project European conference in Lithuania (at least 45 participants are planned).<br/></p>
                        <span class="green">European Conference has three main aims:</span>
                        <ol>
                            <li>To ensure the dissemination of the project and its results beyond the project’s partnership;</li>
                            <li>To multiply the project’s results to youth organizations and communities by offering them the possibilities to learn from the project partners how to integrate the developed outputs (O1-O3) into the management and educational framework of their organizations,</li>
                            <li>To emphasize the new self-employment opportunities based on Life-Style Entrepreneurship (LSE) as a tool to foster self-employment of young people with fewer opportunities, including NEETS.</li>
                            <li>To mainstream the project’s results into national and European educational and social policies, especially to the priorities of the European Social Fund’s programs to ensure the continued use of the project results beyond the project’s lifetime.</li>
                        </ol>
                        <p>During the Conference three Intellectual Outputs (O1-O3) will be presented to the participants and suggested to use for fostering self-employment of young people with fewer opportunities, including NEETS and increasing their competence “Sense of initiative and entrepreneurship”.<br/>
                            The Digital stories of best practices on self-employment using life-style entrepreneurship approach will be demonstrated to motivate youth with fewer opportunities, including NEETs for self-employment and economic independence.</p>
                        <span class="green">The participants of the final conference will discuss the following questions:</span>
                        <ol>
                            <li>Strengthening collaboration among youth workers and formal and non-formal education organizations to promote active participation in life-long learning and engage young people with fewer opportunities in Life-Style Entrepreneurship;</li>
                            <li>Impact of the project’s results on achieving benchmarks of the EU strategy 2020.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
